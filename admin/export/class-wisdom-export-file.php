<?php
/**
 * Export to a CSV file
 *
 * @package Wisdom Plugin
 * @see EDD EDD_Export, https://github.com/easydigitaldownloads/easy-digital-downloads/blob/85c4f7021f6981f288a0afbd96b0a159ef9e2286/includes/admin/reporting/class-export.php
 * @since 1.3.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Wisdom_Export
 * @since 1.3.0
 */
if( ! class_exists( 'Wisdom_Export_File' ) ) {

	class Wisdom_Export_File {
		
		/**
		 * The file the data is stored in
		 */
		protected $file;
		
		/**
		 * Query params
		 */
		public $params;

		/**
		 * The name of the file the data is stored in
		 */
		public $filename;
		
		/**
		 * The type of the file the data is stored in
		 */
		public $filetype;
		
		/**
		 * The current step being processed
		 */
		public $step;
		
		/**
		 * Are we done?
		 */
		public $done;
		
		/**
		 *  Is the directory writable
		 */
		public $is_writable = true;
		
		/**
		 *  Is the export file empty
		 */
		public $is_empty = false;
		
		/**
		 * Constructor
		 * @since 1.0.0
		 */
		
		public function __construct( $_step = 1, $_params = array() ) {
			$upload_dir = wp_upload_dir();
			$this->filetype = '.csv';
			$this->filename = 'wisdom-export' . $this->filetype;
			$this->file = trailingslashit( $upload_dir['basedir'] ) . $this->filename;

			if ( ! is_writeable( $upload_dir['basedir'] ) ) {
				$this->is_writable = false;
			}
			
			$this->step = $_step;
			$this->params = $_params;
			$this->done = false;
		}

		/**
		 * Process a step
		 * @return bool
		 */
		public function process_step() {

			if( $this->step < 2 ) {

				// Make sure we start with a fresh file on step 1
				@unlink( $this->file );
				$this->print_csv_cols();
			}

			$rows = $this->print_csv_rows();

			if( $rows ) {
				return true;
			} else {
				return false;
			}
			
		}
		
		/**
		 * Retrieve the file data is written to
		 * @return string
		 */
		protected function get_file() {

			$file = '';

			if ( @file_exists( $this->file ) ) {

				if ( ! is_writeable( $this->file ) ) {
					$this->is_writable = false;
				}

				$file = @file_get_contents( $this->file );

			} else {

				@file_put_contents( $this->file, '' );
				@chmod( $this->file, 0664 );

			}

			return $file;
		}
		
		/**
		 * Output the CSV columns
		 *
		 * @access public
		 * @return string
		 */
		public function print_csv_cols() {

			$col_data = '';
			$cols = $this->get_csv_cols();
			
			$i = 1;
			foreach( $cols as $col_id => $column ) {
				$col_data .= '"' . addslashes( $column ) . '"';
				$col_data .= $i == count( $cols ) ? '' : ',';
				$i++;
			}
			$col_data .= "\r\n";

			$this->stash_step_data( $col_data );

			return $col_data;

		}
		
		/**
		 * Set the CSV columns
		 *
		 * @access public
		 * @return array $cols All the columns
		 */
		public function csv_cols() {
			$cols = array(
				'id'   		=> __( 'ID',   'wisdom-plugin' ),
				'title' 	=> __( 'Title', 'wisdom-plugin' ),
				'site' 		=> __( 'Site', 'wisdom-plugin' ),
				'slug' 		=> __( 'Slug', 'wisdom-plugin' )
			);
			$fields = wisdom_permitted_keys();
			$exclude = wisdom_exclude_keys_for_export();
			if( $fields ) {
				$params = $this->params;
				foreach( $fields as $field ) {
					if( ! empty( $params[$field] ) && ! in_array( $field, $exclude ) ) {
						$cols[$field] = $field;
					}
				}
			}
			return $cols;
		}

		/**
		 * Retrieve the CSV columns
		 *
		 * @access public
		 * @return array $cols Array of the columns
		 */
		public function get_csv_cols() {
			$cols = $this->csv_cols();
			return $cols;
		}
		
		/**
		 * Print the CSV rows for the current step
		 *
		 * @access public
		 * @return string|false
		 */
		public function print_csv_rows() {

			$row_data = '';
			$data = $this->get_data();
			$cols = $this->get_csv_cols();

			if( $data ) {

				// Output each row
				foreach ( $data as $row ) {
					$i = 1;
					foreach ( $row as $col_id => $column ) {
						// Make sure the column is valid
						if ( array_key_exists( $col_id, $cols ) ) {
							$row_data .= '"' . addslashes( preg_replace( "/\"/","'", $column ) ) . '"';
							$row_data .= $i == count( $cols ) ? '' : ',';
							$i++;
						}
					}
					$row_data .= "\r\n";
				}

				$this->stash_step_data( $row_data );

				return $row_data;
			}

			return false;
		}
		
		/**
		 * Get the data being exported
		 *
		 * @access public
		 * @since 1.4.4
		 * @return array $data Data for Export
		 */
		public function get_data() {
			
			$step = $this->step;
			$params = $this->params;
			
			$offset = wisdom_get_option( 'wisdom_options_settings', 'batch_size', 1000 );
			
			$data = array();
			
			$args = array(
				'post_type'			=> 'tracked-plugin',
				'posts_per_page'	=> $offset,
				'offset'			=> $offset * ( $step - 1 ),
				'fields'			=> 'ids',
				'no_found_rows'		=> true
			);
			
			// Check for some params
			if( $params['wisdom_plugin'] != 'all' ) {
				$args['meta_query'][] = array(
					'key'		=> 'wisdom_plugin_slug',
					'value'		=> $params['wisdom_plugin'],
					'compare'	=> '='
				);
			}
			// Query dates if specified
			// Dates are recorded as UNIX timestamps
			if( isset( $params['wisdom_start_date'] ) && $params['wisdom_start_date'] > 0 ) {
				$start = strtotime( $params['wisdom_start_date'] );
				$args['meta_query'][] = array(
					'key'		=> 'wisdom_first_recorded',
					'value'		=> $start,
					'type'		=> 'numeric',
					'compare'	=> '>='
				);
			}
			if( isset( $params['wisdom_end_date'] ) && $params['wisdom_end_date'] > 0 ) {
				// Set end date to 23:59:59 so that no sites get missed
				$end = strtotime( $params['wisdom_end_date'] ) + 86399;
				$args['meta_query'][] = array(
					'key'		=> 'wisdom_first_recorded',
					'value'		=> $end,
					'type'		=> 'numeric',
					'compare'	=> '<='
				);
			}
			
			$plugins = get_posts( $args );
			$count = $offset * ( $step - 1 );
			if( $plugins ) {
				foreach( $plugins as $plugin_id ) {
					$data[$count] = array(
						'id'   		=> $plugin_id,
						'title' 	=> get_the_title( $plugin_id ),
						'site' 		=> get_post_meta( $plugin_id, 'wisdom_site_name', true ),
						'slug' 		=> get_post_meta( $plugin_id, 'wisdom_plugin_slug', true ),
					);
					$fields = wisdom_permitted_keys();
					$exclude = wisdom_exclude_keys_for_export();
					if( $fields ) {
						foreach( $fields as $field ) {
							if( ! empty( $params[$field] ) && ! in_array( $field, $exclude ) ) {
								$data[$count][$field] = str_replace( ",", "\n", get_post_meta( $plugin_id, 'wisdom_' . $field, true ) );
							}
							
						}
					}
					$count++;
				}
			} else {
				$data = false;
			}

			return $data;
		}
		
		/**
		 * Append data to export file
		 *
		 * @param $data string The data to add to the file
		 * @return void
		 */
		protected function stash_step_data( $data = '' ) {

			$file = $this->get_file();
			$file .= $data;
			@file_put_contents( $this->file, $file );

			// If we have no rows after this step, mark it as an empty export
			$file_rows = file( $this->file, FILE_SKIP_EMPTY_LINES);
			$default_cols = $this->get_csv_cols();
			$default_cols = empty( $default_cols ) ? 0 : 1;

			$this->is_empty = count( $file_rows ) == $default_cols ? true : false;

		}
		
		/**
		 * Set the export headers
		 *
		 * @access public
		 * @return void
		 */
		public function headers() {
			ignore_user_abort( true );

			nocache_headers();
			header( 'Content-Type: text/csv; charset=utf-8' );
			header( 'Content-Disposition: attachment; filename=wisdom-export-' . date( 'm-d-Y-H-i-s' ) . '.csv' );
			header( "Expires: 0" );
		}
		
		/**
		 * Perform the export
		 *
		 * @access public
		 * @since 2.4
		 * @return void
		 */
		public function export() {
			// Set headers
			$this->headers();

			$file = $this->get_file();
			@unlink( $this->file );

			echo $file;

			die();
		}
		
	}

}
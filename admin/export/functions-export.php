<?php
/**
 * Functions for exporting to a CSV file
 *
 * @package Wisdom Plugin
 * @since 1.3.0
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * AJAX
 * Breaks query up into batches to prevent out of memory errors for large datasets
 */
function wisdom_do_batch_export() {
	
	$step = absint( $_POST['step'] );
	
	parse_str( $_POST['params'], $params );
	$params = (array) $params;
	
	if( ! wp_verify_nonce( $params['wisdom_batch_query'], 'wisdom_batch_query' ) ) {
		die();
	}
	
	$export = new Wisdom_Export_File( $step, $params );
	$status = $export->process_step();
	$batch = wisdom_get_option( 'wisdom_options_settings', 'batch_size', 1000 );
	
	$url = add_query_arg( array(
		'post_type'		=> 'tracked-plugin',
		'page'			=> 'export_page',
		'wisdom_export'	=> 'download',
	), admin_url( 'edit.php' ) );
	
	echo json_encode( 
		array( 
			'step' 		=> $step + 1,
			'status'	=> $status,
			'url'		=> $url,
			'batch'		=> $batch
		)
	);
	
	exit;
	
}
add_action( 'wp_ajax_wisdom_do_batch_export', 'wisdom_do_batch_export' );

/**
 * @return void
 */
function wisdom_export_file() {
	if( isset( $_GET['wisdom_export'] ) ) {
		$export = new Wisdom_Export_File();
		$export->export();
	}
}
add_action( 'admin_init', 'wisdom_export_file' );

/**
 * Get list of keys to exclude from export
 * Certain fields are always exported
 * @return Array
 */
function wisdom_exclude_keys_for_export() {
	$keys = array(
		'url',
		'plugin_slug',
		'site_name'
	);
	return apply_filters( 'wisdom_filter_excluded_export_keys', $keys );
}
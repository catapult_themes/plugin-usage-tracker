<?php
/**
 * Here we display our top level dashboard page
 *
 * @package     Wisdom Plugin
 * @since       1.0.0
*/

/**
 * @since 1.0.0
 */
require_once dirname( __FILE__ ) . '/admin-functions.php';
require_once dirname( __FILE__ ) . '/admin-settings.php';
require_once dirname( __FILE__ ) . '/columns.php';
require_once dirname( __FILE__ ) . '/charts/summary.php';
require_once dirname( __FILE__ ) . '/charts/active-deactive.php';
require_once dirname( __FILE__ ) . '/charts/activations-over-time.php';
require_once dirname( __FILE__ ) . '/charts/deactivations-reasons.php';
require_once dirname( __FILE__ ) . '/charts/deactivations-rate-version.php';
require_once dirname( __FILE__ ) . '/charts/plugin-options.php';
require_once dirname( __FILE__ ) . '/charts/other-plugins.php';
require_once dirname( __FILE__ ) . '/charts/languages.php';
require_once dirname( __FILE__ ) . '/export/class-wisdom-export-file.php';
require_once dirname( __FILE__ ) . '/export/functions-export.php';
require_once dirname( __FILE__ ) . '/menu/class-wisdom-admin.php';
require_once dirname( __FILE__ ) . '/menu/class-wisdom-my-plugins.php';
require_once dirname( __FILE__ ) . '/menu/class-wisdom-ajax-reports.php';
require_once dirname( __FILE__ ) . '/menu/class-wisdom-custom-reports.php';
require_once dirname( __FILE__ ) . '/menu/class-wisdom-export.php';
require_once dirname( __FILE__ ) . '/menu/class-wisdom-system-report.php';
//require_once dirname( __FILE__ ) . '/menu/class-wisdom-email-addresses.php';
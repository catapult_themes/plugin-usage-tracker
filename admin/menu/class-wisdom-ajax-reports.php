<?php
/**
 * This class will generate and display reports in the dashboard via AJAX
 *
 * @package Wisdom Plugin
 * @since 1.0.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Wisdom_Dashboard_Reports
 * @since 1.0.0
 */
if( ! class_exists( 'Wisdom_Ajax_Reports' ) ) {

	class Wisdom_Ajax_Reports {
		
		/**
		 * Constructor
		 * @since 1.0.0
		 */
		public function __construct() {
			
		}
		
		/**
		 * Initiate the class and start calling actions and filters
		 * @since 1.0.0
		 */
		public function init() {
			add_action( 'admin_menu', array( $this, 'add_reports_submenu' ) );
			
			// Charts and summaries
			add_action( 'wisdom_chart_summary', 'wisdom_chart_summary' );
			add_action( 'wisdom_chart_active', 'wisdom_chart_active' );
		//	add_action( 'wisdom_chart_languages', 'wisdom_active_languages' );
			add_action( 'wisdom_chart_activations', 'wisdom_activations_over_time' );
			add_action( 'wisdom_chart_deactivations', 'wisdom_deactivations_reasons' );
			add_action( 'wisdom_chart_deactivations-time-versions', 'wisdom_deactivations_rate_by_version' );
			add_action( 'wisdom_chart_plugin-options', 'wisdom_plugin_options' );
			add_action( 'wisdom_chart_other-plugins', 'wisdom_other_plugins' );
		}
		
		// Add the Reports submenu item
		public function add_reports_submenu() {
			add_submenu_page( 'edit.php?post_type=tracked-plugin', __( 'Reports', 'wisdom-plugin' ), __( 'Reports', 'wisdom-plugin' ), 'manage_options', 'wisdom_ajax_reports', array ( $this, 'reports_page' ) );
		}
		
		/**
		 * Display charts
		 * @hooked wisdom_reports_end
		 * @since 1.0.0
		 */
		public function reports_page() { 
			$reports = array(
				'summary' 						=> __( 'Summary', 'wisdom-plugin' ),
				'active' 						=> __( 'Active/Deactivated Plugins', 'wisdom-plugin' ),
				'activations' 					=> __( 'Activations Over Time', 'wisdom-plugin' ),
				'deactivations' 				=> __( 'Deactivation Reasons', 'wisdom-plugin' ),
				'plugin-options' 				=> __( 'Plugin Options', 'wisdom-plugin' ),
				'other-plugins'					=> __( 'Other Plugins', 'wisdom-plugin' )
			);
			$intervals = array(
				'daily'		=> __( 'Daily', 'wisdom-plugin' ),
				'weekly'	=> __( 'Weekly', 'wisdom-plugin' )
			);
			
			// The options
			$plugin_options = wisdom_get_plugin_options();
			if( ! empty( $plugin_options ) ) {
				$plugin_options = wisdom_pluck_option_keys( $plugin_options );
			}
			$plugins = wisdom_get_plugin_slugs();
			
			$current_report = isset( $_GET['report'] ) ? $_GET['report'] : 'summary';
			$current_interval = isset( $_GET['interval'] ) ? $_GET['interval'] : 'daily';
			$current_plugin = isset( $_GET['plugin'] ) ? $_GET['plugin'] : 'all';
			$current_option = isset( $_GET['plugin_option'] ) ? $_GET['plugin_option'] : 'all';
			$current_start = isset( $_GET['wisdom_start_date'] ) ? $_GET['wisdom_start_date'] : '';
			$current_end = isset( $_GET['wisdom_end_date'] ) ? $_GET['wisdom_end_date'] : '';
			$current_return = isset( $_GET['wisdom_return_top'] ) ? $_GET['wisdom_return_top'] : '20'; ?>
			
			<div class="wrap">
				<h1><?php _e( 'Wisdom Reports', 'wisdom-plugin' ); ?></h1>
				
				<?php do_action( 'wisdom_report_start' );
				// wisdom_global is where we store our basic data, enough to generate the summary report and a few others
				$global = get_transient( 'wisdom_summary_all' );
				
				if( ! empty( $global ) ) { ?>
				<div class="wisdom-charts-options">
					<form class="wisdom-report-params" method="post">
						<?php wp_nonce_field( 'wisdom_batch_query', 'wisdom_batch_query' ); ?>
						<select name="wisdom_report" id="wisdom_report">
							<?php foreach( $reports as $key=>$value ) { ?>
								<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $key, $current_report ); ?>><?php echo esc_attr( $value ); ?></option>
							<?php } ?>
						</select>
						<?php if( $current_report == 'activations' ) { ?>
							<select name="interval" id="wisdom_reports-interval">
								<?php foreach( $intervals as $key=>$value ) { ?>
									<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $key, $current_interval ); ?>><?php echo esc_attr( $value ); ?></option>
								<?php } ?>
							</select>
						<?php } ?>					
						<select name="wisdom_plugin" id="wisdom_plugin">
							<option value="all" <?php selected( 'all', $current_plugin ); ?>><?php _e( 'All', 'wisdom-plugin' ); ?></option>
							<?php foreach( $plugins as $key=>$value ) { ?>
								<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $key, $current_plugin ); ?>><?php echo esc_attr( $key ); ?></option>
							<?php } ?>
						</select>
						
						<?php // List of plugin options	?>
						<select name="plugin_option" id="wisdom_reports-plugin-option" style="display: none;">
							<?php if( ! empty( $plugin_options ) ) { 
								foreach( $plugin_options as $key=>$value ) { ?>
									<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $key, $current_option ); ?>><?php echo esc_attr( $key ); ?></option>
								<?php }
							} ?>
						</select>
						
						<input type="text" class="wisdom_date" id="wisdom_start_date" name="wisdom_start_date" placeholder="<?php _e( 'Start date', 'wisdom-plugin' ); ?>" value="<?php echo $current_start; ?>">
						<input type="text" class="wisdom_date" id="wisdom_end_date" name="wisdom_end_date" placeholder="<?php _e( 'End date', 'wisdom-plugin' ); ?>" value="<?php echo $current_end; ?>">
						
						<input type="number" min="1" class="" id="wisdom_return_top" name="" placeholder="<?php _e( 'Return number', 'wisdom-plugin' ); ?>" value="<?php echo $current_return; ?>" style="display: none;">
						
						<input type="hidden" name="post_type" value="tracked-plugin"/>
						<input type="hidden" name="page" value="wisdom_ajax_reports"/>
						
						<input type="submit" id="wisdom-run-query" value="<?php _e( 'Run Query', 'wisdom-plugin' ); ?>" class="button-secondary wisdom-run-query"/>
						
						<?php wp_nonce_field( 'wisdom_chart_options', 'wisdom_chart_options' ); ?>
						
					</form>
				</div>
				
				<script>
					jQuery(document).ready(function($){
						currentDate = new Date();
						$('#wisdom_start_date').datepicker({
							dateFormat: "yy-mm-dd",
							onSelect: function(date){
								var endPicker = $('#wisdom_end_date');
								var startDate = $(this).datepicker('getDate');
								endPicker.datepicker('setDate',startDate);
								endPicker.datepicker('option','minDate',startDate);
							}
						});
						$('#wisdom_end_date').datepicker({
							dateFormat: "yy-mm-dd"
						});
						$('#wisdom_report, #wisdom_plugin').on('change',function(){
							if( $('#wisdom_report').val() == 'plugin-options' ) {
								// Populate select with list of relevant options
								$.ajax({
									type: 'POST',
									url: ajaxurl,
									data: {
										action: 'wisdom_get_options_by_plugin',
										plugin: $('#wisdom_plugin').val(),
										form: $(this).closest('form').serialize(),
										wisdom_chart_options: $('#wisdom_chart_options').val()
									}
								})
								.done(function( response ) {
									//console.log(response);
									$('#wisdom_reports-plugin-option').html(response.html);
									$('#wisdom_reports-plugin-option').fadeIn();
								});
								
							} else {
								$('#wisdom_reports-plugin-option').fadeOut();
							}
							if( $('#wisdom_report').val() == 'other-plugins' ) {
								$('#wisdom_return_top').attr('name','wisdom_return_top');
								$('#wisdom_return_top').fadeIn();
							} else {
								$('#wisdom_return_top').attr('name','');
								$('#wisdom_return_top').fadeOut();
							}
						});
					});
				</script>
				<?php do_action( 'wisdom_chart_' . $current_report ); ?>

			</div>
		<?php }
		}
		
	}
	
	$Wisdom_Ajax_Reports = new Wisdom_Ajax_Reports;
	$Wisdom_Ajax_Reports->init();

}
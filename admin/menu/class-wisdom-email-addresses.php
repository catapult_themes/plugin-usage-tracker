<?php
/**
 * This class grabs email addresses
 *
 * @package Wisdom Plugin
 * @since 1.0.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Wisdom_Email_Addresses
 * @since 1.0.0
 */
if( ! class_exists( 'Wisdom_Email_Addresses' ) ) {

	class Wisdom_Email_Addresses {
		
		/**
		 * Constructor
		 * @since 1.0.0
		 */
		
		public function __construct() {
		}
		
		/**
		 * Initiate the class and start calling actions and filters
		 * @since 1.0.0
		 */
		public function init() {
			add_action( 'admin_menu', array( $this, 'add_email_addresses_submenu' ) );
		}
		
		// Add the Reports submenu item
		public function add_email_addresses_submenu() {
			add_submenu_page( 'edit.php?post_type=tracked-plugin', __( 'Email Addresses', 'wisdom-plugin' ), __( 'Email Addresses', 'wisdom-plugin' ), 'manage_options', 'emails_page', array ( $this, 'emails_page' ) );
		}
		
		/**
		 * Display My Plugins page
		 * @since 1.0.0
		 */
		public function emails_page() { ?>
			<div class="wrap">
				<h1><?php _e( 'Email Addresses', 'wisdom-plugin' ); ?></h1>
				<?php
				// Query each plugin and return email addresses in a list, sorted by plugin slug
				// Query between dates
				
				$current_start = isset( $_GET['wisdom_start_date'] ) ? $_GET['wisdom_start_date'] : '';
				$current_end = isset( $_GET['wisdom_end_date'] ) ? $_GET['wisdom_end_date'] : '';
				$emails = array();
				if( isset( $_GET['submit'] ) ) {
					// We've submitted the form so we can run the query
					$args = array(
						'post_type'			=> 'tracked-plugin',
						'posts_per_page'	=> -1,
						'orderby'			=> 'meta_value',
						'meta_key'			=> 'wisdom_plugin_slug'
					);
					
					$args['meta_query'] = array();
					
					// Only query if the email field is not empty
					$args['meta_query'][] = array(
						'key'		=> 'wisdom_email',
						'value'		=> '',
						'compare'	=> '!='
					);
					
					// Query dates if specified
					// Dates are recorded as UNIX timestamps
					if( isset( $_GET['wisdom_start_date'] ) && $_GET['wisdom_start_date'] > 0 ) {
						$start = strtotime( $_GET['wisdom_start_date'] );
						$args['meta_query'][] = array(
							'key'		=> 'wisdom_first_recorded',
							'value'		=> $start,
							'type'		=> 'numeric',
							'compare'	=> '>='
						);
					}
					
					if( isset( $_GET['wisdom_end_date'] ) && $_GET['wisdom_end_date'] > 0 ) {
						$end = strtotime( $_GET['wisdom_end_date'] );
						$args['meta_query'][] = array(
							'key'		=> 'wisdom_first_recorded',
							'value'		=> $end,
							'type'		=> 'numeric',
							'compare'	=> '<='
						);
					}
					$plugins = new WP_Query( $args );
					global $post;
					if( $plugins->have_posts() ) {
						$current_plugin = '';
						while( $plugins->have_posts() ): $plugins->the_post();
							$plugin_slug = get_post_meta( $post->ID, 'wisdom_plugin_slug', true );
							if( $plugin_slug != $current_plugin ) {
								$current_plugin = $plugin_slug;
								$emails[] = "\r";
								$emails[] = "=== " . sanitize_text_field( $plugin_slug ) . " ===";
								$emails[] = "\r";
							}
							
							$email = get_post_meta( $post->ID, 'wisdom_email', true );
							$emails[] = sanitize_email( $email );
						endwhile;
					}
					
				}
				?>
				<p><?php _e( 'If you have asked users to allow you to collect their email addresses, you can find all addresses here. Use the date fields to filter addresses then copy and paste into your favourite newsletter tool.', 'wisdom-plugin' ); ?></p>
				<p><?php _e( 'If you\'re using Mailchimp, you can automatically add users to lists via the Settings page.', 'wisdom-plugin' ); ?></p>
				<form id="wisdom-emails-form" method="get">
					<label><?php _e( 'From', 'wisdom-plugin' ); ?></label><input type="text" class="wisdom_date" id="wisdom_start_date" name="wisdom_start_date" placeholder="<?php _e( 'Start date', 'wisdom-plugin' ); ?>" value="<?php echo $current_start; ?>">
					<label><?php _e( 'To', 'wisdom-plugin' ); ?></label><input type="text" class="wisdom_date" id="wisdom_end_date" name="wisdom_end_date" placeholder="<?php _e( 'End date', 'wisdom-plugin' ); ?>" value="<?php echo $current_end; ?>">
				
					<input type="hidden" name="post_type" value="tracked-plugin"/>
					<input type="hidden" name="page" value="emails_page"/>
					<p><?php submit_button( __( 'Get Email Addresses', 'wisdom-plugin' ), 'primary', 'submit', false ); ?></p>
				</form>
				<?php if( ! empty( $emails ) ) { ?>
					<div class="wisdom-snippet-wrapper">
						<pre>
						<?php foreach( $emails as $key=>$email ) { 
							echo $email . "\n";
						} ?>
					</pre>
					</div>
				<?php } ?>
				
			</div>
			<script>
				jQuery(document).ready(function($){
					currentDate = new Date();
					$('#wisdom_start_date').datepicker({
						dateFormat: "yy-mm-dd",
						onSelect: function(date){
							var endPicker = $('#wisdom_end_date');
							var startDate = $(this).datepicker('getDate');
							endPicker.datepicker('setDate',startDate);
							endPicker.datepicker('option','minDate',startDate);
						}
					});
					$('#wisdom_end_date').datepicker({
						dateFormat: "yy-mm-dd"
					});
					
					$('#wisdom_end_date').datepicker("setDate", currentDate);
					currentDate.setDate(currentDate.getDate()-30);
					$('#wisdom_start_date').datepicker("setDate", currentDate);
					
				});
			</script>
		<?php }
		
	}
	
	$Wisdom_Email_Addresses = new Wisdom_Email_Addresses;
	$Wisdom_Email_Addresses->init();

}
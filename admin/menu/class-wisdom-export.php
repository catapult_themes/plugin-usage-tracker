<?php
/**
 * Export to a CSV file
 *
 * @package Wisdom Plugin
 * @since 1.3.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Wisdom_Export
 * @since 1.3.0
 */
if( ! class_exists( 'Wisdom_Export' ) ) {

	class Wisdom_Export {
		
		/**
		 * Constructor
		 * @since 1.0.0
		 */
		
		public function __construct() {
		}
		
		/**
		 * Initiate the class and start calling actions and filters
		 * @since 1.0.0
		 */
		public function init() {
			add_action( 'admin_menu', array( $this, 'add_export_submenu' ) );
		}
		
		// Add the Reports submenu item
		public function add_export_submenu() {
			add_submenu_page( 'edit.php?post_type=tracked-plugin', __( 'Export', 'wisdom-plugin' ), __( 'Export', 'wisdom-plugin' ), 'manage_options', 'export_page', array ( $this, 'export_page' ) );
		}
		
		/**
		 * Display Export page
		 */
		public function export_page() { ?>
			<div class="wrap">
				
				<h1><?php _e( 'Export', 'wisdom-plugin' ); ?></h1>
				
				<?php // The options
				$plugin_options = wisdom_get_plugin_options();
				if( ! empty( $plugin_options ) ) {
					$plugin_options = wisdom_pluck_option_keys( $plugin_options );
				}
				$plugins = wisdom_get_plugin_slugs();
			
				$current_plugin = isset( $_GET['wisdom_plugin'] ) ? $_GET['wisdom_plugin'] : 'all';
				$current_start = isset( $_GET['wisdom_start_date'] ) ? $_GET['wisdom_start_date'] : '';
				$current_end = isset( $_GET['wisdom_end_date'] ) ? $_GET['wisdom_end_date'] : ''; ?>
				
				<p><?php _e( 'Choose parameters to export.', 'wisdom-plugin' ); ?></p>
				
				<form id="wisdom-export-form" method="get">
							
					<p>
						<label for="wisdom_plugin"><?php _e( 'Product', 'wisdom-plugin' ); ?></label>
						<select name="wisdom_plugin" id="wisdom_plugin">
							<option value="all" <?php selected( 'all', $current_plugin ); ?>><?php _e( 'All', 'wisdom-plugin' ); ?></option>
							<?php if( $plugins ) {
								foreach( $plugins as $key=>$value ) { ?>
									<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $key, $current_plugin ); ?>><?php echo esc_attr( $key ); ?></option>
								<?php }
							} ?>
						</select>
					</p>
					
					<p>
						<label for="wisdom_plugin"><?php _e( 'Start date', 'wisdom-plugin' ); ?></label>
						<input type="text" class="wisdom_date" id="wisdom_start_date" name="wisdom_start_date" placeholder="<?php _e( 'Start date', 'wisdom-plugin' ); ?>" value="<?php echo $current_start; ?>">
					</p>
					<p>
						<label for="wisdom_plugin"><?php _e( 'End date', 'wisdom-plugin' ); ?></label>
						<input type="text" class="wisdom_date" id="wisdom_end_date" name="wisdom_end_date" placeholder="<?php _e( 'End date', 'wisdom-plugin' ); ?>" value="<?php echo $current_end; ?>">
					</p>
					
					<?php $fields = wisdom_permitted_keys();
					$exclude = wisdom_exclude_keys_for_export();
					if( $fields ) {
						foreach( $fields as $field ) { 
							if( ! in_array( $field, $exclude ) ) { ?>
								<p>
									<input type="checkbox" id="<?php echo esc_attr( $field ); ?>" name="<?php echo esc_attr( $field ); ?>" value="1"><label for="<?php echo esc_attr( $field ); ?>"><?php echo esc_attr( $field ); ?></label>
								</p>
							<?php }
						}
					} ?>
					
					<?php wp_nonce_field( 'wisdom_batch_query', 'wisdom_batch_query' ); ?>
					<input type="hidden" name="post_type" value="tracked-plugin"/>
					<input type="hidden" name="page" value="export_page"/>
					
					<input type="submit" id="wisdom-export-csv" value="<?php _e( 'Export to CSV', 'wisdom-plugin' ); ?>" class="button-primary wisdom-export-csv"/>
					
					<div class="wisdom-batch-progress"></div>
					
				</form>

			</div>
			<script>
				jQuery(document).ready(function($){
					currentDate = new Date();
					$('#wisdom_start_date').datepicker({
						dateFormat: "yy-mm-dd",
						onSelect: function(date){
							var endPicker = $('#wisdom_end_date');
							var startDate = $(this).datepicker('getDate');
						//	endPicker.datepicker('setDate',startDate);
							endPicker.datepicker('option','minDate',startDate);
						}
					});
					$('#wisdom_end_date').datepicker({
						dateFormat: "yy-mm-dd"
					});
					
				});
			</script>
		<?php }
		
	}
	
	$Wisdom_Export = new Wisdom_Export;
	$Wisdom_Export->init();

}
<?php
/**
 * The main admin class
 *
 * @package Wisdom Plugin
 * @since 1.1.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use \DrewM\MailChimp\MailChimp;

/**
 * Class Wisdom_Admin
 * @since 1.1.0
 */
if( ! class_exists( 'Wisdom_Admin' ) ) {

	class Wisdom_Admin {

		public $increment;

		/**
		 * Constructor
		 * @since 1.0.0
		 */
		public function __construct() {

		}

		/**
		 * Initiate the class and start calling actions and filters
		 * @since 1.1.0
		 */
		public function init() {
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
			add_action( 'admin_menu', array( $this, 'add_settings_submenu' ) );

			add_action( 'wp_ajax_wisdom_do_batch_query', array( $this, 'wisdom_do_batch_query' ) );
			add_action( 'wp_ajax_wisdom_check_transient', array( $this, 'wisdom_check_transient' ) );

			add_action( 'admin_init', array( $this, 'get_ids' ) );
			add_action( 'admin_init', array( $this, 'register_options_init' ) );
			add_action( 'admin_init', array( $this, 'register_system_init' ) );

			add_action( 'wisdom_report_start', array( $this, 'report_form' ) );

			// Only use this if you know what you're doing
			// Will delete all wisdom transients
			// add_action( 'init', array( $this, 'delete_all_wisdom_transients' ) );
		}

		public function enqueue_scripts() {
			// Only enqueue scripts if we're on a Wisdom page
			if( wisdom_is_wisdom_page() ) {
				wp_enqueue_script( 'wisdom-js', WISDOM_PLUGIN_URL . 'assets/js/wisdom.js', array( 'jquery' ), WISDOM_PLUGIN_VERSION, true );
				wp_enqueue_script( 'chart-js', WISDOM_PLUGIN_URL . 'assets/js/Chart.js', array( 'jquery', 'jquery-ui-datepicker' ), '2.4.0', true );
				wp_enqueue_style( 'wisdom-admin-css', WISDOM_PLUGIN_URL . 'assets/css/admin-style.css', array(), WISDOM_PLUGIN_VERSION );
			}
		}

		public function register_options_init() {
			register_setting( 'wisdom_options', 'wisdom_options_settings' );

			add_settings_section(
				'wisdom_options_section',
				__( 'Wisdom Settings', 'wisdom-plugin' ),
				array( $this, 'settings_section_callback' ),
				'wisdom_options'
			);

			// Set default options
			$options = get_option( 'wisdom_options_settings' );
			if( false === $options ) {
				// Get defaults
				$defaults = $this->get_default_settings();
				update_option( 'wisdom_options_settings', $defaults );
			}

			$settings = wisdom_settings();
			if( ! empty( $settings ) ) {
				foreach( $settings as $setting ) {
					add_settings_field(
						$setting['id'],
						$setting['label'],
						array( $this, $setting['callback'] ),
						'wisdom_options',
						'wisdom_options_section',
						$setting
					);
				}
			}
		}

		public function register_system_init() {
			register_setting( 'wisdom_system', 'wisdom_system_settings' );

			add_settings_section(
				'wisdom_system_section',
				__( 'Wisdom System', 'wisdom-plugin' ),
				array( $this, 'system_section_callback' ),
				'wisdom_system'
			);

			$settings = wisdom_system_settings();
			if( ! empty( $settings ) ) {
				foreach( $settings as $setting ) {
					add_settings_field(
						$setting['id'],
						$setting['label'],
						array( $this, $setting['callback'] ),
						'wisdom_system',
						'wisdom_system_section',
						$setting
					);
				}
			}

		}

		public function get_default_settings() {
			$defaults = array(
				'batch_size'				=> 1000,
				'top_plugins'				=> 20,
				'mailchimp_api_key'			=> '',
				'use_multiple_lists'		=> 1
			);
			return $defaults;
		}

		public function number_callback( $args ) {
			$options = get_option( $args['section'] );
			$value = '';
			if( isset( $options[$args['id']] ) ) {
				$value = $options[$args['id']];
			} ?>
			<input type='number' min="1" name="<?php echo $args['section']; ?>[<?php echo $args['id']; ?>]" value="<?php echo esc_attr( $value ); ?>" />
			<?php if( isset( $args['description'] ) ) { ?>
				<p class="description"><?php echo $args['description']; ?></p>
			<?php }
		}

		public function textarea_callback( $args ) {
			$options = get_option( $args['section'] );
			$value = '';
			if( isset( $options[$args['id']] ) ) {
				$value = $options[$args['id']];
			} ?>
			<textarea style="width: 300px; height: 100px;" name="<?php echo $args['section']; ?>[<?php echo $args['id']; ?>]"><?php echo esc_html( $value ); ?></textarea>
			<?php if( isset( $args['description'] ) ) { ?>
				<p class="description"><?php echo $args['description']; ?></p>
			<?php }
		}

		public function send_button_callback( $args ) {
			$options = get_option( $args['section'] );
			$current_url = admin_url( 'edit.php?post_type=tracked-plugin&page=wisdom_settings&tab=wisdom_system&send_report=true' );
			if( ! isset( $_GET['send_report'] ) ) { ?>
			<a class="button button-primary" href="<?php echo esc_url( $current_url ); ?>"><?php echo esc_html( $args['label'] ); ?></a>
			<?php } else {
				printf( '<p>%s</p>', __( 'Report sent. Thank you.', 'wisdom-plugin' ) );
			}
			if( isset( $args['description'] ) ) { ?>
				<p class="description"><?php echo $args['description']; ?></p>
			<?php }
		}

		public function api_callback( $args ) {
			$options = get_option( $args['section'] );
			$value = '';
			if( isset( $options[$args['id']] ) ) {
				$value = $options[$args['id']];
			} ?>
			<input type='text' name="<?php echo $args['section']; ?>[<?php echo $args['id']; ?>]" value="<?php echo esc_attr( $value ); ?>" />
			<?php if( isset( $args['description'] ) ) { ?>
				<p class="description"><?php echo $args['description']; ?></p>
			<?php }
			// Check validity of API key
			if( ! empty( $value ) ) {
				$Wisdom_Mailchimp = new Wisdom_Mailchimp;
				$validate = $Wisdom_Mailchimp->validate_key( $value );
				if( $validate == '200' ) { ?>
					<p class="validation valid" style="color: green;"><span class="dashicons dashicons-yes"></span> <?php _e( 'Valid key', 'wisdom-plugin' ); ?></p>
				<?php } else { ?>
					<p class="validation invalid" style="color: red;"><span class="dashicons dashicons-no-alt"></span> <?php _e( 'Invalid key', 'wisdom-plugin' ); ?></p>
				<?php }

			}
		}

		public function mailchimp_select_callback( $args ) {
			$options = get_option( $args['section'] );
			$value = '';
			if( isset( $options[$args['id']] ) ) {
				$value = $options[$args['id']];
			}

			$key = $options['mailchimp_api_key'];
			if( ! empty( $key ) ) {
				$MailChimp = new MailChimp( $key );
				$num_lists = apply_filters( 'wisdom_filter_number_lists', 50 );
				$lists = $MailChimp->get( 'lists', [
					'count' => $num_lists
				]);
			}

			// This is how many plugins we're allowed to track
			$limit = get_option( 'wisdom_license_limit', -1 );
			$licence_status = get_option( 'wisdom_plugin_license_status' );
			if( $licence_status != 'valid' ) {
				$limit = -1;
				printf( '<p>%s</p>',  __( 'Please check you have activated your Wisdom license key.', 'wisdom-plugin' ) );
			}
			// A list of all the plugin slugs recorded
			$slugs = wisdom_get_plugin_slugs();

			// Our set of optional rules for lists
			$rules = array(
				'plugins_include'	=> __( 'Plugins include', 'wisdom-plugin' ),
				'themes_include'	=> __( 'Themes include', 'wisdom-plugin' )
			);

			/**
			 * Set how many rules we can create for different lists
			 * @todo make this setting a repeater field
			 */
			$number_rules = apply_filters( 'wisdom_filter_number_rules', 5, $options );

			if( ! empty( $slugs ) && $licence_status == 'valid' ) {
				echo '<table class="widefat">';
				echo '<thead><tr><td>' . __( 'Product', 'wisdom-plugin' ) . '</td><td>' . __( 'List', 'wisdom-plugin' ) . '</td><td>' . __( 'Rule', 'wisdom-plugin' ) . '</td><td>' . __( 'Condition', 'wisdom-plugin' ) . '</td></tr></thead>';
				for( $i = 0; $i < $number_rules; $i++ ) {
					echo '<tr>';
					// Only display the permitted number of plugins we're allowed to track
					if( $i < $limit || $limit == 0 ) { ?>
						<td>
							<select name='<?php echo $args['section']; ?>[<?php echo $args['id']; ?>][<?php echo esc_attr( $i ); ?>][slug]'>
								<?php $selected = false;
									if( isset( $options[$args['id']][$i]['slug'] ) ) {
										$value = $options[$args['id']][$i]['slug'];
										$selected = selected( $value, 'all', false );
									} ?>
								<option value='all' <?php echo $selected; ?>><?php _e( 'All products', 'wisdom-plugin' ); ?></option>
								<?php foreach( $slugs as $slug=>$value ) {
									$selected = false;
									if( isset( $options[$args['id']][$i]['slug'] ) ) {
										$value = $options[$args['id']][$i]['slug'];
										$selected = selected( $value, $slug, false );
									} ?>
									<option value='<?php echo esc_attr( $slug ); ?>' <?php echo $selected; ?>><?php echo esc_attr( $slug ); ?></option>
								<?php } ?>
							</select>
						</td>
						<td>
						<?php if( ! empty( $lists['lists'] ) ) {
							?>
							<select name='<?php echo $args['section']; ?>[<?php echo $args['id']; ?>][<?php echo esc_attr( $i ); ?>][list]'>
								<?php $selected = false;
									if( isset( $options[$args['id']][$i]['list'] ) ) {
										$value = $options[$args['id']][$i]['list'];
										$selected = selected( $value, 'no_list', false );
									} ?>
								<option value='no_list' <?php echo $selected; ?>><?php _e( 'No list', 'wisdom-plugin' ); ?></option>
								<?php foreach( $lists['lists'] as $list ) {
									$selected = false;
									if( isset( $options[$args['id']][$i]['list'] ) ) {
										$value = $options[$args['id']][$i]['list'];
										$selected = selected( $value, esc_attr( $list['id'] ), false );
									} ?>
									<option value='<?php echo esc_attr( $list['id'] ); ?>' <?php echo $selected; ?>><?php echo esc_attr( $list['name'] ); ?></option>
								<?php } ?>
							</select>
							</td>

							<td>
								<select name='<?php echo $args['section']; ?>[<?php echo $args['id']; ?>][<?php echo esc_attr( $i ); ?>][rule]'>
									<?php $selected = false;
										if( isset( $options[$args['id']][$i]['rule'] ) ) {
											$value = $options[$args['id']][$i]['rule'];
											$selected = selected( $value, 'no_rule', false );
										} ?>
									<option value='no_rule' <?php echo $selected; ?>><?php _e( 'No rule', 'wisdom-plugin' ); ?></option>
									<?php foreach( $rules as $key=>$value ) {
										$selected = false;
										if( isset( $options[$args['id']][$i]['rule'] ) ) {
											$current = $options[$args['id']][$i]['rule'];
											$selected = selected( $key, $current, false );
										} ?>
										<option value='<?php echo esc_attr( $key ); ?>' <?php echo $selected; ?>><?php echo esc_attr( $value ); ?></option>
									<?php } ?>
								</select>
							</td>

							<td>
								<?php $condition = '';
								if( isset( $options[$args['id']][$i]['condition'] ) ) {
									$condition = $options[$args['id']][$i]['condition'];
								} ?>
								<input type='text' name="<?php echo $args['section']; ?>[<?php echo $args['id']; ?>][<?php echo esc_attr( $i ); ?>][condition]" value="<?php echo esc_attr( $condition ); ?>" />
							</td>
						<?php } else { ?>
							<p><?php _e( 'No lists available. Please check your API key.', 'wisdom-plugin' ); ?></p>
						<?php }
						echo '</td>';
					}
					echo '</tr>';
				}
				echo '</table>'; ?>

			<?php } else if( empty( $slugs ) ) {
				printf( '<p>%s</p>', __( 'No products recorded: you can map Mailchimp lists once you start receiving data from your tracked sites.', 'wisdom-plugin' ) );
			}

			if( isset( $args['description'] ) ) { ?>
				<p class="description"><?php echo $args['description']; ?></p>
			<?php }
		}

		public function checkbox_callback( $args ) {
			$options = get_option( $args['section'] );
			$value = '';
			if( isset( $options[$args['id']] ) ) {
				$value = $options[$args['id']];
			}
			$checked  = ! empty( $value ) ? checked( 1, $value, false ) : '';
			?>
			<input type='checkbox' name="<?php echo $args['section']; ?>[<?php echo $args['id']; ?>]" <?php echo $checked; ?> value='1'>
			<?php
			if( isset( $args['description'] ) ) { ?>
				<p class="description"><?php echo $args['description']; ?></p>
			<?php }
		}

		function delete_all_wisdom_transients() {
		    global $wpdb;
		    $sql = 'DELETE FROM ' . $wpdb->options . ' WHERE option_name LIKE "_transient_wisdom_%"';
		    $wpdb->query( $sql );
		}

		public function get_ids() {
			$options = get_option( 'wisdom_options_settings' );
			$inc = 1000;
			if( isset( $options['batch_size'] ) ) {
				$inc = absint( $options['batch_size'] );
			}
			$this->increment = apply_filters( 'wisdom_filter_batch_increment', $inc );
		}

		// Add the Settings submenu item
		public function add_settings_submenu() {
			add_submenu_page( 'edit.php?post_type=tracked-plugin', __( 'Settings', 'wisdom-plugin' ), __( 'Settings', 'wisdom-plugin' ), 'manage_options', 'wisdom_settings', array ( $this, 'settings_page' ) );
		}

		public function settings_section_callback() {
		}

		public function system_section_callback() {
			$Wisdom_System_Report = new Wisdom_System_Report;
			$Wisdom_System_Report->send_report();
			$Wisdom_System_Report->system_report();
		}

		/**
		 * Display page content
		 * @since 1.0.0
		 */
		public function settings_page() {
			$current = isset( $_GET['tab'] ) ? $_GET['tab'] : 'wisdom_options';
			$tabs = array(
				'wisdom_options'	=>	__( 'General', 'wisdom-plugin' ),
				'wisdom_system'		=>	__( 'System', 'wisdom-plugin' )
			);
			$tabs = apply_filters( 'wisdom_settings_tabs', $tabs ); ?>
			<div class="wrap">
				<h1><?php _e( 'Settings', 'wisdom-plugin' ); ?></h1>
				<?php settings_errors(); ?>

				<h2 class="nav-tab-wrapper">
					<?php foreach( $tabs as $tab => $name ) {
						$class =( $tab == $current ) ? ' nav-tab-active' : '';
						echo "<a class='nav-tab$class' href='?post_type=tracked-plugin&page=wisdom_settings&tab=$tab'>$name</a>";
					} ?>
				</h2>

				<form action='options.php' method='post'>
					<?php
					settings_fields( $current );
					do_settings_sections( $current );
					if( $current != 'wisdom_system' ) {
						submit_button();
					} ?>
				</form>
			</div><!-- .wrap -->


		<?php }

		public function report_form() {
			// wisdom_global is where we store our basic data, enough to generate the summary report and a few others
			$global = get_transient( 'wisdom_summary_all' ); ?>

			<div class="wisdom-batch-processor">
				<form class="wisdom-ajax-params" method="post">
					<?php wp_nonce_field( 'wisdom_batch_query', 'wisdom_batch_query' ); ?>
					<input type="hidden" name="wisdom_plugin" value="all" />
					<input type="hidden" name="transient" value="wisdom_summary_all" />
					<?php if( isset( $_GET['page'] ) ) { ?>
						<input type="hidden" name="page" value="<?php echo esc_attr( $_GET['page'] ); ?>" />
					<?php } ?>

					<?php if( ! empty( $global ) ) { ?>
						<p>
						<?php printf(
							'%s %s',
							__( 'Core report data last updated on ', 'wisdom-plugin' ),
							date( 'c', $global['last_run'] )
						); ?>

						<input type="submit" id="wisdom-refresh-query" value="<?php _e( 'Refresh data', 'wisdom-plugin' ); ?>" class="wisdom-run-query button-secondary" style="vertical-align: middle;" /></p>
					<?php } else { ?>

						<p><?php _e( 'We need to get the report data first. Please click the button below to start. Depending on how many sites you are tracking and the available server resources, you may find that you experience an out of memory error. If so, just reduce the batch size setting on the Settings page.', 'wisdom-plugin' ); ?></p>
						<p><input type="submit" id="wisdom-run-query" value="<?php _e( 'Get report data', 'wisdom-plugin' ); ?>" class="wisdom-run-query button-primary"/></p>
				<?php } ?>

					<div class="wisdom-batch-progress"></div>

				</form>
			</div>
		<?php }

		/**
		 * AJAX
		 * Breaks query up into batches to prevent out of memory errors for large datasets
		 * Builds $plugin_data array that is then stored as a transient
		 * Saves ever growing array as transient to prevent loss of data
		 * If anyone has alternative ideas, I'd be interested to hear them
		 * @since 1.1.0
		 */
		public function wisdom_do_batch_query() {

			$offset = absint( $_POST['offset'] );

			parse_str( $_POST['params'], $params );
			$params = (array) $params;

			if( ! wp_verify_nonce( $params['wisdom_batch_query'], 'wisdom_batch_query' ) ) {
				die();
			}

			/**
			 * @todo
			 * Use IDs for transients instead of full names in case name exceeds permitted length
			*/

			// If a transient name has been passed through, use that
			if( isset( $params['transient'] ) ) {
				$transient = esc_attr( $params['transient'] );
			} else {
				// Get transient name
				$transient = wisdom_get_transient_name( $params );
			}

			$plugin_data = array(); // Record all data in big array
			if( $offset == 0 ) {
				// This is our first pass
				// Save some useful data for the query
				$plugin_data['plugin_ids'] = wisdom_get_query_ids( $params );
				// total_plugins is a misnomer; this refers to the total number of sites included in the report
				$plugin_data['total_plugins'] = count( $plugin_data['plugin_ids'] );
				// Ensure any existing transient is deleted
				delete_transient( $transient );
				// If we're doing wisdom_summary_all, then we're refreshing all data so delete existing transients
				if( $transient == 'wisdom_summary_all' ) {
					$index = get_transient( 'wisdom_transient_index' );
					if( ! empty( $index ) ) {
						foreach( $index as $delete ) {
							delete_transient( $delete );
						}
					}
					delete_transient( 'wisdom_transient_index' );
				}

			} else {
				// We save the data to this transient in blocks, then pick it back up again at the start of a new batch
				$plugin_data = get_transient( $transient );
				// Set timestamp
				$plugin_data['last_run'] = time();
			}

			$url = '';

			if( $offset > $plugin_data['total_plugins'] ) {
				$offset = 'done';
				$args = array(
					'post_type'			=> 'tracked-plugin',
					'page'				=> $params['page'],
					'plugin'			=> $params['wisdom_plugin'],
					'metafield'			=> $params['metafield'],
					'report'			=> $params['wisdom_report'],
					'wisdom_start_date'	=> $params['wisdom_start_date'],
					'wisdom_end_date'	=> $params['wisdom_end_date'],
					'interval'			=> $params['interval'],
					'plugin_option'		=> $params['plugin_option'],
					'wisdom_return_top'	=> $params['wisdom_return_top'],
				);
				$url = add_query_arg( $args, admin_url( 'edit.php' ) );
				// Save the transient name to our list of transients
				$index = get_transient( 'wisdom_transient_index' );
				if( ! empty( $index ) && is_array( $index ) ) {
					if( ! in_array( $index, $transient ) ) {
						$index[] = $transient;
					}
				} else {
					$index = array( $transient );
				}
				set_transient( 'wisdom_transient_index', $index );
			} else {
				// Query the tracked-plugins
				$args = array(
					'post_type'			=> 'tracked-plugin',
					'posts_per_page'	=> $this->increment,
					'offset'			=> $offset,
					'fields'			=> 'ids',
					'no_found_rows'		=> true,
					'post__in'			=> $plugin_data['plugin_ids']
				);

				// Check for some params
				if( $params['wisdom_plugin'] != 'all' ) {
					$args['meta_query'][] = array(
						'key'		=> 'wisdom_plugin_slug',
						'value'		=> $params['wisdom_plugin'],
						'compare'	=> '='
					);
				}
				// Query dates if specified
				// Dates are recorded as UNIX timestamps
				if( isset( $params['wisdom_start_date'] ) && $params['wisdom_start_date'] > 0 ) {
					$start = strtotime( $params['wisdom_start_date'] );
					$args['meta_query'][] = array(
						'key'		=> 'wisdom_first_recorded',
						'value'		=> $start,
						'type'		=> 'numeric',
						'compare'	=> '>='
					);
				}
				if( isset( $params['wisdom_end_date'] ) && $params['wisdom_end_date'] > 0 ) {
					// Set end date to 23:59:59 so that no sites get missed
					$end = strtotime( $params['wisdom_end_date'] ) + 86399;
					$args['meta_query'][] = array(
						'key'		=> 'wisdom_first_recorded',
						'value'		=> $end,
						'type'		=> 'numeric',
						'compare'	=> '<='
					);
				}

				$plugins = get_posts( $args );
				foreach( $plugins as $plugin_id ) {
					// Break these up so we only collect the data we need for each report

					// Slug and status are included in all reports
					$plugin_slug = get_post_meta( $plugin_id, 'wisdom_plugin_slug', true );
					if( ! empty( $plugin_slug ) ) {
						if( isset( $plugin_data['slugs'][esc_attr( $plugin_slug )] ) ) {
							$plugin_data['slugs'][esc_attr( $plugin_slug )]++;
						} else {
							$plugin_data['slugs'][esc_attr( $plugin_slug )] = 1;
						}
					}
					$status = get_post_meta( $plugin_id, 'wisdom_status', true );
					if( ! empty( $status ) ) {
						if( isset( $plugin_data['wisdom_status'][esc_attr( $status )] ) ) {
							$plugin_data['wisdom_status'][esc_attr( $status )]++;
						} else {
							$plugin_data['wisdom_status'][esc_attr( $status )] = 1;
						}
					}

					// Activations over time monitors the number of activations and deactivations per day
					if( $params['wisdom_report'] == 'activations' ) {
						// Check the status first. Plugins can be reactivated.
						if( $status == 'Active' ) {
							$first = get_post_meta( $plugin_id, 'wisdom_first_recorded', true );
							if( ! empty( $first ) ) {
								// Round the date to the day, not the second, to avoid massive datasets
								$day = date( 'd F Y', $first );
								$first = strtotime( $day ) + 1;
								if( isset( $plugin_data['first_rec'][esc_attr( $first )] ) ) {
									$plugin_data['first_rec'][esc_attr( $first )]++;
								} else {
									$plugin_data['first_rec'][esc_attr( $first )] = 1;
								}
							}
						} else if( $status == 'Deactivated' ) {
							$deactivated_date = get_post_meta( $plugin_id, 'wisdom_deactivated_date', true );
							if( ! empty( $deactivated_date ) ) {
								// Round the date to the day, not the second, to avoid massive datasets
								$day = date( 'd F Y', $deactivated_date );
								$deactivated_date = strtotime( $day ) + 1;
								if( isset( $plugin_data['deactivated_date'][esc_attr( $deactivated_date )] ) ) {
									$plugin_data['deactivated_date'][esc_attr( $deactivated_date )]++;
								} else {
									$plugin_data['deactivated_date'][esc_attr( $deactivated_date )] = 1;
								}
							}
						}

					} else if( $params['wisdom_report'] == 'deactivations' ) {
						// Get the deactivation reasons
						$reason = get_post_meta( $plugin_id, 'wisdom_deactivation_reason', true );
						if( ! empty( $reason ) ) {
							$reason = json_decode( $reason );
							if( is_array( $reason ) ) {
								foreach( $reason as $r ) {
									if( isset( $plugin_data['reasons'][esc_attr( $plugin_slug )][esc_attr( $r )] ) ) {
										$plugin_data['reasons'][esc_attr( $plugin_slug )][esc_attr( $r )]++;
									} else {
										$plugin_data['reasons'][esc_attr( $plugin_slug )][esc_attr( $r )] = 1;
									}
								}
							}
						}

					} else if( $params['wisdom_report'] == 'plugin-options' ) {

						$options = get_post_meta( $plugin_id, 'wisdom_plugin_options_fields', true );

						if( ! empty( $options ) ) {

							foreach( $options as $key=>$value ) {
								// Check if we've got this option name yet
								// We don't bother recording wisdom_registered_setting
								if( ! isset( $plugin_data['options'][esc_attr( $plugin_slug )][esc_attr( $key )] ) ) {
									// Create a new element in the array
									// Capture the key, obviously
									// Then start making an array of values and the number of times they appear
									$plugin_data['options'][esc_attr( $plugin_slug )][esc_attr( $key )] = array(
										'values'	=> array(
											$value	=> 1
										)
									);
								} else {
									// We already have this option name recorded
									// Get the array of values that have already been recorded for this key
									$values = $plugin_data['options'][esc_attr( $plugin_slug )][esc_attr( $key )]['values'];
									if( ! isset( $plugin_data['options'][esc_attr( $plugin_slug )][esc_attr( $key )]['values'][$value] ) ) {
										// If we haven't seen this value yet, then add it to the other values and start counting it
										$plugin_data['options'][esc_attr( $plugin_slug )][esc_attr( $key )]['values'][$value] = 1;
									} else {
										// We have this value already so we increment the count
										$plugin_data['options'][esc_attr( $plugin_slug )][esc_attr( $key )]['values'][$value]++;
									}
								}
							}
						}
					} else if( $params['wisdom_report'] == 'other-plugins' ) {
						// Don't include our own plugins
						$our_plugins = wisdom_get_plugin_slugs();
						// Just the plugin slugs
						$our_plugins = array_keys( $our_plugins );

   						$plugins = get_post_meta( $plugin_id, 'wisdom_active_plugins', true );
						$plugins = explode( ',', $plugins );

   						if( ! empty( $plugins ) ) {

   							foreach( $plugins as $plugin_path ) {

								$plugin = explode( '/', $plugin_path );
								// Get the plugin directory to use
								$slug = str_replace( '.php', '', $plugin[1] );
								if( isset( $plugin[0] ) && ! in_array( $slug, $our_plugins ) ) {

									if( ! isset( $plugin_data['other_plugins'][esc_attr( $plugin[0] )] ) ) {
										// If we haven't seen this value yet, then add it to the other values and start counting it
										$plugin_data['other_plugins'][esc_attr( $plugin[0] )] = 1;
									} else {
										// We have this value already so we increment the count
										$plugin_data['other_plugins'][esc_attr( $plugin[0] )]++;
									}

								}

   							}

   						}

					} else if( isset( $params['metafield'] ) ) {
						// We're doing a meta field report
						$meta_value = get_post_meta( $plugin_id, $params['metafield'], true );

						if( ! empty( $meta_value ) ) {
							if( isset( $plugin_data[$params['metafield']][esc_attr( $meta_value )] ) ) {
								$plugin_data[$params['metafield']][esc_attr( $meta_value )]++;
							} else {
								$plugin_data[$params['metafield']][esc_attr( $meta_value )] = 1;
							}
						}

					} else {

						$lang = get_post_meta( $plugin_id, 'wisdom_site_language', true );
						$theme = get_post_meta( $plugin_id, 'wisdom_theme', true );

						if( ! empty( $lang ) ) {
							if( isset( $plugin_data['langs'][esc_attr( $lang )] ) ) {
								$plugin_data['langs'][esc_attr( $lang )]++;
							} else {
								$plugin_data['langs'][esc_attr( $lang )] = 1;
							}
						}
						if( ! empty( $theme ) ) {
							if( isset( $plugin_data['themes'][esc_attr( $theme )] ) ) {
								$plugin_data['themes'][esc_attr( $theme )]++;
							} else {
								$plugin_data['themes'][esc_attr( $theme )] = 1;
							}
						}

					}

				}

				$offset += $this->increment;
				set_transient( $transient, $plugin_data );

			}

			// $data = $this->plugin_data;
			$percentage = min( 100, ( $offset / $plugin_data['total_plugins'] ) * 100 );

			echo json_encode( array( 'offset' => $offset, 'params' => $params, 'percentage' => $percentage, 'plugin' => $params['wisdom_plugin'], 'count' => $plugin_data['total_plugins'], 'ids' => $plugin_data['plugin_ids'], 'transient' => $transient, 'url' => $url ) );

			exit;
		}

		/**
		 * Check whether the data we need has already been stored in a transient
		 * @since 1.1.0
		 */
		public function wisdom_check_transient() {

			parse_str( $_POST['params'], $params );
			$params = (array) $params;

			if( ! wp_verify_nonce( $params['wisdom_batch_query'], 'wisdom_batch_query' ) ) {
				die();
			}

			// Get the transient name
			if( isset( $params['transient'] ) ) {
				$transient = esc_attr( $params['transient'] );
			} else {
				// Get transient name
				$transient = wisdom_get_transient_name( $params );
			}

			$data = get_transient( $transient );
			$url = '';
			if( ! empty( $data ) ) {
				$status = 'ok';
				$args = array(
					'post_type'			=> 'tracked-plugin',
					'page'				=> $params['page'],
					'plugin'			=> $params['wisdom_plugin'],
					'metafield'			=> $params['metafield'],
					'report'			=> $params['wisdom_report'],
					'wisdom_start_date'	=> $params['wisdom_start_date'],
					'wisdom_end_date'	=> $params['wisdom_end_date'],
					'interval'			=> $params['interval'],
					'plugin_option'		=> $params['plugin_option'],
					'wisdom_return_top'	=> $params['wisdom_return_top'],
				);
				$url = add_query_arg( $args, admin_url( 'edit.php' ) );
			} else {
				$status = 'nok';
			}

			echo json_encode( array( 'status' => $status, 'transient' => $transient, 'url' => $url ) );
			exit;

		}

	}

	$Wisdom_Admin = new Wisdom_Admin;
	$Wisdom_Admin->init();

}

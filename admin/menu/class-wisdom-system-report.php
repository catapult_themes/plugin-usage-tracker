<?php
/**
 * The class to send back system data
 *
 * @package Wisdom Plugin
 * @since 1.3.1
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Wisdom_System_Report
 * @since 1.3.1
 */
if( ! class_exists( 'Wisdom_System_Report' ) ) {

	class Wisdom_System_Report {
		
		/**
		 * Constructor
		 */
		public function __construct() {
		}
		
		/**
		 * Initiate the class and start calling actions and filters
		 * @todo Add this as tab to Settings page
		 */
		public function init() {
			add_action( 'admin_init', array( $this, 'send_report' ) );
		}
		
		public function system_report() {
			$report = $this->get_system_report();
			echo '<div class="wisdom-system-report"><pre>' . $report . '</pre></div>';
		}
		
		public function get_system_report() {
			$settings = array(
				'wisdom_allow_tracking',
				'wisdom_block_notice',
				'wisdom_collect_email',
				'wisdom_last_track_time',
				'wisdom_plugin_license_key',
				'wisdom_plugin_license_status',
				'wisdom_license_limit',
				'wisdom_options_settings',
				'wisdom_plugins_option_name',
				'wisdom_mc_tracker'
			);
			$report = '<table><tbody>';
			$report .= "<tr>";
			$report .= "<td>" . __( 'Site URL' ) . "</td>";
			$report .= "<td>";
			$report .= get_bloginfo( 'url' );
			$report .= "</td>";
			$report .= "</tr>";
			foreach( $settings as $setting ) {
				$option = get_option( $setting );
				if( false !== $option ) {
					$report .= "<tr>";
					$report .= "<td>" . esc_html( $setting ) . "</td>";
					$report .= "<td>";
					if( is_array( $option ) ) {
						foreach( $option as $key=>$value ) {
							$report .= esc_html( $key ) . " => ";
							if( is_array( $value ) ) {
								$value = print_r( $value, true );
							}
							$report .= $value . "<br>";
						}
					} else {
						$report .= esc_html( $option );
					}
					$report .= "</td>";
					$report .= "</tr>";
				}
			}
			$report .= '</tbody></table>';
			
			return $report;
		}
		
		public function send_report() {
			if( isset( $_GET['send_report'] ) ) {
				add_filter( 'wp_mail_content_type', array( $this, 'set_html_mail' ) );
				$report = $this->get_system_report();
				wp_mail(
					'info@catapultthemes.com',
					'Wisdom system report from ' . get_bloginfo( 'name' ),
					$report
				);
				remove_filter( 'wp_mail_content_type', array( $this, 'set_html_mail' ) );
			}
		}
		
		public function set_html_mail() {
			return 'text/html';
		}
		
	}

}
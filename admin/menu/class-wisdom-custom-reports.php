<?php
/**
 * This class will generate and display custom reports in the dashboard
 *
 * @package Wisdom Plugin
 * @since 1.0.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Wisdom_Custom_Reports
 * @since 1.0.0
 */
if( ! class_exists( 'Wisdom_Custom_Reports' ) ) {

	class Wisdom_Custom_Reports {

		/**
		 * Constructor
		 * @since 1.0.0
		 */
		public function __construct() {

		}

		/**
		 * Initiate the class and start calling actions and filters
		 * @since 1.0.0
		 */
		public function init() {
			add_action( 'admin_menu', array( $this, 'add_reports_submenu' ) );
		}

		// Add the Reports submenu item
		public function add_reports_submenu() {
			add_submenu_page( 'edit.php?post_type=tracked-plugin', __( 'Custom Reports', 'wisdom-plugin' ), __( 'Custom Reports', 'wisdom-plugin' ), 'manage_options', 'wisdom_custom_reports', array ( $this, 'reports_page' ) );
		}

		/**
		 * Display charts
		 * @hooked wisdom_reports_end
		 * @since 1.0.0
		 */
		public function reports_page() {

			$plugins = wisdom_get_plugin_slugs();

			$metafields = wisdom_metafields();

			$current_plugin = isset( $_GET['plugin'] ) ? $_GET['plugin'] : '';
			$current_field = isset( $_GET['metafield'] ) ? $_GET['metafield'] : '';
			$current_chart = isset( $_GET['chart'] ) ? $_GET['chart'] : 'bar';
			$current_start = isset( $_GET['wisdom_start_date'] ) ? $_GET['wisdom_start_date'] : '';
			$current_end = isset( $_GET['wisdom_end_date'] ) ? $_GET['wisdom_end_date'] : '';
			$current_return = isset( $_GET['wisdom_return_top'] ) ? $_GET['wisdom_return_top'] : ''; ?>
			<div class="wrap">
				<h1><?php _e( 'Custom Reports', 'wisdom-plugin' ); ?></h1>
				<?php do_action( 'wisdom_report_start' );

				$global = get_transient( 'wisdom_summary_all' ); ?>

					<?php if( ! empty( $global ) ) { ?>
					<div class="wisdom-charts-options">
						<form class="wisdom-report-params" method="post">
							<?php wp_nonce_field( 'wisdom_batch_query', 'wisdom_batch_query' ); ?>
							<select name="wisdom_plugin" id="wisdom_plugin_select">
								<option value="all" <?php selected( 'all', $current_plugin ); ?>><?php _e( 'All Products', 'wisdom-plugin' ); ?></option>
								<?php foreach( $plugins as $key=>$value ) { ?>
									<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $key, $current_plugin ); ?>><?php echo esc_attr( $key ); ?></option>
								<?php } ?>
							</select>

							<?php // List of meta fields to query ?>
							<select name="metafield" id="wisdom_reports-metafield">
								<?php foreach( $metafields as $key=>$value ) { ?>
									<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $key, $current_field ); ?>><?php echo esc_attr( $value ); ?></option>
								<?php } ?>
							</select>

							<?php // List of chart types ?>
							<select name="chart" id="wisdom_reports-chart">
								<option value="bar" <?php selected( 'bar', $current_chart ); ?>><?php _e( 'Bar', 'wisdom-plugin' ); ?></option>
								<option value="doughnut" <?php selected( 'doughnut', $current_chart ); ?>><?php _e( 'Doughnut', 'wisdom-plugin' ); ?></option>
							</select>

							<input type="text" class="wisdom_date" id="wisdom_start_date" name="wisdom_start_date" placeholder="<?php _e( 'Start date', 'wisdom-plugin' ); ?>" value="<?php echo $current_start; ?>">
							<input type="text" class="wisdom_date" id="wisdom_end_date" name="wisdom_end_date" placeholder="<?php _e( 'End date', 'wisdom-plugin' ); ?>" value="<?php echo $current_end; ?>">

							<?php if( $current_return > 0 ) {
								$current_return = absint( $current_return );
							} else {
								$current_return = '';
							} ?>
							<input type="number" min="1" class="" id="wisdom_return_top" name="wisdom_return_top" placeholder="<?php _e( 'Return number', 'wisdom-plugin' ); ?>" value="<?php echo $current_return; ?>">

							<input type="hidden" name="post_type" value="tracked-plugin"/>
							<input type="hidden" name="page" value="wisdom_custom_reports"/>

							<input type="submit" id="wisdom-run-query" value="<?php _e( 'Run Query', 'wisdom-plugin' ); ?>" class="button-secondary wisdom-run-query"/>

						</form>
					</div>
					<script>
						jQuery(document).ready(function($){
							currentDate = new Date();
							$('#wisdom_start_date').datepicker({
								dateFormat: "yy-mm-dd",
								onSelect: function(date){
									var endPicker = $('#wisdom_end_date');
									var startDate = $(this).datepicker('getDate');
									endPicker.datepicker('setDate',startDate);
									endPicker.datepicker('option','minDate',startDate);
								}
							});
							$('#wisdom_end_date').datepicker({
								dateFormat: "yy-mm-dd"
							});
					//		$('#wisdom_end_date').datepicker("setDate", currentDate);
					//		currentDate.setDate(currentDate.getDate()-30);
					//		$('#wisdom_start_date').datepicker("setDate", currentDate);
						});
					</script>

					<?php if( ! empty( $current_plugin ) && ! empty( $current_field ) ) {
						$params = array(
							'metafield'				=> $current_field,
							'wisdom_plugin'			=> $current_plugin,
							'wisdom_start_date'		=> $current_start,
							'wisdom_end_date'		=> $current_end,
							'wisdom_return_top'		=> $current_return
						);
						$this->display_chart( $params, $current_chart );
					} ?>

				</div>
			<?php }
			}


		/**
		 * Displays the chart
		 * @since 1.0.0
		 */
		public function display_chart( $params, $current_chart ) {

			$transient = wisdom_get_transient_name( $params );
			error_log('xxzx');
			error_log( print_r( $params, true ) );
			$field_data = wisdom_get_meta_data_from_transient( $transient, $params['metafield'] );
			error_log('---');
			error_log( print_r( $field_data,true ));
			if( ! is_array( $field_data ) ) {
				_e( 'No data found for this report', 'wisdom-plugin' );
				return;
			}
			// Sort data with most common first
			arsort( $field_data );

			// We're going to use this value to calculate percentages below
			$total_value = array_sum( $field_data );

			// Get number of results to return
			$current_return = isset( $params['wisdom_return_top'] ) ? $params['wisdom_return_top'] : '20';
			$field_data = array_slice( $field_data, 0, absint( $current_return ), true );

			$count = 0; // Keep track of how many elements
			$colors = wisdom_nice_colors();
			$number_colors = count( $colors );
			if( ! empty( $field_data ) ) {
				$lang_labels = array();
				$lang_data = array();
				$bg_colors = array();
				$border_colors = array();
				foreach( $field_data as $key=>$value ) {
					$lang_labels[] = '"' . esc_attr( $key ) . '"';
					$lang_data[] = esc_attr( $value );
					// Decide which color to use
					if( $count >= $number_colors ) {
						// Use a random one
						$color = wisdom_generate_random_rgb();
					} else {
						$color = $colors[$count];
					}
					$bg_colors[] = '"rgba(' . $color . ',0.75' . ')"';
					$border_colors[] = '"rgba(' . $color . ',1' . ')"';
					$count++;
				}
				$labels = '[' . join( ',', $lang_labels ) . ']';
				$data = '[' . join( ',', $lang_data ) . ']';
				$bg = '[' . join( ',', $bg_colors ) . ']';
				$border = '[' . join( ',', $border_colors ) . ']';
			} else {
				_e( 'No data found for this report', 'wisdom-plugin' );
				return;
			}

			// Create a table to display this data
			?>
			<div id="wisdom-summary-wrapper" class="wisdom-summary-totals wisdom-other-plugins-totals">
				<table class="form-table wisdom-language-table">
					<tbody>
						<?php $count = 0; ?>
						<?php foreach( $field_data as $key=>$value ) { ?>
						<tr>
							<th scope="row"><span style="border: 2px solid <?php echo str_replace( '"', '', $border_colors[$count] ); ?>; background-color: <?php echo str_replace( '"', '', $bg_colors[$count] ); ?>;"></span><?php echo esc_html( $key ); ?></th>
							<td><?php echo esc_html( $value ); ?></td>
							<td><?php echo round( ( $value/$total_value ) * 100, 2 ) . '%'; ?></td>
						</tr>
						<?php $count++; ?>
						<?php } ?>
					</tbody>
				</table>
			</div><!-- .wisdom-summary-totals -->

			<div id="wisdom-charts-wrapper" class="wisdom-charts wisdom-other-plugins-charts">
				<div class="canvas-wrapper">
					<div style="position: relative; ">
						<canvas id="chartOne" width="400" height="400"></canvas>
					</div>
					<div style="position: relative; width: 49%;">
						<canvas id="chartTwo" width="400" height="200"></canvas>
					</div>
				</div>
			</div>

			<script>
				jQuery(document).ready(function($){
					// Let's set some vars
					var type = '<?php echo esc_attr( $current_chart ); ?>';
					var labels = <?php echo $labels; ?>;
					var data = <?php echo $data; ?>;
					var bg = <?php echo $bg; ?>;
					var border = <?php echo $border; ?>;

					var ctx = document.getElementById("chartOne").getContext("2d");
					var myChart = new Chart(ctx, {
					    type: type,
					    data: {
					        labels: labels,
					        datasets: [
								{
									data: data,
									backgroundColor: bg,
									borderColor: border,
									borderWidth: 3
								}
							]
					    },
						options: {
							legend: false
						}

					});
				});
			</script>
		<?php }

	}

	$Wisdom_Custom_Reports = new Wisdom_Custom_Reports;
	$Wisdom_Custom_Reports->init();

}

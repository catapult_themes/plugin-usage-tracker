<?php
/**
 * This class will allow you to specify your plugins
 *
 * @package Wisdom Plugin
 * @since 1.0.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class Wisdom_My_Plugins
 * @since 1.0.0
 */
if( ! class_exists( 'Wisdom_My_Plugins' ) ) {

	class Wisdom_My_Plugins {
		
		private $plugin_limit = 1;
		
		/**
		 * Constructor
		 * @since 1.0.0
		 */
		public function __construct() {
			// Set the number of plugins we're allowed to track
			// Don't do this every page load
			$this->plugin_limit = wisdom_plugin_set_plugin_limit();
		}
		
		/**
		 * Initiate the class and start calling actions and filters
		 * @since 1.0.0
		 */
		public function init() {
			add_action( 'admin_menu', array( $this, 'add_my_plugins_submenu' ) );
			add_action( 'admin_init', array( $this, 'register_wisdom_plugins_setting') );
		}
		
		// Add the Reports submenu item
		public function add_my_plugins_submenu() {
			add_submenu_page( 'edit.php?post_type=tracked-plugin', __( 'My Products', 'wisdom-plugin' ), __( 'My Products', 'wisdom-plugin' ), 'manage_options', 'my_plugins_page', array ( $this, 'my_plugins_page' ) );
		}
		
		/**
		 * Register the setting that will record our plugin slugs
		 * @since 1.0.0
		 */
		public function register_wisdom_plugins_setting() {
			// creates our settings in the options table
			register_setting( 'wisdom_plugins_option_group', 'wisdom_plugins_option_name', array( $this, 'wisdom_plugins_sanitize_callback' ) );
			
			add_settings_section(
				'wisdom_tracked_plugins_section', 
				__( 'Products', 'wisdom-plugin' ),
				array( $this, 'section_callback' ), 
				'wisdom_plugins_option_group'
			);
			
			//delete_option( 'wisdom_plugins_option_name' );
			$options = get_option( 'wisdom_plugins_option_name' );
			if( false === $options ) {
				$options = $this->default_settings();
				update_option( 'wisdom_plugins_option_name', $options );
			}
			
			add_settings_field( 
				'base_site', 
				__( 'Website URL', 'wisdom-plugin' ), 
				array( $this, 'base_site_callback' ),
				'wisdom_plugins_option_group',
				'wisdom_tracked_plugins_section'
			);
			
			// Add a field for each plugin available
			if( $this->plugin_limit > 0 ) {
				for( $i = 1; $i <= $this->plugin_limit; $i++ ) {
					add_settings_field ( 
						'plugin_slug_' . $i, 
						__( 'Basename for Product ', 'wisdom-plugin' ) . $i, 
						array( $this, 'plugin_slug_callback' ),
						'wisdom_plugins_option_group',
						'wisdom_tracked_plugins_section',
						array(
							'id'	=> 'plugin_slug_' . $i
						)
					);
					// Tracked options
					add_settings_field( 
						'tracked_options_' . $i, 
						__( 'Options for Plugin ', 'wisdom-plugin' ) . $i, 
						array( $this, 'tracked_options_callback' ),
						'wisdom_plugins_option_group',
						'wisdom_tracked_plugins_section',
						array(
							'id'	=> 'tracked_options_' . $i
						)
					);
					// Require opt-in
					add_settings_field( 
						'require_optin_' . $i, 
						__( 'Require Opt-in for Product ', 'wisdom-plugin' ) . $i, 
						array( $this, 'require_optin_callback' ),
						'wisdom_plugins_option_group',
						'wisdom_tracked_plugins_section',
						array(
							'id'	=> 'require_optin_' . $i
						)
					);
					// Include deactivation form
					add_settings_field( 
						'include_form_' . $i, 
						__( 'Include Deactivation Form for Plugin ', 'wisdom-plugin' ) . $i, 
						array( $this, 'include_form_callback' ),
						'wisdom_plugins_option_group',
						'wisdom_tracked_plugins_section',
						array(
							'id'	=> 'include_form_' . $i
						)
					);
					// Marketing message
					add_settings_field( 
						'marketing_' . $i, 
						__( 'Notification for Product ', 'wisdom-plugin' ) . $i, 
						array( $this, 'marketing_callback' ),
						'wisdom_plugins_option_group',
						'wisdom_tracked_plugins_section',
						array(
							'id'	=> 'marketing_' . $i,
							'index'	=> $i // Send this here because the snippet is compiled in this setting
						)
					);
				}
			}
		}
		
		/**
		 * Return the default settings
		 * @since 1.0.0
		 */
		public function default_settings() {
			$options = array(
				'base_site'			=> '',
				'plugin_slug_1'		=> '',
				'plugin_slug_2'		=> '',
				'plugin_slug_3'		=> '',
				'tracked_options_1'	=> '',
				'tracked_options_2'	=> '',
				'tracked_options_3'	=> '',
				'require_optin_1'	=> 1,
				'require_optin_2'	=> 1,
				'require_optin_3'	=> 1,
				'include_form_1'	=> 'true',
				'include_form_2'	=> 'true',
				'include_form_3'	=> 'true',
				'marketing_1'		=> 1,
				'marketing_2'		=> 1,
				'marketing_3'		=> 1,
			);
			return $options;
		}
		
		public function section_callback() {
			printf( 
				'<p>%s</p>',
				__( 'Enter the product details below.', 'wisdom-plugin' ),
				intval( $this->plugin_limit )
			);
		}
		
		public function base_site_callback() {
			$options = get_option( 'wisdom_plugins_option_name' );
			$value = '';
			if( isset( $options['base_site'] ) ) {
				$value = $options['base_site'];
			}
			?>
			<input type='text' class="widefat" name='wisdom_plugins_option_name[base_site]' value="<?php echo esc_url( $value ); ?>" />
			<p class="description"><?php _e( 'The website that will receive tracking data.', 'wisdom-plugin' ); ?></p>
			<?php
		}
		
		public function plugin_slug_callback( $args ) {
			$options = get_option( 'wisdom_plugins_option_name' );
			$value = '';
			if( isset( $options[$args["id"]] ) ) {
				$value = $options[$args["id"]];
			}		
			?>
			<input type='text' class="widefat" name='wisdom_plugins_option_name[<?php echo $args["id"]; ?>]' value="<?php echo esc_attr( $value ); ?>" />
			<p class="description"><?php _e( 'Enter the basename of your main plugin file, without the file extension, or if you\'re tracking a theme, enter the theme name', 'wisdom-plugin' ); ?></p>
			<?php
		}
		
		public function tracked_options_callback( $args ) {
			$options = get_option( 'wisdom_plugins_option_name' );
			$value = '';
			if( isset( $options[$args["id"]] ) ) {
				$value = $options[$args["id"]];
			}
			?>
			<input type='text' class="widefat" name='wisdom_plugins_option_name[<?php echo $args["id"]; ?>]' value="<?php echo esc_attr( $value ); ?>" />
		<?php }
		
		public function require_optin_callback( $args ) {
			$options = get_option( 'wisdom_plugins_option_name' );
			$value = '';
			if( isset( $options[$args["id"]] ) ) {
				$value = 1;
			}
			?>
			<input type='checkbox' name='wisdom_plugins_option_name[<?php echo $args["id"]; ?>]' <?php checked( ! empty( $value ), 1 ); ?> value="1" />
		<?php }
		
		public function include_form_callback( $args ) {
			$options = get_option( 'wisdom_plugins_option_name' );
			$value = '';
			if( isset( $options[$args["id"]] ) ) {
				$value = 1;
			}
			?>
			<input type='checkbox' name='wisdom_plugins_option_name[<?php echo $args["id"]; ?>]' <?php checked( ! empty( $value ), 1 ); ?> value="1" />
		<?php }
		
		public function marketing_callback( $args ) {
			$options = get_option( 'wisdom_plugins_option_name' );
			$value = '';
			$index = '';
			if( isset( $options[$args["id"]] ) ) {
				$value = $options[$args["id"]];
			}
			if( isset( $args["index"] ) ) {
				$index = intval( $args["index"] );
			}
			?>
			<select name='wisdom_plugins_option_name[<?php echo $args["id"]; ?>]'>
				<option value="0" <?php selected( $value, 0 );?>><?php _e( 'Option 0', 'wisdom-plugin' ); ?></option>
				<option value="1" <?php selected( $value, 1 );?>><?php _e( 'Option 1', 'wisdom-plugin' ); ?></option>
				<option value="2" <?php selected( $value, 2 );?>><?php _e( 'Option 2', 'wisdom-plugin' ); ?></option>
				<option value="3" <?php selected( $value, 3 );?>><?php _e( 'Option 3', 'wisdom-plugin' ); ?></option>
			</select>
			
			<?php
			$url = '';
			$slug = '';
			$settings = '';
			$optin = 'true';
			$form = 'true';
			$marketing = 1;
			if( isset( $options['base_site'] ) ) {
				$url = $options['base_site'];
			}
			if( isset( $options['plugin_slug_' . $index] ) ) {
				$slug = $options['plugin_slug_' . $index];
			}
			// Get any options and tidy up the string
			if( isset( $options['tracked_options_' . $index] ) ) {
				$settings = trim( $options['tracked_options_' . $index], ',' );
				// Tidy up the settings setting
				$settings = $this->tidy_settings( $settings );
				
			}
			// Opt in is required
			if( ! isset( $options['require_optin_' . $index] ) ) {
				$optin = 'false';
			}
			// Include deactivation form
			if( ! isset( $options['include_form_' . $index] ) ) {
				$form = 'false';
			}
			// Notification type
			if( isset( $options['marketing_' . $index] ) ) {
				$marketing = intval( $options['marketing_' . $index] );
			}
			
			// Populate the snippet
			$prefix = sanitize_text_field( str_replace( array( "-", " " ), "_", $slug ) );
			$prefix = preg_replace( "/[^A-Za-z0-9_]/", "", $prefix );
			$snippet = '';
			if( ! empty( $prefix ) && ! empty( $url ) ) {
				$url = esc_url( $url );
				// Only permit alphanumeric and underscore
				$snippet = str_replace( 
					array( 'your_product_prefix', 'BASE_URL', 'VARIABLE_NAME', 'SETTINGS', 'OPTIN', 'FORM', 'MARKETING' ),
					array( $prefix, $url, '$wisdom', $settings, $optin, $form, $marketing ),
					wisdom_code_snippet()
				);
			} else {
				if( empty( $prefix ) ) {
					$snippet .= '<br>' . __( 'No slug set', 'wisdom-plugin' );
				}
				if( empty( $url ) ) {
					$snippet .= '<br>' . __( 'No URL set', 'wisdom-plugin' );
				}
			} ?>
			<h4><?php _e( 'Code snippet for this product', 'wisdom-plugin' ); ?></h4>
			<div class="wisdom-snippet-wrapper">
				<pre style="margin-top: 0;">
					<?php echo '<br>' . trim( $snippet ); ?>
				</pre>
			</div>
			<p class="description"><?php _e( 'Copy the above snippet and paste it into your plugin main file or your theme functions.php file.', 'wisdom-plugin' ); ?></p>
			<?php
		}
		
		function wisdom_plugins_sanitize_callback( $input ) {
			$input['plugin_slug_1'] = sanitize_text_field( trim( $input['plugin_slug_1'] ) );
			$input['plugin_slug_2'] = sanitize_text_field( trim( $input['plugin_slug_2'] ) );
			$input['plugin_slug_3'] = sanitize_text_field( trim( $input['plugin_slug_3'] ) );
			$input['base_site'] = esc_url( trim( $input['base_site'] ) );
			// Options are stored as a comma separated list
			$input['tracked_options_1'] = sanitize_text_field( trim( $input['tracked_options_1'] ) );
			$input['tracked_options_2'] = sanitize_text_field( trim( $input['tracked_options_2'] ) );
			$input['tracked_options_3'] = sanitize_text_field( trim( $input['tracked_options_3'] ) );
			return $input;
		}
		
		/**
		 * Display My Plugins page
		 * @since 1.0.0
		 */
		public function my_plugins_page() { ?>
			<div class="wrap">
				<h1><?php _e( 'My Products', 'wisdom-plugin' ); ?></h1>
				<?php
				if( $this->plugin_limit == -1 ) {
					// License doesn't appear to be valid
					printf( 
						'<p>%s</p>',
						__( 'Your license doesn\'t appear to be valid. Please check.', 'wisdom-plugin' )
					);
				} else if( $this->plugin_limit == 0 ) {
					// License doesn't appear to be valid
					printf( 
						'<p>%s</p>',
						__( 'You can track an unlimited number of products.', 'wisdom-plugin' )
					);
					printf( 
						'<p>%s</p>',
						__( 'Get product snippet code.', 'wisdom-plugin' )
					); ?>
					<form method="post">
						<?php // Retain any values that have already been entered
						$url = site_url();
						$prefix = '';
						$settings = '';
						$unquoted_settings = '';
						$_optin = 1;
						$optin = 'true';
						$_form = 1;
						$form = 'true';
						$marketing = 1;
						if( isset( $_POST['base_url'] ) && isset( $_POST['plugin_slug'] ) ) {
							$url = esc_url( $_POST['base_url'] );
							// Only permit alphanumeric and underscore
							$prefix = sanitize_text_field( str_replace( array( "-", " " ), "_", $_POST['plugin_slug'] ) );
							$prefix = preg_replace( "/[^A-Za-z0-9_]/", "", $prefix );
						} 
						if( isset( $_POST['tracked_options'] ) ) {
							$settings = trim( $_POST['tracked_options'], ',' );
							// Get rid of unwanted quote marks
							$unquoted_settings = sanitize_text_field( str_replace( array( "\'", "\"" ), "", $settings ) );
							// Tidy up the settings setting
							$settings = $this->tidy_settings( $settings );
						}
						// Check if submit is set so that we know if to set default values for checkboxes or use inputted values
						if( isset( $_POST['submit'] ) ) {
							// The user has clicked the submit button so use $_POST values for the checkboxes
							if( ! isset( $_POST['_require_optin'] ) ) {
								$_optin = 0;
								$optin = 'false';
							}
							if( ! isset( $_POST['_include_form'] ) ) {
								$_form = 0;
								$form = 'false';
							}
						}
						
						if( isset( $_POST['marketing'] ) ) {
							$marketing = $_POST['marketing'];
						}
						?>
						<p>
							<input type="text" required style="width: 300px;" name="base_url" id="base_url" value="<?php echo esc_url( $url ); ?>" >
							<label for="base_url"><?php _e( 'URL for base website', 'wisdom-plugin' ); ?></label>
						</p>
						<p>
							<input type="text" required style="width: 300px;" name="plugin_slug" id="plugin_slug" value="<?php echo esc_attr( $prefix ); ?>">
							<label for="plugin_slug"><?php _e( 'Plugin basename or theme name', 'wisdom-plugin' ); ?></label>
						</p>
						<p>
							<input type="text" style="width: 300px;" name="tracked_options" id="tracked_options" value="<?php echo esc_attr( $unquoted_settings ); ?>">
							<label for="tracked_options"><?php _e( 'Options to track (comma separated)', 'wisdom-plugin' ); ?></label>
						</p>
						<p>
							<input type="checkbox" name="_require_optin" id="_require_optin" <?php checked( 1, $_optin ); ?> value="1">
							<input type="hidden" name="require_optin" id="require_optin" value="<?php echo esc_attr( $optin ); ?>">
							<label for="require_optin"><?php _e( 'Require opt-in', 'wisdom-plugin' ); ?></label>
						</p>
						<p>
							<input type="checkbox" name="_include_form" id="_include_form" <?php checked( 1, $_form ); ?> value="true" >
							<input type="hidden" name="include_form" id="include_form" value="<?php echo esc_attr( $form ); ?>">
							<label for="include_form"><?php _e( 'Include deactivation form', 'wisdom-plugin' ); ?></label>
						</p>
						<p>
							<select name='marketing'>
								<option value="0" <?php selected( $marketing, 0 );?>><?php _e( 'Option 0', 'wisdom-plugin' ); ?></option>
								<option value="1" <?php selected( $marketing, 1 );?>><?php _e( 'Option 1', 'wisdom-plugin' ); ?></option>
								<option value="2" <?php selected( $marketing, 2 );?>><?php _e( 'Option 2', 'wisdom-plugin' ); ?></option>
								<option value="3" <?php selected( $marketing, 3 );?>><?php _e( 'Option 3', 'wisdom-plugin' ); ?></option>
							</select>
							<label for="marketing"><?php _e( 'Notification type', 'wisdom-plugin' ); ?></label>
						</p>
						<p><button type="submit" name="submit" class="secondary button"><?php _e( 'Get Snippet', 'wisdom-plugin' ); ?></button>
						<?php // Ensure the snippet has a unique prefix
						if( isset( $_POST['base_url'] ) && isset( $_POST['plugin_slug'] ) ) {
							// Replace default elements with unique
							$snippet = str_replace( 
							array( 'your_product_prefix', 'BASE_URL', 'VARIABLE_NAME', 'SETTINGS', 'OPTIN', 'FORM', 'MARKETING' ),
							array( $prefix, $url, '$wisdom', $settings, $optin, $form, $marketing ),
							wisdom_code_snippet() );
							echo '<div class="wisdom-snippet-wrapper"><pre>' . $snippet . '</pre></div>';
						}
						?>
					</form>
					
				<?php } else {
					printf( 
						'<p>%s: %s</p>',
						__( 'Number of products to track', 'wisdom-plugin' ),
						intval( $this->plugin_limit )
					); 
				 ?>
					<form method="post" action="options.php">
						<?php
						settings_fields( 'wisdom_plugins_option_group' );
						do_settings_sections( 'wisdom_plugins_option_group' );
						submit_button();
						?>
					</form>
						
				<?php }
				
				?>
			</div>
		<?php }
		
		/**
		 * Tidy up the string for tracked settings
		 */
		public function tidy_settings( $settings ) {
			// Only allow alphanumeric, dashes, underscores and commas in the settings
			$settings_field = sanitize_text_field( str_replace( array( "-", " " ), "_", $settings ) );
			$settings_field = trim( $settings_field, ',' );
			$settings_field = explode( ',', $settings );
			// Split the string into an array, tidy it up, and wrap each element in quotes
			if( ! empty( $settings_field ) && is_array( $settings_field ) ) {
				$settings = '';
				foreach( $settings_field as $setting ) {
					// Check our element isn't just a comma and therefore empty
					if( ! empty( $setting ) ) {
						$settings .= '\'' . preg_replace( "/[^A-Za-z0-9_]/", "", $setting ) . '\'' . ',';
					}
					
				}
			} else {
				$settings = '';
			}
			$settings = trim( $settings, ',' );
			
			return $settings;
		}
		
	}
	
	$Wisdom_My_Plugins = new Wisdom_My_Plugins;
	$Wisdom_My_Plugins->init();

}
<?php
/**
 * Functions returning admin settings
 *
 * @package Wisdom Plugin
 * @since 1.2.0
*/

/**
 * Get all our settings fields
 *
 * @since 1.1.0
 * @return Array
*/
function wisdom_settings() {
	$settings = array(
		'batch_size' => array(
			'id'			=> 'batch_size',
			'label'			=> __( 'Batch size', 'wisdom-plugin' ),
			'callback'		=> 'number_callback',
			'page'			=> 'wisdom_options',
			'section'		=> 'wisdom_options_settings',
			'description'	=> __( 'Reduce this figure if you get out of memory issues running reports', 'wisdom-plugin' )
		),
		'mailchimp_api_key' => array(
			'id'		=> 'mailchimp_api_key',
			'label'		=> __( 'Mailchimp API Key', 'wisdom-plugin' ),
			'callback'	=> 'api_callback',
			'page'		=> 'wisdom_options',
			'section'	=> 'wisdom_options_settings',
		),
		'map_lists' => array(
			'id'			=> 'map_lists',
			'label'			=> __( 'Mailchimp subscriptions', 'wisdom-plugin' ),
			'callback'		=> 'mailchimp_select_callback',
			'page'			=> 'wisdom_options',
			'section'		=> 'wisdom_options_settings',
			'description'	=> __( 'Use the table above to define rules for subscribing users to different Mailchimp lists when they activate a tracked product', 'wisdom-plugin' )
		),
		'unsubscribe_deactivated' => array(
			'id'		=> 'unsubscribe_deactivated',
			'label'		=> __( 'Unsubscribe on deactivation', 'wisdom-plugin' ),
			'callback'	=> 'checkbox_callback',
			'page'		=> 'wisdom_options',
			'section'	=> 'wisdom_options_settings',
			'description'	=> __( 'Select this option to unsubscribe users from the lists above when they deactivate a tracked product', 'wisdom-plugin' )
		),
		'deactivate_lists' => array(
			'id'			=> 'deactivate_lists',
			'label'			=> __( 'Mailchimp deactivations', 'wisdom-plugin' ),
			'callback'		=> 'mailchimp_select_callback',
			'page'			=> 'wisdom_options',
			'section'		=> 'wisdom_options_settings',
			'description'	=> __( 'Use the table above to add users to different Mailchimp lists when they deactivate a tracked product', 'wisdom-plugin' )
		),
	);
	
	return $settings;
}

/**
 * Get all system settings fields
 *
 * @since 1.3.2
 * @return Array
*/
function wisdom_system_settings() {
	$settings = array(
		'send_button' => array(
			'id'			=> 'send_button',
			'label'			=> __( 'Send to Support', 'wisdom-plugin' ),
			'callback'		=> 'send_button_callback',
			'page'			=> 'wisdom_system',
			'section'		=> 'wisdom_system_settings'
		),
	);
	return $settings;
}

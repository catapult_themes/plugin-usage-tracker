<?php
/**
 * Some options and settings for displaying Tracked Plugins
 *
 * @package Wisdom Plugin
 * @since 1.0.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Add custom columns to tracked-plugin post type
 * @since 1.0.0
 */
if( ! function_exists( 'wisdom_add_custom_columns' ) ) {
	function wisdom_add_custom_columns( $columns ) {
		unset( $columns['date'] );
		$columns['site'] = __( 'Site', 'wisdom-plugin' );
		$columns['p_slug'] = __( 'Slug', 'wisdom-plugin' );
		$columns['plugin'] = __( 'Name', 'wisdom-plugin' );
		$columns['type'] = __( 'Type', 'wisdom-plugin' );
		$columns['status'] = __( 'Status', 'wisdom-plugin' );
		$columns['since'] = __( 'Since', 'wisdom-plugin' );
		$columns['deactivated'] = __( 'Deactivated', 'wisdom-plugin' );
		$columns['version'] = __( 'Version', 'wisdom-plugin' );
		$columns['lang'] = __( 'Lang', 'wisdom-plugin' );
		return $columns;
	}
	add_filter( 'manage_tracked-plugin_posts_columns', 'wisdom_add_custom_columns' );
}

/**
 * Populate custom columns with values
 * @since 1.0.0
 */
function wisdom_manage_custom_columns( $column, $post_id ) {
	if ( get_post_type( $post_id ) == 'tracked-plugin' ) {
		if( $column == 'site' ) {
			echo get_post_meta( $post_id, 'wisdom_site_name', true );
		} else if( $column == 'p_slug' ) {
			echo get_post_meta( $post_id, 'wisdom_plugin_slug', true );
		} else if( $column == 'type' ) {
			echo get_post_meta( $post_id, 'wisdom_product_type', true );
		} else if( $column == 'plugin' ) {
			echo get_post_meta( $post_id, 'wisdom_plugin', true );
		} else if( $column == 'status' ) {
			$status = get_post_meta( $post_id, 'wisdom_status', true );
			// We always record status as Active or Deactivated
			// Then allow translations here
			if( $status == 'Active' ) {
				_e( 'Active', 'wisdom-plugin' );
			} else if ( $status == 'Deactivated' ) {
				_e( 'Deactivated', 'wisdom-plugin' );
			} else {
				echo $status;
			}
		} else if( $column == 'since' ) {
			$since = get_post_meta( $post_id, 'wisdom_first_recorded', true );
			if( ! empty( $since ) ) {
				$date = wisdom_format_timestamp( $since );
				echo $date;
			}
		} else if( $column == 'deactivated' ) {
			$deact = get_post_meta( $post_id, 'wisdom_deactivated_date', true );
			if( ! empty( $deact ) ) {
				$date = wisdom_format_timestamp( $deact );
				echo $date;
			}
		} else if( $column == 'version' ) {
			echo get_post_meta( $post_id, 'wisdom_current_version', true );
		} else if( $column == 'lang' ) {
			echo get_post_meta( $post_id, 'wisdom_site_language', true );
		}
	}
}
add_action( 'manage_posts_custom_column', 'wisdom_manage_custom_columns', 10, 2 );

/**
 * Register sortable columns
 * @since 1.0.0
 */
function wisdom_sortable_plugin_columns( $columns ) {
	$columns['site'] = 'site';
	$columns['p_slug'] = 'p_slug';
	$columns['plugin'] = 'plugin';
	$columns['type'] = 'type';
	$columns['status'] = 'status';
	$columns['since'] = 'since';
	$columns['deactivated'] = 'deactivated';
	$columns['version'] = 'version';
	$columns['lang'] = 'lang';
	return $columns;
}
add_filter( 'manage_edit-tracked-plugin_sortable_columns', 'wisdom_sortable_plugin_columns' );


/**
 * Sort the columns
 * Based on work in EDD
 * @since 1.0
 */
function wisdom_sort_plugins( $vars ) {
	// Check if we're viewing the "download" post type
	if ( isset( $vars['post_type'] ) && 'tracked-plugin' == $vars['post_type'] ) {
		// Check if 'orderby' is set to "site"
		if ( isset( $vars['orderby'] ) && 'site' == $vars['orderby'] ) {
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => 'wisdom_site_name',
					'orderby'  => 'meta_value'
				)
			);
		}
		// Check if 'orderby' is set to "p_slug"
		if ( isset( $vars['orderby'] ) && 'p_slug' == $vars['orderby'] ) {
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => 'wisdom_plugin_slug',
					'orderby'  => 'meta_value'
				)
			);
		}

		// Check if "orderby" is set to "plugin"
		if ( isset( $vars['orderby'] ) && 'plugin' == $vars['orderby'] ) {
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => 'wisdom_plugin',
					'orderby'  => 'meta_value'
				)
			);
		}
		
		// Check if "orderby" is set to "type"
		if ( isset( $vars['orderby'] ) && 'type' == $vars['orderby'] ) {
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => 'wisdom_product_type',
					'orderby'  => 'meta_value'
				)
			);
		}

		// Check if "orderby" is set to "status"
		if ( isset( $vars['orderby'] ) && 'status' == $vars['orderby'] ) {
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => 'wisdom_status',
					'orderby'  => 'meta_value'
				)
			);
		}
		
		// Check if "orderby" is set to "since"
		if ( isset( $vars['orderby'] ) && 'since' == $vars['orderby'] ) {
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => 'wisdom_first_recorded',
					'orderby'  => 'meta_value_num'
				)
			);
		}
		
		// Check if "orderby" is set to "deactivated"
		if ( isset( $vars['orderby'] ) && 'deactivated' == $vars['orderby'] ) {
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => 'wisdom_deactivated_date',
					'orderby'  => 'meta_value_num'
				)
			);
		}
		
		// Check if "orderby" is set to "version"
		if ( isset( $vars['orderby'] ) && 'version' == $vars['orderby'] ) {
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => 'wisdom_current_version',
					'orderby'  => 'meta_value'
				)
			);
		}
		
		// Check if "orderby" is set to "lang"
		if ( isset( $vars['orderby'] ) && 'lang' == $vars['orderby'] ) {
			$vars = array_merge(
				$vars,
				array(
					'meta_key' => 'wisdom_site_language',
					'orderby'  => 'meta_value'
				)
			);
		}
		
	}

	return $vars;
}

/**
 * Sorts the columns
 *
 * @since 1.0
 * @return void
 */
function wisdom_plugins_load() {
	add_filter( 'request', 'wisdom_sort_plugins' );
}
add_action( 'load-edit.php', 'wisdom_plugins_load', 9999 );

/**
 * Filter slugs
 *
 * @since 1.1.0
 * @return void
 */
function wisdom_filter_tracked_plugins() {
    global $typenow;
    global $wp_query;
    if ( $typenow == 'tracked-plugin' ) { // Your custom post type slug
        $plugins = wisdom_get_plugin_slugs(); // Options for the filter select field
		$current_plugin = '';
		if( isset( $_GET['slug'] ) ) {
			$current_plugin = $_GET['slug']; // Check if option has been selected
		} ?>
		<select name="slug" id="slug">
			<option value="all" <?php selected( 'all', $current_plugin ); ?>><?php _e( 'All', 'wisdom-plugin' ); ?></option>
			<?php if( $plugins ) {
				foreach( $plugins as $key=>$value ) { ?>
					<option value="<?php echo esc_attr( $key ); ?>" <?php selected( $key, $current_plugin ); ?>><?php echo esc_attr( $key ); ?></option>
				<?php }
			} ?>
		</select>
        <?php
    }
}
add_action( 'restrict_manage_posts', 'wisdom_filter_tracked_plugins' );

/**
 * Update query
 *
 * @since 1.1.0
 * @return void
 */
function wisdom_sort_plugins_by_slug( $query ) {
    global $pagenow;
	// Get the post type
	$post_type = isset( $_GET['post_type'] ) ? $_GET['post_type'] : '';
	if ( is_admin() && $pagenow=='edit.php' && $post_type == 'tracked-plugin' && isset( $_GET['slug'] ) && $_GET['slug'] !='all' ) {
        $query->query_vars['meta_key'] = 'wisdom_plugin_slug';
        $query->query_vars['meta_value'] = $_GET['slug'];
		$query->query_vars['meta_compare'] = '=';
    }
}
add_filter( 'parse_query', 'wisdom_sort_plugins_by_slug' );
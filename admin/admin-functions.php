<?php
/**
 * Functions returning reporting data
 *
 * @package Wisdom Plugin
 * @since 1.0.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Adds wisdom-theme class to tracked-plugin edit screen
 * Hides plugin metabox when product is a theme
 * @since 1.4.0
 */
function wisdom_filter_admin_body_class( $classes ) {
	global $post;
	if( $post ) {
		$product_type = get_post_meta( $post->ID, 'wisdom_product_type', true );
		$screen = get_current_screen();
		if( isset( $screen->id ) && $screen->id == 'tracked-plugin' && $product_type == 'theme' ) {
			$classes .= ' wisdom-theme ';
		}
	}
	return $classes;
}
add_filter( 'admin_body_class', 'wisdom_filter_admin_body_class' );

/**
 * Are we on a Wisdom page?
 *
 * @since 1.0.0
 * @return Boolean
*/
function wisdom_is_wisdom_page() {
	global $pagenow, $typenow;
	$is_wisdom_page = false;
	$post_type = isset( $_GET['post_type'] )  ? strtolower( $_GET['post_type'] )  : false;
	if ( ( 'tracked-plugin' == $typenow || 'tracked-plugin' === $post_type ) ) {
		$is_wisdom_page = true;
	}
	return $is_wisdom_page;
}

/**
 * Return a timestamp in the correct format for the site
 *
 * @since 1.0.0
 * @return Integer
*/
function wisdom_format_timestamp( $time ) {
	$date_format = get_option( 'date_format' );
	$date = date( $date_format, $time );
	return $date;
}

/**
 * How many sites are we tracking?
 * This just returns the count of tracked-plugin posts
 *
 * @since 1.0.0
 * @return Integer
*/
function wisdom_summary_total_sites() {
	$count = wp_count_posts( 'tracked-plugin' );
	return $count->publish;
}

/**
 * Gets how many sites we're tracking for this report
 * Look for values in a transient
 * @since 1.1.0
 * @return Integer
*/
function wisdom_get_total_sites_summary() {
	// Report parameters
	$params = wisdom_get_report_params();
	$transient = wisdom_get_transient_name( $params );
	$data = get_transient( $transient );
	$count = 0;
	if( isset( $data['total_plugins'] ) ) {
		$count = $data['total_plugins'];
	}
	return $count;
}

/**
 * Returns report parameters
 * Look for values in a transient
 * @since 1.1.0
 * @return Array
*/
function wisdom_get_report_params() {
	// Report parameters
	$params = array();
	$params['wisdom_report'] = isset( $_GET['report'] ) ? $_GET['report'] : 'summary';
	$params['wisdom_plugin'] = isset( $_GET['plugin'] ) ? $_GET['plugin'] : 'all';
	$params['wisdom_start_date'] = isset( $_GET['wisdom_start_date'] ) ? $_GET['wisdom_start_date'] : '';
	$params['wisdom_end_date'] = isset( $_GET['wisdom_end_date'] ) ? $_GET['wisdom_end_date'] : '';
	$params['interval'] = isset( $_GET['interval'] ) ? $_GET['interval'] : '';
	$params['wisdom_return_top'] = isset( $_GET['wisdom_return_top'] ) ? $_GET['wisdom_return_top'] : '';

	return $params;

}

/**
 * Gets how many plugins we're tracking for this report
 * Look for values in a transient
 * @since 1.1.0
 * @return Integer
*/
function wisdom_get_total_plugins_summary() {
	// Report parameters
	$params = wisdom_get_report_params();
	$transient = wisdom_get_transient_name( $params );
	$data = get_transient( $transient );
	$count = 0;
	if( isset( $data['slugs'] ) ) {
		$count = count( $data['slugs'] );
	}
	return $count;
}

/**
 * Gets count of specific values by metafield for this report
 * Look for values in a transient
 * @since 	1.1.0
 * @param	$args meta_key and meta_value to query
 * @return 	Integer
*/
function wisdom_get_total_by_meta_summary( $args ) {

	if( isset( $args['meta_key'] ) ) {
		$meta_key = $args['meta_key'];
	}
	if( isset( $args['meta_value'] ) ) {
		$meta_value = $args['meta_value'];
	}
	// Report parameters
	$params = wisdom_get_report_params();
	$transient = wisdom_get_transient_name( $params );
	$data = get_transient( $transient );

	if( isset( $data[$meta_key][$meta_value] ) ) {
		return $data[$meta_key][$meta_value];
	}

	return 0;
}

/**
 * Gets the transient name we need
 * @since 1.1.0
 * @param $params	Parameters for the report data
 * @return String
*/
function wisdom_get_transient_name( $params ) {
	// Build transient name using plugin names and start and end dates
	$transient = 'wisdom';
	if( isset( $params['wisdom_report'] ) ) {
		$transient .= '_' . esc_attr( $params['wisdom_report'] );
	}
	if( isset( $params['metafield'] ) ) {
		$transient .= '_' . esc_attr( $params['metafield'] );
	}
	if( isset( $params['wisdom_plugin'] ) ) {
		$transient .= '_' . esc_attr( $params['wisdom_plugin'] );
	} else {
		$transient .= '_all';
	}

	if( isset( $params['wisdom_start_date'] ) && $params['wisdom_start_date'] > 0 ) {
		$transient .= '_' . esc_attr( $params['wisdom_start_date'] );
	}
	if( isset( $params['wisdom_end_date'] ) && $params['wisdom_end_date'] > 0 ) {
		$transient .= '_to_' . esc_attr( $params['wisdom_end_date'] );
	}
	return $transient;
}

/**
 * Query the IDs only for much better performance
 *
 * @since 1.1.0
 * @return Mixed	False if no results | Array if query has results
*/
function wisdom_get_query_ids( $params=array() ) {
	// Query the IDs only for all tracked-plugins
	$args = array(
		'post_type'			=> 'tracked-plugin',
		'posts_per_page'	=> -1,
		'fields'			=> 'ids',
		'post_status'		=> 'publish'
	);
	if( ! empty( $params['wisdom_plugin'] ) && $params['wisdom_plugin'] != 'all' ) {
		$args['meta_query'][] = array(
			'key'		=> 'wisdom_plugin_slug',
			'value'		=> $params['wisdom_plugin'],
			'compare'	=> '='
		);
	}
	if( ! empty( $params['wisdom_start_date'] ) && $params['wisdom_start_date'] > 0 ) {
		// Round start date time down to 00:00 so that no sites get missed
		$start = strtotime( $params['wisdom_start_date'] );
		$args['meta_query'][] = array(
			'key'		=> 'wisdom_first_recorded',
			'value'		=> $start,
			'type'		=> 'numeric',
			'compare'	=> '>='
		);
	}
	if( ! empty( $params['wisdom_end_date'] ) && $params['wisdom_end_date'] > 0 ) {
		// Set end date time to 23:59:59 so that no sites get missed
		$end = strtotime( $params['wisdom_end_date'] ) + 86399;
		$args['meta_query'][] = array(
			'key'		=> 'wisdom_first_recorded',
			'value'		=> $end,
			'type'		=> 'numeric',
			'compare'	=> '<='
		);
	}
	$id_query = new WP_Query( $args );
	if( $id_query->have_posts() ) {
		return $id_query->posts;
	}

	return false;
}

/**
 * Get array of plugin slugs
 *
 * @since 1.1.0
 * @return Array
*/
function wisdom_get_plugin_slugs() {
	// Get plugins from transient, stored as $key=>$value array
	$plugins = wisdom_get_transient_value_by_key( 'wisdom_summary_all', 'slugs' );
	// See if we've identified any others since the transient was last saved
	$other_plugins = get_option( 'wisdom_tracked_plugins' );
	if( ! empty( $plugins ) ) {
		if( false !== $other_plugins ) {
			foreach( $other_plugins as $other_plugin ) {
				if( ! in_array( $other_plugin, $plugins ) ) {
					// Save value as 1 as this is likely to be the first one
					$plugins[$other_plugin] = 1;
				}
			}
		}
		return $plugins;
	}
	return false;
}

/**
 * Return wisdom_plugin_data transient value by key
 * @since 1.1.0
 * @param $transient	The transient to search in
 * @param $key			The key to look for
 * @return Mixed
*/
function wisdom_get_transient_value_by_key( $transient, $key ) {
	/**
	 * The first place to look is the transient
	 * The values might be slightly out of date but saves running another big query
	 */
	$stored_data = get_transient( $transient );
	if( ! empty( $stored_data[$key] ) ) {
		return $stored_data[$key];
	}
	return false;
}

/**
 * Return wisdom_plugin_data transient count by key and value
 * @since 1.1.0
 * @param $transient	The transient to search in
 * @param $key			The key to look for
 * @param $value		The specific value to look for in $key
 * @return Mixed
*/
function wisdom_get_transient_meta_by_key_value( $transient, $key, $value ) {
	$stored_data = get_transient( $transient );
	if( ! empty( $stored_data[$key][$value] ) ) {
		return $stored_data[$key][$value];
	}
	return false;
}

/**
 * Get active / deactivated count per day
 * Use this to compile daily, weekly, monthly reports
 *
 * @since 1.1.0
 * @return Integer
*/
function wisdom_get_activations_per_day( $status='first_rec', $start_date, $interval, $plugin=null ) {
	$params = wisdom_get_report_params();
	$transient = wisdom_get_transient_name( $params );
	$data = get_transient( $transient );
	if( $interval == 'daily' ) {
		// If the interval is daily, just get the count for that day
		if( isset( $data[$status][$start_date] ) ) {
			return $data[$status][$start_date];
		}
	} else if( $interval == 'weekly' ) {
		// If it's weekly, iterate through 7 days
		$return = 0; // The value to return
		$increment = 604800; // Seconds in a day
		for( $i = 0; $i < 7; $i++ ) {
			// Work out the datestamp of each day in the week
			$date = $start_date + ( $increment * $i );
			if( isset( $data[$status][$date] ) ) {
				$return += $data[$status][$date];
			}
		}
		return $return;
	}

	return 0;
}

/**
 * Get deactivation reasons
 *
 * @since 1.1.0
 * @return Array
*/
function wisdom_get_deactivation_reasons() {
	$params = wisdom_get_report_params();
	$transient = wisdom_get_transient_name( $params );
	$data = get_transient( $transient );
	if( isset( $data['reasons'] ) ) {
		return $data['reasons'];
	}

	return false;
}

/**
 * Get plugin options
 *
 * @since 1.1.0
 * @return Array
*/
function wisdom_get_plugin_options( $plugin=array(), $params=array() ) {
	if( empty( $params ) ) {
		$params = wisdom_get_report_params();
	}
	$transient = wisdom_get_transient_name( $params );
	$data = get_transient( $transient );
	if( empty( $plugin ) ) {
		$plugin = $params['wisdom_plugin'];
	}
	if( isset( $data['options'][$plugin] ) ) {
		$options = $data['options'][$plugin];
		return $options;
	}

	return false;
}

/**
 * Return metafield data by key and value
 * @since 1.1.0
 * @param $transient	The transient to search in
 * @param $key			The key to look for
 * @return Mixed
*/
function wisdom_get_meta_data_from_transient( $transient, $key ) {
	$stored_data = get_transient( $transient );
	error_log( 'ke'.$key);
	error_log( print_r( $stored_data, true ) );

	if( ! empty( $stored_data[$key] ) ) {
		error_log('xxx');
		error_log(print_r( $stored_data[$key], true ) );
		return $stored_data[$key];
	}
	return false;
}

/**
 * Returns list of other active plugins
 * @since 1.2.0
 * @return Mixed
*/
function wisdom_get_other_active_plugins() {
	$params = wisdom_get_report_params();
	$transient = wisdom_get_transient_name( $params );
	$data = get_transient( $transient );
	if( isset( $data['other_plugins'] ) ) {
		$plugins = $data['other_plugins'];
		arsort( $plugins );
		// Get number of results to return
		$show_top = isset( $params['wisdom_return_top'] ) ? $params['wisdom_return_top'] : '20';
		$top_plugins = array_slice( $plugins, 0, absint( $show_top ), true );
		return $top_plugins;
	}
	return false;
}

/**
 * Get WP_Query object of plugins where specified meta_key is not empty
 *
 * @since 1.0.0
 * @return Array
*/
function wisdom_get_by_meta( $meta_key, $plugin=null ) {

	$ids = wisdom_get_query_ids();
	if( ! $ids ) {
		return 0;
	}

	// Query the tracked-plugins
	$args = array(
		'post_type'			=> 'tracked-plugin',
		'posts_per_page'	=> -1,
		'post__in'			=> $ids,
		'fields'			=> 'ids'
	);

	// If we haven't passed a value in for the plugin, see if it's set in the URL
	if( empty( $plugin ) ) {
		$plugin = isset( $_GET['plugin'] ) ? $_GET['plugin'] : 'all';
	}

	$args['meta_query'] = array();

	// Only query the specified plugin
	if( ! empty( $plugin ) && $plugin != 'all' ) {
		$args['meta_query'][] = array(
			'key'		=> 'wisdom_plugin_slug',
			'value'		=> $plugin,
			'compare'	=> '='
		);
	}

	// Only query if the meta_key is not empty
	$args['meta_query'][] = array(
		'key'		=> $meta_key,
		'value'		=> '',
		'compare'	=> '!='
	);

	// Query dates if specified
	// Dates are recorded as UNIX timestamps
	if( isset( $_GET['wisdom_start_date'] ) && $_GET['wisdom_start_date'] > 0 ) {
		$start = strtotime( $_GET['wisdom_start_date'] );
		$args['meta_query'][] = array(
			'key'		=> 'wisdom_first_recorded',
			'value'		=> $start,
			'type'		=> 'numeric',
			'compare'	=> '>='
		);
	}
	if( isset( $_GET['wisdom_end_date'] ) && $_GET['wisdom_end_date'] > 0 ) {
		$end = strtotime( $_GET['wisdom_end_date'] );
		$args['meta_query'][] = array(
			'key'		=> 'wisdom_first_recorded',
			'value'		=> $end,
			'type'		=> 'numeric',
			'compare'	=> '<='
		);
	}

	// Get the WP_Query object
	$plugins = new WP_Query( $args );
	return $plugins;
}



/**
 * Get array of specified meta_key
 * Count all meta_values
 *
 * @since 1.0.0
 * @return Array
*/
function wisdom_breakdown_meta( $meta_key, $plugin=null ) {

	$ids = wisdom_get_query_ids();
	if( ! $ids ) {
		return 0;
	}

	// Query the tracked-plugins
	$args = array(
		'post_type'			=> 'tracked-plugin',
		'posts_per_page'	=> -1,
		'post__in'			=> $ids,
		'fields'			=> 'ids'
	);

	// If we haven't passed a value in for the plugin, see if it's set in the URL
	if( empty( $plugin ) ) {
		$plugin = isset( $_GET['plugin'] ) ? $_GET['plugin'] : 'all';
	}

	$args['meta_query'] = array();

	// Only query the specified plugin
	if( ! empty( $plugin ) && $plugin != 'all' ) {
		$args['meta_query'][] = array(
			'key'		=> 'wisdom_plugin_slug',
			'value'		=> $plugin,
			'compare'	=> '='
		);
	}

	// Query dates if specified
	// Dates are recorded as UNIX timestamps
	if( isset( $_GET['wisdom_start_date'] ) && $_GET['wisdom_start_date'] > 0 ) {
		$start = strtotime( $_GET['wisdom_start_date'] );
		$args['meta_query'][] = array(
			'key'		=> 'wisdom_first_recorded',
			'value'		=> $start,
			'type'		=> 'numeric',
			'compare'	=> '>='
		);
	}
	if( isset( $_GET['wisdom_end_date'] ) && $_GET['wisdom_end_date'] > 0 ) {
		$end = strtotime( $_GET['wisdom_end_date'] );
		$args['meta_query'][] = array(
			'key'		=> 'wisdom_first_recorded',
			'value'		=> $end,
			'type'		=> 'numeric',
			'compare'	=> '<='
		);
	}

	// Grab all meta_values and count each
	// E.g: $meta_values = array(
	// 		'pl_PL' => 8,
	//		'en_US'	=> 66,
	// );
	$meta_values = array();
	$plugins = new WP_Query( $args );
	$total_set = 0;
	if( $plugins->have_posts() ) {
		// Count all with this meta_key set so that we can count missing
		global $post;
		while( $plugins->have_posts() ) : $plugins->the_post();
			$meta_value = get_post_meta( $post->ID, $meta_key, true );
			// See if we already have this plugin recorded
			if( ! empty( $meta_value ) ) {
				if( ! isset( $meta_values[$meta_value] ) ) {
					// If we haven't got this one yet, add it and start counting
					$meta_values[$meta_value] = 1;
				} else {
					// Increment it
					$meta_values[$meta_value]++;
				}
				$total_set++;
			}
		endwhile;
		wp_reset_query();
	}

	$missing = $plugins->post_count - $total_set;
	// Only include Not Set if there are missing values
	if( $missing > 0 ) {
		$meta_values['Not set'] = $missing;
	}
	return $meta_values;
}

/**
 * Get array of specified meta_key
 *
 * @since 1.0.0
 * @param $meta_key	key to query by
 * @param $meta_value value of meta_key to query for
 * @param $plugin plugin to query on
 * @return Posts Object
*/
function wisdom_breakdown_key_by_value( $meta_key, $meta_value, $plugin=null ) {

	$ids = wisdom_get_query_ids();
	if( ! $ids ) {
		return 0;
	}

	// Query the tracked-plugins
	$args = array(
		'post_type'			=> 'tracked-plugin',
		'post__in'			=> $ids
	);

	// If we haven't passed a value in for the plugin, see if it's set in the URL
	if( empty( $plugin ) ) {
		$plugin = isset( $_GET['plugin'] ) ? $_GET['plugin'] : 'all';
	}

	$args['meta_query'] = array();

	// Only query the specified plugin
	if( ! empty( $plugin ) && $plugin != 'all' ) {
		$args['meta_query'][] = array(
			'key'		=> 'wisdom_plugin_slug',
			'value'		=> $plugin,
			'compare'	=> '='
		);
	}

	// Query the key by value
	if( isset( $meta_key ) && isset( $meta_value ) ) {
		$args['meta_query'][] = array(
			'key'		=> $meta_key,
			'value'		=> $meta_value,
			'compare'	=> '='
		);
	}

	// Query dates if specified
	// Dates are recorded as UNIX timestamps
	if( isset( $_GET['wisdom_start_date'] ) && $_GET['wisdom_start_date'] > 0 ) {
		$start = strtotime( $_GET['wisdom_start_date'] );
		$args['meta_query'][] = array(
			'key'		=> 'wisdom_first_recorded',
			'value'		=> $start,
			'type'		=> 'numeric',
			'compare'	=> '>='
		);
	}
	if( isset( $_GET['wisdom_end_date'] ) && $_GET['wisdom_end_date'] > 0 ) {
		$end = strtotime( $_GET['wisdom_end_date'] );
		$args['meta_query'][] = array(
			'key'		=> 'wisdom_first_recorded',
			'value'		=> $end,
			'type'		=> 'numeric',
			'compare'	=> '<='
		);
	}

	$plugins = new WP_Query( $args );

	return $plugins;
}

/**
 * Get array of specified meta_key
 * Complex, like JSON arrays or objects
 *
 * @since 1.0.0
 * @return Integer
 * @param $meta_key 	The meta_key to query
 * @param $plugin		Pass a plugin slug if required
 * @param $date_field	By default, wisdom_first_recorded but could be wisdom_deactivated_date
*/
function wisdom_json_array_meta( $meta_key, $plugin=null, $date_field='wisdom_first_recorded' ) {

	$ids = wisdom_get_query_ids();
	if( ! $ids ) {
		return 0;
	}

	// Query the tracked-plugins
	$args = array(
		'post_type'			=> 'tracked-plugin',
		'post__in'			=> $ids
	);

	// If we haven't passed a value in for the plugin, see if it's set in the URL
	if( empty( $plugin ) ) {
		$plugin = isset( $_GET['plugin'] ) ? $_GET['plugin'] : 'all';
	}

	$args['meta_query'] = array();

	// Only query the specified plugin
	if( ! empty( $plugin ) && $plugin != 'all' ) {
		$args['meta_query'][] = array(
			'key'		=> 'wisdom_plugin_slug',
			'value'		=> $plugin,
			'compare'	=> '='
		);
	}

	// Query dates if specified
	// Dates are recorded as UNIX timestamps
	if( isset( $_GET['wisdom_start_date'] ) && $_GET['wisdom_start_date'] > 0 ) {
		$start = strtotime( $_GET['wisdom_start_date'] );
		$args['meta_query'][] = array(
			'key'		=> $date_field,
			'value'		=> $start,
			'type'		=> 'numeric',
			'compare'	=> '>='
		);
	}
	if( isset( $_GET['wisdom_end_date'] ) && $_GET['wisdom_end_date'] > 0 ) {
		$end = strtotime( $_GET['wisdom_end_date'] );
		$args['meta_query'][] = array(
			'key'		=> $date_field,
			'value'		=> $end,
			'type'		=> 'numeric',
			'compare'	=> '<='
		);
	}

	// Grab all fields containing the meta_value
	// E.g: $meta_values = array(
	// 		[0] => Array ( [0] => Set up is too difficult [1] => Found a better plugin [2] => Didn't work ),
	//		[1] => Array ( [0] => Found a better plugin [2] => Didn't work ),
	//		[2] => Array ( [0] => Didn't work )
	// );
	$fields = array();
	$plugins = new WP_Query( $args );
	if( $plugins->have_posts() ) {
		// Count all with this meta_key set so that we can count missing
		global $post;
		while( $plugins->have_posts() ) : $plugins->the_post();
			$meta_value = get_post_meta( $post->ID, $meta_key, true );
			$meta_value = json_decode( $meta_value );
			// Add it to our list
			if( ! empty( $meta_value ) ) {
				$fields[] = $meta_value;
			}
		endwhile;
		wp_reset_query();
	}

	return $fields;
}

/**
 * Get array of specified meta_key on a given date or date range
 * Use this to compile daily, weekly, monthly reports
 *
 * @since 1.0.0
 * @return Integer
*/
function wisdom_breakdown_meta_by_time( $args, $start, $end, $plugin=null ) {

	// If key and value not passed then bounce
	if( ! isset( $args['meta_key'] ) || ! isset( $args['meta_value'] ) ) {
		return;
	}
	$meta_key = $args['meta_key'];
	$meta_value = $args['meta_value'];

	$ids = wisdom_get_query_ids();
	if( ! $ids ) {
		return 0;
	}

	// Query the tracked-plugins
	$args = array(
		'post_type'			=> 'tracked-plugin',
		'post__in'			=> $ids
	);

	// If we haven't passed a value in for the plugin, see if it's set in the URL
	if( empty( $plugin ) ) {
		$plugin = isset( $_GET['plugin'] ) ? $_GET['plugin'] : 'all';
	}

	$query_args['meta_query'] = array();

	// Firstly, set meta_query for defined key and value
	$query_args['meta_query'][] = array(
		'key'		=> $meta_key,
		'value'		=> $meta_value,
		'compare'	=> '='
	);

	// Next, only query a plugin if specified
	if( ! empty( $plugin ) && $plugin != 'all' ) {
		$query_args['meta_query'][] = array(
			'key'		=> 'wisdom_plugin_slug',
			'value'		=> $plugin,
			'compare'	=> '='
		);
	}

	// Query dates if specified
	// Dates are recorded as UNIX timestamps
	$query_args['meta_query'][] = array(
		'key'		=> 'wisdom_first_recorded',
		'value'		=> $start,
		'type'		=> 'numeric',
		'compare'	=> '>='
	);

	$query_args['meta_query'][] = array(
		'key'		=> 'wisdom_first_recorded',
		'value'		=> $end,
		'type'		=> 'numeric',
		'compare'	=> '<='
	);

	// Grab all meta_values and count each
	// E.g: $meta_values = array(
	// 		'pl_PL' => 8,
	//		'en_US'	=> 66,
	// );
	$meta_values = array();
	$plugins = new WP_Query( $query_args );
	$total_set = 0;
	if( $plugins->have_posts() ) {
		// Count all with this meta_key set so that we can count missing
		global $post;
		while( $plugins->have_posts() ) : $plugins->the_post();
			$meta_value = get_post_meta( $post->ID, $meta_key, true );
			// See if we already have this plugin recorded
			if( ! empty( $meta_value ) ) {
				if( ! isset( $meta_values[$meta_value] ) ) {
					// If we haven't got this one yet, add it and start counting
					$meta_values[$meta_value] = 1;
				} else {
					// Increment it
					$meta_values[$meta_value]++;
				}
				$total_set++;
			}
		endwhile;
		wp_reset_query();
	}

	$missing = $plugins->post_count - $total_set;
	// Only include Not Set if there are missing values
	if( $missing > 0 ) {
		$meta_values['Not set'] = $missing;
	}
	return $meta_values;
}

/**
 * Generate a random RGB color value
 * @todo Create a list of preferred color values to use before having to use random colors
 *
 * @since 1.0.0
 * @return String
*/
function wisdom_generate_random_rgb(){
	$r = rand( 0, 255 );
	$g = rand( 0, 255 );
	$b = rand( 0, 255 );
	return $r . ',' . $g . ',' . $b;
}

/**
 * A list of nice colors
 *
 * @since 1.0.0
 * @link https://flatuicolors.com/
 * @return String
*/
function wisdom_nice_colors(){
	$colors = array(
		'26, 188, 156',
		'52, 152, 219',
		'155, 89, 182',
		'52, 73, 94',
		'22, 160, 133',
		'41, 128, 185',
		'142, 68, 173',
		'44, 62, 80',
		'241, 196, 15',
		'230, 126, 34',
		'231, 76, 60',
		'149, 165, 166',
		'243, 156, 18',
		'211, 84, 0',
		'192, 57, 43',
		'39, 174, 96',
		'127, 140, 141',
		'46, 204, 113',
	);
	return $colors;
}

/**
 * Get all options returned for a plugin
 *
 * @since 1.0.0
 * @todo Need to save this in a transient
 * @todo Instead of refreshing the transient every time new data comes in, allow user to view cached data or clear the cache
 * @return Array
*/
function wisdom_all_plugin_options(){
	$current_plugin = isset( $_GET['plugin'] ) ? $_GET['plugin'] : 'all';
	// We can only query one plugin at a time
	if( $current_plugin != 'all' ) {
		// Get a WP_Query object for this plugin
		$plugins = wisdom_get_by_meta( 'wisdom_plugin_options_fields', $current_plugin );
		// We're going to collect the options in this array
		$plugin_options = array();
		global $post;
		if( $plugins->have_posts() ) {
			while( $plugins->have_posts() ) : $plugins->the_post();
			// Get the options field for the tracked plugin
			$options = get_post_meta( $post->ID, 'wisdom_plugin_options_fields', true );
			// If we have an array of options then iterate through them and record them
			if( ! empty( $options ) && is_array( $options ) ) {
				foreach( $options as $key=>$value ) {
					// Check if we've got this option name yet
					// We don't bother recording wisdom_registered_setting
					if( ! isset( $plugin_options[$key] ) ) {
						// Create a new element in the array
						// Capture the key, obviously
						// Then start making an array of values and the number of times they appear
						$plugin_options[$key] = array(
							'key'		=> $key,
							'values'	=> array(
								$value	=> 1
							)
						);
					} else {
						// We already have this option name recorded
						// Get the array of values that have already been recorded for this key
						$values = $plugin_options[$key]['values'];
						if( ! isset( $plugin_options[$key]['values'][$value] ) ) {
							// If we haven't seen this value yet, then add it to the other values and start counting it
							$plugin_options[$key]['values'][$value] = 1;
						} else {
							// We have this value already so we increment the count
							$plugin_options[$key]['values'][$value]++;
						}
					}
				}
			}
			endwhile;

		}
		return $plugin_options;
		wp_reset_query();
	}
	return null;
}

/**
 * Get all metafields
 *
 * @since 1.0.0
 * @todo Need to save this in a transient
 * @return Array
*/
function wisdom_metafields(){
	$metafields = array();
	$metaboxes = wisdom_metaboxes();
	if( ! empty( $metaboxes ) ) {
		$count = 0;
		foreach( $metaboxes as $metabox ) {
			if( isset( $metabox['fields'] ) && is_array( $metabox['fields'] ) ) {
				foreach( $metabox['fields'] as $metafield ) {
					// Don't include arrays and only include fields with report set to true
					if( isset( $metafield['type'] ) && $metafield['type'] != 'array' && isset( $metafield['report'] ) && $metafield['report'] == true ) {
						$metafields[$metafield['ID']] = $metafield['title'];
					}
				}
			}
		}
	}
	return $metafields;
}

/**
 * Check validity of Mailchimp API key
 *
 * @since 1.2.0
 * @return Integer
*/
function wisdom_validate_mc_key( $key ) {
	// Get Mailchimp API key
	$MailChimp = new MailChimp( $key );
	$lists = $MailChimp->get('lists');
	$response = $MailChimp->getLastResponse();
	return $response['headers']['http_code'];
}

/**
 * Get option key names
 *
 * @since 1.2.1
 * @return Array
*/
function wisdom_pluck_option_keys( $plugin_options ) {
	$options = array();
	if( $plugin_options ) {
		foreach( $plugin_options as $key=>$value ) {
			$options[$key] = $key;
		}
	}
	return $options;
}

/**
 * Get list of plugin options via AJAX
 *
 * @since 1.2.1
 * @return Array
*/
function wisdom_get_options_by_plugin() {

	parse_str( $_POST['form'], $form );

	if( ! isset( $_POST['wisdom_chart_options'] ) || ! wp_verify_nonce( $_POST['wisdom_chart_options'], 'wisdom_chart_options' ) ) {
		wp_send_json( array( 'result' => 'error' ) );
		exit;
	}

	$plugin = $_POST['plugin'];

	$params = array();
	$params['wisdom_report'] = isset( $form['wisdom_report'] ) ? $form['wisdom_report'] : 'summary';
	$params['wisdom_plugin'] = isset( $form['wisdom_plugin'] ) ? $form['wisdom_plugin'] : 'all';
	$params['wisdom_start_date'] = isset( $form['wisdom_start_date'] ) ? $form['wisdom_start_date'] : '';
	$params['wisdom_end_date'] = isset( $form['wisdom_end_date'] ) ? $form['wisdom_end_date'] : '';
	$params['interval'] = isset( $form['interval'] ) ? $form['interval'] : '';
	$params['wisdom_return_top'] = isset( $form['wisdom_return_top'] ) ? $form['wisdom_return_top'] : '';


	$options = wisdom_get_plugin_options( $plugin, $params );
	$html = array();
	if( $options ) {
		foreach( $options as $key=>$value ) {
			$html[] = '<option value="' . esc_attr( $key ) . '">' . esc_attr( $key ) . '</option>';
		}
		$html = join( '', $html );
	}

	wp_send_json( array(
		'options'	=> $options,
		'params'	=> $params,
		'form'		=> $form,
		'html'		=> $html,
		'plugin'	=> $plugin
	) );

	exit;

}
add_action( 'wp_ajax_wisdom_get_options_by_plugin', 'wisdom_get_options_by_plugin' );

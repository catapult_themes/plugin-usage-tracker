<?php
/**
 * Languages chart
 *
 * @package Wisdom Plugin
 * @since 1.0.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This is a simple Active/Deactivated chart
 * All plugins
 * No time period defined
 * @since 1.0.0
 */
function wisdom_active_languages() {
	// Gets an array of all languages and number of sites using each language
	$languages = wisdom_breakdown_meta( 'wisdom_site_language' );
	$count = 0; // Keep track of how many languages
	$colors = wisdom_nice_colors();
	$number_colors = count( $colors );
	if( ! empty( $languages ) ) {
		$lang_labels = array();
		$lang_data = array();
		$bg_colors = array();
		$border_colors = array();
		foreach( $languages as $key=>$value ) {
			$lang_labels[] = '"' . esc_attr( $key ) . '"';
			$lang_data[] = esc_attr( $value );
			// Decide which color to use
			if( $count > $number_colors ) {
				// Use a random one
				$color = wisdom_generate_random_rgb();
			} else {
				$color = $colors[$count];
			}
			$bg_colors[] = '"rgba(' . $color . ',0.75' . ')"';
			$border_colors[] = '"rgba(' . $color . ',1' . ')"';
			$count++;
		}
		$labels = '[' . join( ',', $lang_labels ) . ']';
		$data = '[' . join( ',', $lang_data ) . ']';
		$bg = '[' . join( ',', $bg_colors ) . ']';
		$border = '[' . join( ',', $border_colors ) . ']';
	} else {
		_e( 'No data found for this report', 'wisdom-plugin' );
		return;
	}
	
	// Create a table to display this data
	?>
	<div id="wisdom-summary-wrapper" class="wisdom-summary-totals">
		<table class="form-table wisdom-language-table">
			<tbody>
				<?php foreach( $languages as $key=>$value ) { ?>
				<tr>
					<th scope="row"><?php echo esc_html( $key ); ?></th>
					<td><?php echo esc_html( $value ); ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div><!-- .wisdom-summary-totals -->

	<div id="wisdom-charts-wrapper" class="wisdom-charts">
		<div class="canvas-wrapper">
			<div style="position: relative; ">
				<canvas id="chartOne" width="400" height="400"></canvas>
			</div>
			<div style="position: relative; width: 49%;">
				<canvas id="chartTwo" width="400" height="200"></canvas>
			</div>
		</div>
	
		<?php wisdom_active_summary(); ?>
	
	</div>
	

	<script>
		jQuery(document).ready(function($){
			// Let's set some vars
			var type = 'doughnut';
			var labels = <?php echo $labels; ?>;
			var data = <?php echo $data; ?>;
			var bg = <?php echo $bg; ?>;
			var border = <?php echo $border; ?>;
			
			var ctx = document.getElementById("chartOne").getContext("2d");
			var myChart = new Chart(ctx, {
			    type: type,
			    data: {
			        labels: labels,
			        datasets: [
						{
							data: data,
							backgroundColor: bg,
							borderColor: border,
							borderWidth: 3
						}
					]
			    }
			});
		});
	</script>
<?php }
<?php
/**
 * Deactivation rate by version
 * Find each version of the specified plugin
 * Then get how many active and deactivated instances there are
 *
 * @package Wisdom Plugin
 * @since 1.0.0
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This is a bar chart showing deactivation rate by version
 * Single plugin only
 * @since 1.0.0
 */
function wisdom_deactivations_rate_by_version() {
	printf( 
		'<h2>%s</h2>', 
		 __( 'Deactivation rate by version number', 'wisdom-plugin' )
	);
	$current_plugin = isset( $_GET['plugin'] ) ? $_GET['plugin'] : 'all';
	if( $current_plugin == 'all' ) {
		echo '<p>' . __( 'Please select a plugin', 'wisdom-plugin' ) . '</p>';
		return;
	}
	// Gets the wp_query object of deactivated sites for this plugin
	$deactivations = wisdom_breakdown_key_by_value(
		'wisdom_status', 			// meta_key for query
		'Deactivated' 			// meta_value for query
	);
	
	// Iterate through the object to find current version, activation time and deactivation time
	$deactivation_versions = array();	// Array of version numbers
	$deactivation_times = array();		// Array of total activation time per version
	
	if( $deactivations->have_posts() ) {
		global $post;
		while( $deactivations->have_posts() ) : $deactivations->the_post();
			// We're looking for the version number and the time between activating and deactivating
			$version = get_post_meta( $post->ID, 'wisdom_current_version', true );
			$activated = get_post_meta( $post->ID, 'wisdom_first_recorded', true );
			$deactivated = get_post_meta( $post->ID, 'wisdom_deactivated_date', true );
			$time_active = '';
			if( ! empty( $activated ) && ! empty( $deactivated ) ) {
				// Number of seconds active
				$time_active = intval( $deactivated ) - intval( $activated );
			}
			// See if we already have this plugin recorded
			if( ! empty( $version ) && ! empty( $time_active ) ) {
				if( ! isset( $deactivation_versions[$version] ) ) {
					// If we haven't got this one yet, add it and start counting
					$deactivation_versions[$version] = array(
						'version'		=> $version,
						'count'			=> 1,
						'time_active'	=> intval( $time_active )
					);
				} else {
					// Increment it
					$deactivation_versions[$version]['count'] ++;
					$deactivation_versions[$version]['time_active'] += intval( $time_active );
				}
			}
		endwhile;
		wp_reset_query();
	}

	$count = 0; // Keep track of how many languages
	$colors = wisdom_nice_colors();
	$number_colors = count( $colors );

	// Now we have to get the average activation time for each version
	// It's a lot of work
	if( ! empty( $deactivation_versions ) ) {
		// Reverse the array so that earlier versions are plotted first
		$deactivation_versions = array_reverse( $deactivation_versions );
		// Data for the chart
		$averages = array();
		$version_labels = array();
		$reason_data = array();
		$bg_colors = array();
		$border_colors = array();
		
		foreach( $deactivation_versions as $version ) {
			// Average time per version in hours
			$averages[] = '"' . round( intval( $version['time_active'] ) / intval( $version['count'] ) / 360 ) . '"';
			$version_labels[] = '"' . sanitize_text_field( $version['version'] ) . '"';

			// Decide which color to use
			if( $count > $number_colors ) {
				// Use a random one
				$color = wisdom_generate_random_rgb();
			} else {
				$color = $colors[$count];
			}
			$bg_colors[] = '"rgba(' . $color . ',0.75' . ')"';
			$border_colors[] = '"rgba(' . $color . ',1' . ')"';
			$count++;
		}
		$labels = '[' . join( ',', $version_labels ) . ']';
		$data = '[' . join( ',', $averages ) . ']';
		$bg = '[' . join( ',', $bg_colors ) . ']';
		$border = '[' . join( ',', $border_colors ) . ']';
	} else {
		_e( 'No data found for this report', 'wisdom-plugin' );
		return;
	}
	
	// Create a table to display this data
	if( ! empty( $deactivation_versions ) ) { ?>
		<div id="wisdom-summary-wrapper" class="wisdom-summary-totals">
			<table class="form-table wisdom-language-table">
				<tbody>
					<tr>
						<th scope="row"><?php _e( 'Version', 'wisdom-plugin' ); ?></th>
						<th scope="row"><?php _e( 'Hours', 'wisdom-plugin' ); ?></th>
					</tr>
					<?php foreach( $deactivation_versions as $version ) { ?>
					<tr>
						<th scope="row"><?php echo esc_html( $version['version'] ); ?></th>
						<td><?php echo round( intval( $version['time_active'] ) / intval( $version['count'] ) / 360 ); ?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div><!-- .wisdom-summary-totals -->
	
	<?php } ?>

	<div id="wisdom-charts-wrapper" class="wisdom-charts">
		<div class="canvas-wrapper">
			<div style="position: relative; ">
				<canvas id="chartOne" width="400" height="400"></canvas>
			</div>
			<div style="position: relative; width: 49%;">
				<canvas id="chartTwo" width="400" height="200"></canvas>
			</div>
		</div>
	</div>
	

	<script>
		jQuery(document).ready(function($){
			// Let's set some vars
			var type = 'bar';
			var labels = <?php echo $labels; ?>;
			var data = <?php echo $data; ?>;
			var bg = <?php echo $bg; ?>;
			var border = <?php echo $border; ?>;
			
			var ctx = document.getElementById("chartOne").getContext("2d");
			var myChart = new Chart(ctx, {
			    type: type,
			    data: {
			        labels: labels,
			        datasets: [
						{
							label: '',
							data: data,
							backgroundColor: bg,
							borderColor: border,
							borderWidth: 3
						}
					]
			    },
				options: {
					scales: {
						xAxes: [{
							scaleLabel: {
								display: true,
								labelString: "<?php _e( 'Version number', 'wisdom-plugin' ); ?>"
							}
						}],
						yAxes: [{
							scaleLabel: {
								display: true,
								labelString: "<?php _e( 'Average hours before deactivating', 'wisdom-plugin' ); ?>"
							}
						}]
					}
				}
			});
		});
	</script>
<?php }
<?php
/**
 * Reports summary page
 * The default page under Reports
 *
 * @package Wisdom Plugin
 * @since 1.0.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Outputs the content for the summary report
 * @since 1.0.0
 */
function wisdom_chart_summary() {
	$summaries = array(
		'sites'			=> array(
			'title'		=> __( 'Total Sites Tracked', 'wisdom-plugin' ),
			'callback'	=> 'wisdom_get_total_sites_summary',
			'args'		=> array()
		),
		'plugins'		=> array(
			'title'		=> __( 'Total Products Tracked', 'wisdom-plugin' ),
			'callback'	=> 'wisdom_get_total_plugins_summary',
			'args'		=> array()
		),
		'active'		=> array(
			'title'		=> __( 'Total Products Active', 'wisdom-plugin' ),
			'callback'	=> 'wisdom_get_total_by_meta_summary',
			'args'		=> array(
				'meta_key'		=> 'wisdom_status',
				'meta_value'	=> 'Active'
			)
		),
		'deactivated'	=> array(
			'title'		=> __( 'Total Products Deactivated', 'wisdom-plugin' ),
			'callback'	=> 'wisdom_get_total_by_meta_summary',
			'args'		=> array(
				'meta_key'		=> 'wisdom_status',
				'meta_value'	=> 'Deactivated'
			)
		)
	);
	?>
	<div id="wisdom-summary-wrapper" class="wisdom-summary-totals">
		<?php foreach( $summaries as $summary ) { ?>
			<table class="form-table wisdom-summary-table">
				<tbody>
					<tr>
						<th scope="row"><?php echo esc_html( $summary['title'] ); ?></th>
						<td><?php echo esc_html( call_user_func( $summary['callback'], $summary['args'] ) ); ?></td>
					</tr>
				</tbody>
			</table>
		<?php } ?>
		<?php // Get average deactivation rate
		if( isset( $summaries['active'] ) && isset( $summaries['deactivated'] ) ) {
			$active = call_user_func( $summaries['active']['callback'], $summaries['active']['args'] );
			$deactivated = call_user_func( $summaries['deactivated']['callback'], $summaries['deactivated']['args'] );
			$total = intval( $active ) + intval( $deactivated );
			// Avoid dividing by 0
			if( $deactivated == 0 ) {
				$average = 0;
			} else {
				$average = round( ( $deactivated / $total ) * 100, 1 )  . '%';
			}
			?>
			<table class="form-table wisdom-summary-table">
				<tbody>
					<tr>
						<th scope="row"><?php _e( 'Deactivation Rate'); ?></th>
						<td><?php echo esc_html( $average ); ?></td>
					</tr>
				</tbody>
			</table>
		<?php } ?>
	</div><!-- .wisdom-summary-totals -->

	<div id="wisdom-charts-wrapper" class="wisdom-charts">
		<div class="canvas-wrapper">
			<div style="position: relative; ">
				<canvas id="chartOne" width="400" height="400"></canvas>
			</div>
			<div style="position: relative; width: 49%;">
				<canvas id="chartTwo" width="400" height="200"></canvas>
			</div>
		</div>
	
		<?php wisdom_report_summary(); ?>
	
	</div>
	
	
<?php }

/**
 * This is a simple Active/Deactivated chart
 * All plugins
 * No time period defined
 * @since 1.0.0
 */
function wisdom_report_summary() {
	$total_active = wisdom_get_total_by_meta_summary( array( 'meta_key' => 'wisdom_status', 'meta_value' => 'Active' ) );
	$total_deactive = wisdom_get_total_by_meta_summary( array( 'meta_key' => 'wisdom_status', 'meta_value' => 'Deactivated' ) );
	$labels = '["' . __( 'Active', 'wisdom-plugin' ) .'","' . __( 'Deactivated', 'wisdom-plugin' ) .'"]';
	?>
	<script>
		jQuery(document).ready(function($){
			function generateRandomColors(numColors){
				var allColors, color, count;
				allColors=[];
				count = 0;
				do {
					color=new Array();
					for( var i=0; i<3; i++ ) {
						color.push(Math.floor(Math.random()*256));
					}
					allColors.push(color.join());
					count++
				}
				while (count<numColors);
			}
			
			// Let's set some vars
			var type = 'doughnut';
			var labels = <?php echo $labels; ?>;
			var data = [<?php echo $total_active; ?>, <?php echo $total_deactive; ?>];
			
			var ctx = document.getElementById("chartOne").getContext("2d");
			var myChart = new Chart(ctx, {
			    type: type,
			    data: {
			        labels: labels,
			        datasets: [
						{
							data: data,
							backgroundColor: [
								'rgba(255, 99, 132, 0.5)',
								'rgba(75, 192, 192, 0.5)'
							],
							borderColor: [
								'rgba(255, 99, 132, 1)',
								'rgba(75, 192, 192, 1)'
							],
							borderWidth: 3
						}
					]
			    }
			});
		});
	</script>
<?php }
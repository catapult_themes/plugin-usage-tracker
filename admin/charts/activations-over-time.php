<?php
/**
 * Reports page for Active / Deactivated plugins
 *
 * @package Wisdom Plugin
 * @since 1.0.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Outputs the content for the summary report
 * @since 1.0.0
 */
function wisdom_activations_over_time() {
	$current_start = isset( $_GET['wisdom_start_date'] ) ? $_GET['wisdom_start_date'] : '';
	$current_end = isset( $_GET['wisdom_end_date'] ) ? $_GET['wisdom_end_date'] : '';
	if( empty( $current_start ) || empty( $current_end ) ) { ?>
		<p><?php _e( 'Please ensure you\'ve set the dates for the report', 'wisdom-plugin' ); ?></p>
	<?php }
	if( $current_start > $current_end ) {?>
		<p><?php _e( 'Please ensure the end date is after the start date', 'wisdom-plugin' ); ?></p>
	<?php }
	$summaries = array(
		'active'		=> array(
			'title'		=> __( 'Active', 'wisdom-plugin' ),
			'callback'	=> 'wisdom_get_total_by_meta_summary',
			'args'		=> array(
				'meta_key'		=> 'wisdom_status',
				'meta_value'	=> 'Active'
			)
		),
		'deactivated'	=> array(
			'title'		=> __( 'Deactivated', 'wisdom-plugin' ),
			'callback'	=> 'wisdom_get_total_by_meta_summary',
			'args'		=> array(
				'meta_key'		=> 'wisdom_status',
				'meta_value'	=> 'Deactivated'
			)
		)
	); ?>
	<div id="wisdom-summary-wrapper" class="wisdom-summary-totals">
		<?php foreach( $summaries as $summary ) { ?>
			<table class="form-table wisdom-summary-table">
				<tbody>
					<tr>
						<th scope="row"><?php echo $summary['title']; ?></th>
						<td><?php echo call_user_func( $summary['callback'], $summary['args'] ); ?></td>
					</tr>
				</tbody>
			</table>
		<?php } ?>
	</div><!-- .wisdom-summary-totals -->

	<div id="wisdom-charts-wrapper" class="wisdom-charts">
		<div class="canvas-wrapper">
			<div style="position: relative; ">
				<canvas id="chartOne" width="400" height="400"></canvas>
			</div>
			<div style="position: relative; width: 49%;">
				<canvas id="chartTwo" width="400" height="200"></canvas>
			</div>
		</div>
	
		<?php wisdom_activations_over_time_chart(); ?>
	
	</div>
	
	
<?php }

/**
 * This is a line chart by day
 * @todo set unit, e.g. daily, weekly, monthly
 * @since 1.0.0
 */
function wisdom_activations_over_time_chart() {

	// Figure out our date range
	// Dates are recorded as UNIX timestamps
	if( isset( $_GET['wisdom_start_date'] ) && $_GET['wisdom_start_date'] > 0 ) {
		$start = strtotime( $_GET['wisdom_start_date'] );
	}
	if( isset( $_GET['wisdom_end_date'] ) && $_GET['wisdom_end_date'] > 0 ) {
		$end = strtotime( $_GET['wisdom_end_date'] );
	}
	if( ! isset( $start ) || ! isset( $end ) ) {
		return;
	}
	if( $start > $end ) {
		return;
	}
	
	// Determine the interval
	$interval = isset( $_GET['interval'] ) ? $_GET['interval'] : 'daily';
	if( $interval == 'daily' ) {
		// Daily
		// 86400 seconds in 24 hours
		$increment = 86400;
		
	} else if( $interval == 'weekly' ) {
		// Weekly
		$increment = 604800;
	} else {
		// Monthly
		$increment = 604800;
	}
	
	$length = $end - $start;
	// Calculate the number of points to plot
	$points = ( $length/$increment ) + 1;

	// We iterate over the time period at the intervals specified
	$labels = array();
	$data = array(); // Record Active data
	$d_data = array(); // Record Deactivated data
	
	$highest_y_value = 0;
	
	for( $x = 0; $x < $points; $x++ ) {
		$interval_start = $start + ( $x * $increment );
		$interval_end = $interval_start + ( $increment - 1 );
		
		$total_active = wisdom_get_activations_per_day( 'first_rec', $interval_start+1, $interval );
		$total_deactive = wisdom_get_activations_per_day( 'deactivated_date', $interval_start+1, $interval );
		
		$highest_y_value = max( $highest_y_value, $total_active, $total_deactive );
		
		$labels[] = '"' . wisdom_format_timestamp( $interval_start ) . '"';
		
		if( isset( $total_active ) ) {
			$data[] = $total_active;
		} else {
			$data[] = 0;
		}
		if( isset( $total_deactive ) ) {
			$d_data[] = $total_deactive;
		} else {
			$d_data[] = 0;
		}
	}
	
	$labels = '[' . join( ',', $labels ) .']';
	$data = '[' . join( ',', $data ) .']';
	$d_data = '[' . join( ',', $d_data ) .']';
	$colors = wisdom_nice_colors();
	$bg_colors = '"rgba(' . $colors[0] . ',0.1' . ')"';
	$border_colors = '"rgba(' . $colors[0] . ',1' . ')"';
	$d_bg_colors = '"rgba(' . $colors[1] . ',0.1' . ')"';
	$d_border_colors = '"rgba(' . $colors[1] . ',1' . ')"';
	
	// Calculate nice value for y-axis ticks
	$y_tick = 1;
	if( $highest_y_value > 20 && $highest_y_value <= 50 ) {
		$y_tick = 5;
	} else if( $highest_y_value > 50 && $highest_y_value <= 250 ) {
		$y_tick = 10;
	} else if( $highest_y_value > 250 && $highest_y_value <= 500 ) {
		$y_tick = 20;
	} else if( $highest_y_value > 50 && $highest_y_value <= 1000 ) {
		$y_tick = 50;
	} else if( $highest_y_value > 1000 && $highest_y_value <= 5000 ) {
		$y_tick = 100;
	} else if( $highest_y_value > 5000 ) {
		$y_tick = 250;
	}
	?>
	<script>
		jQuery(document).ready(function($){
			// Let's set some vars
			var type = 'line';
			var labels = <?php echo $labels; ?>;
			var data = <?php echo $data; ?>;
			var d_data = <?php echo $d_data; ?>;
			var bg = <?php echo $bg_colors; ?>;
			var border = <?php echo $border_colors; ?>;
			var d_bg = <?php echo $d_bg_colors; ?>;
			var d_border = <?php echo $d_border_colors; ?>;
			var y_tick = <?php echo $y_tick; ?>;
			var ctx = document.getElementById("chartOne").getContext("2d");
			var myChart = new Chart(ctx, {
			    type: 'line',
			    data: {
			        labels: labels,
			        datasets: [{
						label: "Active",
						data: data,
						fill: true,
						backgroundColor: bg,
						borderColor: border,
						pointBackgroundColor: border,
						pointBorderColor: "rgba(241,241,241,1)",
						pointBorderWidth: "3",
						pointRadius: "6",
					},
					{
						label: "Deactivated",
						data: d_data,
						fill: true,
						backgroundColor: d_bg,
						borderColor: d_border,
						pointBackgroundColor: d_border,
						pointBorderColor: "rgba(241,241,241,1)",
						pointBorderWidth: "3",
						pointRadius: "6",
					}]
			    },
				options: {
					scales: {
						xAxes: [{
							gridLines: {
								display: false
							}
						}],
						yAxes: [{
							ticks: {
								stepSize: y_tick,
								min: 0
							},
							gridLines: {
								display: false
							}
						}]
					}
				}
			});
		});
	</script>
<?php }
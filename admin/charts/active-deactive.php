<?php
/**
 * Reports page for Active / Deactivated plugins
 *
 * @package Wisdom Plugin
 * @since 1.0.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Outputs the content for the summary report
 * @since 1.0.0
 */
function wisdom_chart_active() {
	$summaries = array(
		'sites'			=> array(
			'title'		=> __( 'Total Sites Tracked', 'wisdom-plugin' ),
			'callback'	=> 'wisdom_get_total_sites_summary',
			'args'		=> array()
		),
		'plugins'		=> array(
			'title'		=> __( 'Plugins Tracked', 'wisdom-plugin' ),
			'callback'	=> 'wisdom_get_total_plugins_summary',
			'args'		=> array()
		),
		'active'		=> array(
			'title'		=> __( 'Active', 'wisdom-plugin' ),
			'callback'	=> 'wisdom_get_total_by_meta_summary',
			'args'		=> array(
				'meta_key'		=> 'wisdom_status',
				'meta_value'	=> 'Active'
			)
		),
		'deactivated'	=> array(
			'title'		=> __( 'Deactivated', 'wisdom-plugin' ),
			'callback'	=> 'wisdom_get_total_by_meta_summary',
			'args'		=> array(
				'meta_key'		=> 'wisdom_status',
				'meta_value'	=> 'Deactivated'
			)
		)
	); ?>
	<div id="wisdom-summary-wrapper" class="wisdom-summary-totals">
		<?php foreach( $summaries as $summary ) { ?>
			<table class="form-table wisdom-summary-table">
				<tbody>
					<tr>
						<th scope="row"><?php echo $summary['title']; ?></th>
						<td><?php echo call_user_func( $summary['callback'], $summary['args'] ); ?></td>
					</tr>
				</tbody>
			</table>
		<?php } ?>
		<?php // Get average deactivation rate
		if( isset( $summaries['active'] ) && isset( $summaries['deactivated'] ) ) {
			$active = call_user_func( $summaries['active']['callback'], $summaries['active']['args'] );
			$deactivated = call_user_func( $summaries['deactivated']['callback'], $summaries['deactivated']['args'] );
			$total = intval( $active ) + intval( $deactivated );
			// Avoid dividing by 0
			if( $deactivated == 0 ) {
				$average = 0;
			} else {
				$average = round( ( $deactivated / $total ) * 100, 1 )  . '%';
			}
			?>
			<table class="form-table wisdom-summary-table">
				<tbody>
					<tr>
						<th scope="row"><?php _e( 'Deactivation Rate'); ?></th>
						<td><?php echo esc_html( $average ); ?></td>
					</tr>
				</tbody>
			</table>
		<?php } ?>
	</div><!-- .wisdom-summary-totals -->

	<div id="wisdom-charts-wrapper" class="wisdom-charts">
		<div class="canvas-wrapper">
			<div style="position: relative; ">
				<canvas id="chartOne" width="400" height="400"></canvas>
			</div>
			<div style="position: relative; width: 49%;">
				<canvas id="chartTwo" width="400" height="200"></canvas>
			</div>
		</div>
	
		<?php wisdom_active_summary(); ?>
	
	</div>
	
	
<?php }

/**
 * This is a simple Active/Deactivated chart
 * All plugins
 * No time period defined
 * @since 1.0.0
 */
function wisdom_active_summary() {
	$total_active = wisdom_get_total_by_meta_summary( array( 'meta_key' => 'wisdom_status', 'meta_value' => 'Active' ) );
	$total_deactive = wisdom_get_total_by_meta_summary( array( 'meta_key' => 'wisdom_status', 'meta_value' => 'Deactivated' ) );
	$labels = '["' . __( 'Active', 'wisdom-plugin' ) .'","' . __( 'Deactivated', 'wisdom-plugin' ) .'"]';
	$colors = wisdom_nice_colors();
	$bg_colors = '["rgba(' . $colors[0] . ',0.75' . ')","rgba(' . $colors[1] . ',0.75' . ')"]';
	$border_colors = '["rgba(' . $colors[0] . ',1' . ')","rgba(' . $colors[1] . ',1' . ')"]';
	?>
	<script>
		jQuery(document).ready(function($){		
			// Let's set some vars
			var type = 'doughnut';
			var labels = <?php echo $labels; ?>;
			var data = [<?php echo $total_active; ?>, <?php echo $total_deactive; ?>];
			var bg = <?php echo $bg_colors; ?>;
			var border = <?php echo $border_colors; ?>;
			
			var ctx = document.getElementById("chartOne").getContext("2d");
			var myChart = new Chart(ctx, {
			    type: type,
			    data: {
			        labels: labels,
			        datasets: [
						{
							data: data,
							backgroundColor: bg,
							borderColor: border,
							borderWidth: 3
						}
					]
			    }
			});
		});
	</script>
<?php }
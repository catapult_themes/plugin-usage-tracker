<?php
/**
 * Reports summary page
 * The default page under Reports
 *
 * @package Wisdom Plugin
 * @since 1.0.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Outputs the content for the summary report
 * @since 1.0.0
 */
function wisdom_plugin_options() {
	
	$current_plugin = isset( $_GET['plugin'] ) ? $_GET['plugin'] : 'all';
	if( $current_plugin == 'all' ) {
		echo '<p>' . __( 'Please select a plugin', 'wisdom-plugin' ) . '</p>';
		return;
	}
	
	$current_option = isset( $_GET['plugin_option'] ) ? $_GET['plugin_option'] : '';
	if( $current_option == '' ) {
		echo '<p>' . __( 'Please select an option', 'wisdom-plugin' ) . '</p>';
		return;
	}
	
	$plugin_options = wisdom_get_plugin_options(); ?>
	
	<div id="wisdom-summary-wrapper" class="wisdom-summary-totals">
		<table class="form-table wisdom-language-table">
			<tbody>
				<?php
				$labels = array();
				$data = array();
				$bg_colors = array();
				$border_colors = array();
				$count_colors = 0; // Keep track of the colors
				$nice_colors = wisdom_nice_colors();
				$number_colors = count( $nice_colors );
				if( isset( $plugin_options[$current_option] ) ) {
					$count_set = 0; // Keeping a running count of options that have a value
					// Get all the values set for this option
					$option = $plugin_options[$current_option];
					if( isset( $option['key'] ) ) {
						printf( '<h2>%s</h2>',
							esc_html( $option['key'] )
						);
					}
					
					if( ! empty( $option ) ) {
						if( ! empty( $option['values'] ) ) {
							// All the values assigned to the option ?>
							<tr>
								<th scope="row"><?php _e( 'Value', 'wisdom-plugin' ); ?></th>
								<th scope="row"><?php _e( 'Freq', 'wisdom-plugin' ); ?></th>
								<th scope="row"><?php _e( '%', 'wisdom-plugin' ); ?></th>
							</tr>
							
							<?php 
							$total_sites = 0;
							foreach( $option['values'] as $value=>$count ) {
								// Get the total number of sites being tracked
								$total_sites += $count;
							}
							foreach( $option['values'] as $value=>$count ) { ?>
								<tr>
									<th scope="row"><?php echo esc_html( $value ); ?></th>
									<td><?php echo esc_html( $count ); ?></td>
									<td><?php echo esc_html( round( ( $count / $total_sites ) * 100, 2 ) ); ?></td>
									<?php
									$labels[] = '"' . esc_html( $value ) . '"';
									$data[] = $count;
									$count_set = $count_set + $count;
									if( $count_colors > $number_colors ) {
										// Use a random one
										$color = wisdom_generate_random_rgb();
									} else {
										$color = $nice_colors[$count_colors];
									}
									$bg_colors[] = '"rgba(' . $color . ',0.75' . ')"';
									$border_colors[] = '"rgba(' . $color . ',1' . ')"';
									$count_colors++;
									?>
								</tr>
							<?php } ?>
							<tr>
								<th scope="row"><?php _e( 'Not Set', 'wisdom-plugin' ); ?></th>
								<td><?php echo esc_html( $total_sites - $count_set ); ?></td>
								<td><?php echo esc_html( round( ( ( $total_sites - $count_set ) / $total_sites ) * 100, 2 ) ); ?></td>
							</tr>
						<?php }
					}
				} ?>
				
				
			</tbody>
		</table>
	</div><!-- .wisdom-summary-totals -->

	<div id="wisdom-charts-wrapper" class="wisdom-charts">
		<div class="canvas-wrapper">
			<div style="position: relative; ">
				<canvas id="chartOne" width="400" height="400"></canvas>
			</div>
			<div style="position: relative; width: 49%;">
				<canvas id="chartTwo" width="400" height="200"></canvas>
			</div>
		</div>
		<?php
		$labels = '[' . join( ',', $labels ) . ']';
		$data = '[' . join( ',', $data ) . ']';
		$bg = '[' . join( ',', $bg_colors ) . ']';
		$border = '[' . join( ',', $border_colors ) . ']';
		?>
		<script>
			jQuery(document).ready(function($){	
				// Let's set some vars
				var type = 'bar';
				var labels = <?php echo $labels; ?>;
				var data = <?php echo $data; ?>;
				var bg = <?php echo $bg; ?>;
				var border = <?php echo $border; ?>;
			
				var ctx = document.getElementById("chartOne").getContext("2d");
				var myChart = new Chart(ctx, {
				    type: type,
				    data: {
				        labels: labels,
				        datasets: [
							{
								data: data,
								backgroundColor: bg,
								borderColor: border,
								borderWidth: 3
							}
						]
				    },
					options: {
						legend: {
							display: false
						},
						scales: {
							yAxes: [{
								ticks: {
									min: 0,
								}
							}]
						}
					}
				});
			});
		</script>
	
	</div>
	
	
<?php }
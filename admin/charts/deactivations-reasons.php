<?php
/**
 * Deactivations and reasons
 *
 * @package Wisdom Plugin
 * @since 1.0.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This is a simple Active/Deactivated chart
 * All plugins
 * No time period defined
 * @since 1.0.0
 */
function wisdom_deactivations_reasons() {
	
	$current_plugin = isset( $_GET['plugin'] ) ? $_GET['plugin'] : 'all';

	// Gets us a multi-dimensional array like plugin_slug -> deactivation_reason -> count
	$reasons = wisdom_get_deactivation_reasons();

	$all_reasons = array();
	if( ! empty( $reasons ) && is_array( $reasons ) ) {
		// Iterate through each plugin
		foreach( $reasons as $p=>$r ) {
			// Check if we're querying one plugin or all
			if( $current_plugin == 'all' || $current_plugin == $p ) {
				// Now iterate through each reason
				foreach( $r as $reason=>$count ) {
					if( isset( $all_reasons[esc_attr( $reason )] ) ) {
						$all_reasons[esc_attr( $reason )] += $count;
					} else {
						$all_reasons[esc_attr( $reason )] = $count;
					}
				}
			}
		}
	}
	// Sort largest first
	arsort( $all_reasons );

	$count = 0; // Keep track of how many languages
	$colors = wisdom_nice_colors();
	$number_colors = count( $colors );
	if( ! empty( $all_reasons ) ) {
		$reason_labels = array();
		$reason_data = array();
		$bg_colors = array();
		$border_colors = array();
		foreach( $all_reasons as $key=>$value ) {
			$reason_labels[] = '"' . esc_html( $key ) . '"';
			$reason_data[] = esc_attr( $value );
			// Decide which color to use
			if( $count > $number_colors ) {
				// Use a random one
				$color = wisdom_generate_random_rgb();
			} else {
				$color = $colors[$count];
			}
			$bg_colors[] = '"rgba(' . $color . ',0.75' . ')"';
			$border_colors[] = '"rgba(' . $color . ',1' . ')"';
			$count++;
		}
		$labels = '[' . join( ',', $reason_labels ) . ']';
		$data = '[' . join( ',', $reason_data ) . ']';
		$bg = '[' . join( ',', $bg_colors ) . ']';
		$border = '[' . join( ',', $border_colors ) . ']';
	} else {
		_e( 'No data found for this report', 'wisdom-plugin' );
		return;
	}
	
	// Create a table to display this data
	if( ! empty( $all_reasons ) ) { ?>
	<div id="wisdom-summary-wrapper" class="wisdom-summary-totals">
		<table class="form-table wisdom-language-table">
			<tbody>
				<?php foreach( $all_reasons as $key=>$value ) { ?>
				<tr>
					<th scope="row"><?php echo esc_html( $key ); ?></th>
					<td><?php echo esc_html( $value ); ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div><!-- .wisdom-summary-totals -->
	
	<?php } ?>

	<div id="wisdom-charts-wrapper" class="wisdom-charts">
		<div class="canvas-wrapper">
			<div style="position: relative; ">
				<canvas id="chartOne" width="400" height="400"></canvas>
			</div>
			<div style="position: relative; width: 49%;">
				<canvas id="chartTwo" width="400" height="200"></canvas>
			</div>
		</div>
	</div>
	

	<script>
		jQuery(document).ready(function($){
			// Let's set some vars
			var type = 'doughnut';
			var labels = <?php echo $labels; ?>;
			var data = <?php echo $data; ?>;
			var bg = <?php echo $bg; ?>;
			var border = <?php echo $border; ?>;
			
			var ctx = document.getElementById("chartOne").getContext("2d");
			var myChart = new Chart(ctx, {
			    type: type,
			    data: {
			        labels: labels,
			        datasets: [
						{
							data: data,
							backgroundColor: bg,
							borderColor: border,
							borderWidth: 3
						}
					]
			    }
			});
		});
	</script>
<?php }
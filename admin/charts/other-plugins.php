<?php
/**
 * Data on other plugins run on tracked sites
 *
 * @package Wisdom Plugin
 * @since 1.2.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Outputs the content for the report
 * @since 1.0.0
 */
function wisdom_other_plugins() {
	
	$other_plugins = wisdom_get_other_active_plugins();
	$total_sites = wisdom_get_total_sites_summary(); 
	if( ! is_numeric( $total_sites ) || $total_sites < 1 ) {
		return;
	}
	
	// Calculate nice value for y-axis ticks
	$y_tick = 1;
	// The value of the first element in the array is the greatest number
	$highest_y_value = reset( $other_plugins );
	if( $highest_y_value > 20 && $highest_y_value <= 50 ) {
		$y_tick = 5;
	} else if( $highest_y_value > 50 && $highest_y_value <= 250 ) {
		$y_tick = 10;
	} else if( $highest_y_value > 250 && $highest_y_value <= 500 ) {
		$y_tick = 20;
	} else if( $highest_y_value > 50 && $highest_y_value <= 1000 ) {
		$y_tick = 50;
	} else if( $highest_y_value > 1000 && $highest_y_value <= 2500 ) {
		$y_tick = 100;
	} else if( $highest_y_value > 2500 && $highest_y_value <= 5000 ) {
		$y_tick = 250;
	} else if( $highest_y_value > 5000 ) {
		$y_tick = 500;
	}
	
	?>
	
	<div id="wisdom-summary-wrapper" class="wisdom-summary-totals wisdom-other-plugins-totals">
		<table class="form-table wisdom-language-table">
			<tbody>
				<?php
				$labels = array();
				$data = array();
				$bg_colors = array();
				$border_colors = array();
				$count_colors = 0; // Keep track of the colors
				$nice_colors = wisdom_nice_colors();
				$number_colors = count( $nice_colors );
				if( isset( $other_plugins ) ) { ?>
					<tr>
						<th scope="row"><?php _e( 'Plugin', 'wisdom-plugin' ); ?></th>
						<th scope="row"><?php _e( 'Freq', 'wisdom-plugin' ); ?></th>
						<th scope="row"><?php _e( '%', 'wisdom-plugin' ); ?></th>
					</tr>
							
					<?php foreach( $other_plugins as $key=>$value ) { ?>
						<tr>
							<th scope="row"><?php echo esc_html( $key ); ?></th>
							<td><?php echo esc_html( $value ); ?></td>
							<td><?php echo esc_html( round( ( $value / $total_sites ) * 100, 2 ) ); ?></td>
							<?php
							$labels[] = '"' . esc_html( $key ) . '"';
							$data[] = $value;
							if( $count_colors >= $number_colors ) {
								// Use a random one
								$color = wisdom_generate_random_rgb();
							} else {
								$color = $nice_colors[$count_colors];
							}
							$bg_colors[] = '"rgba(' . $color . ',0.75' . ')"';
							$border_colors[] = '"rgba(' . $color . ',1' . ')"';
							$count_colors++;
							?>
						</tr>
					<?php } ?>
	
				<?php } ?>
				
				
			</tbody>
		</table>
	</div><!-- .wisdom-summary-totals -->

	<div id="wisdom-charts-wrapper" class="wisdom-charts wisdom-other-plugins-charts">
		<div class="canvas-wrapper">
			<div style="position: relative; ">
				<canvas id="chartOne" width="400" height="400"></canvas>
			</div>
			<div style="position: relative; width: 49%;">
				<canvas id="chartTwo" width="400" height="200"></canvas>
			</div>
		</div>
		<?php
		$labels = '[' . join( ',', $labels ) . ']';
		$data = '[' . join( ',', $data ) . ']';
		$bg = '[' . join( ',', $bg_colors ) . ']';
		$border = '[' . join( ',', $border_colors ) . ']';
		?>
		<script>
			jQuery(document).ready(function($){	
				// Let's set some vars
				var type = 'bar';
				var labels = <?php echo $labels; ?>;
				var data = <?php echo $data; ?>;
				var bg = <?php echo $bg; ?>;
				var border = <?php echo $border; ?>;
				var y_tick = <?php echo $y_tick; ?>;
			
				var ctx = document.getElementById("chartOne").getContext("2d");
				var myChart = new Chart(ctx, {
				    type: type,
				    data: {
				        labels: labels,
				        datasets: [
							{
								data: data,
								backgroundColor: bg,
								borderColor: border,
								borderWidth: 3
							}
						]
				    },
					options: {
						legend: {
							display: false
						},
						scales: {
							xAxes: [{
								ticks: {
									autoSkip: false
								}
							}],
							yAxes: [{
								ticks: {
									stepSize: y_tick,
									min: 0,
								}
							}]
						}
					}
				});
			});
		</script>
	
	</div>
	
	
<?php }
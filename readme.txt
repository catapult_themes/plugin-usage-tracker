=== Wisdom ===
Contributors: Catapult_Themes
Tags: analytics testing plugin
Requires at least: 4.3
Tested up to: 5.0.3
Stable tag: 1.4.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Get real data on how people are using your plugins and themes.

== Description ==

Get real data about your plugins and themes: track and analyse user data to find out who uses your plugins and themes, what options they enable, and the reasons they deactivate

== Installation ==

N/A

== Frequently Asked Questions ==

N/A

== Screenshots ==

N/A

== Changelog ==

= 1.4.5 30 January 2019 =
* Added: wisdom_mailchimp_optin_status filter to allow double opt-in for Mailchimp
* Updated: updater functions prior to ownership transfer

= 1.4.4 4 March 2018 =
* Updated: retrieve up to 50 Mailchimp lists

= 1.4.3 26 February 2018 =
* Updated: class-plugin-usage-tracker updated to 1.2.4

= 1.4.2 23 January 2018 =
* Fixed: not correctly updating status on reactivation

= 1.4.1 17 January 2018 =
* Added: wisdom_deactivate_existing_item and wisdom_create_new_item hooks

= 1.4.0 11 January 2018 =
* Added: support for tracking themes
* Updated: renamed Tracked Plugins to Tracked Products
* Updated: tracked-product admin screen
* Updated: performance improvements

= 1.3.3 28 December 2017 =
* Updated: class-plugin-usage-tracker updated to 1.1.2

= 1.3.2 5 December 2017 =
* Added: Mailchimp metabox
* Updated: print correct number of Mailchimp rules
* Updated: sanitize_email for admin email addresses
* Updated: record blank value for missing email addresses
* Updated: check for new plugins on save-data.php
* Updated: removed sslverify=>false from wp_remote_post

= 1.3.1 5 December 2017 =
* Added: debugging page and tracking
* Fixed: parse error on empty export query

= 1.3.0 2 December 2017 =
* Added: export to CSV
* Added: track options outside Settings API
* Added: option to unsubscribe deactivated users from Mailchimp
* Fixed: parse error for empty array in columns.php
* Updated: display options field via AJAX in Plugin Options report
* Updated: show message in Mailchimp lists if no data received
* Updated: removed 'Email Addresses' menu item - email addresses can now be exported instead
* Updated: code refactoring and standardizing file names

= 1.2.0 3 October 2017 =
* Added: Mailchimp integration
* Added: new parameter to return top n results
* Fixed: clearing weekly cron job correctly
* Fixed: incorrect sites total in plugin-options.php chart
* Updated: better error catching

= 1.1.0 21 July 2017 =
* Added: batch processing for large queries
* Added: filter post type by plugin slug
* Added: Wisdom_Admin class
* Added: option to opt out of tracking (tracking file)

= 1.0.0 17 February 2017 =
* Initial commit

== Upgrade Notice ==

N/A

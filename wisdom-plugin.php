<?php
/*
Plugin Name: Wisdom Plugin
Plugin URI: https://wisdomplugin.com/
Description: Allows you to receive and track data on your plugins
Version: 1.4.5
Author: Catapult Themes
Author URI: https://catapultthemes.com/
Text Domain: wisdom-plugin
Domain Path: /languages
*/

// Exit if accessed directly

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Define constants
 **/
if ( ! defined( 'WISDOM_PLUGIN_URL' ) ) {
	define( 'WISDOM_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}
if ( ! defined( 'WISDOM_PLUGIN_DIR' ) ) {
	define( 'WISDOM_PLUGIN_DIR', dirname( __FILE__ ) );
}
if ( ! defined( 'WISDOM_PLUGIN_VERSION' ) ) {
	define( 'WISDOM_PLUGIN_VERSION', '1.4.5' );
}

// this is the URL our updater / license checker pings. This should be the URL of the site with EDD installed
define( 'WISDOM_PLUGIN_STORE_URL', 'https://wisdomplugin.com' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

// the name of your product. This should match the download name in EDD exactly
define( 'WISDOM_PLUGIN_ITEM_NAME', 'Wisdom' ); // you should use your own CONSTANT name, and be sure to replace it throughout this file

// the name of the settings page for the license input to be displayed
define( 'WISDOM_PLUGIN_LICENSE_PAGE', 'wisdom-license' );

require_once dirname( __FILE__ ) . '/includes/functions.php';
require_once dirname( __FILE__ ) . '/includes/metaboxes.php';
require_once dirname( __FILE__ ) . '/includes/post-type.php';
require_once dirname( __FILE__ ) . '/includes/save-data.php';

require_once dirname( __FILE__ ) . '/includes/libs/MailChimp.php';
require_once dirname( __FILE__ ) . '/includes/class-wisdom-mailchimp.php';


if( ! class_exists( 'Wisdom_EDD_SL_Plugin_Updater' ) ) {
	include( dirname( __FILE__ ) . '/updater/EDD_SL_Plugin_Updater.php' );
}
require_once dirname( __FILE__ ) . '/updater/updater.php';

// Check for updates
function wisdom_plugin_updater() {

	// retrieve our license key from the DB
	$license_key = trim( get_option( 'wisdom_plugin_license_key' ) );

	// setup the updater
	$edd_updater = new Wisdom_EDD_SL_Plugin_Updater( WISDOM_PLUGIN_STORE_URL, __FILE__, array(
			'version' 	=> WISDOM_PLUGIN_VERSION, 				// current version number
			'license' 	=> $license_key, 						// license key (used get_option above to retrieve from DB)
			'item_name' => WISDOM_PLUGIN_ITEM_NAME, 			// name of this plugin
			'author' 	=> 'Catapult Themes',  					// author of this plugin
			'beta'		=> false
		)
	);
}
add_action( 'admin_init', 'wisdom_plugin_updater', 0 );

if( is_admin() ) {
	require_once dirname( __FILE__ ) . '/admin/admin.php';
}

/**
 * This function allows you to track usage of your plugin
 * Place in your main plugin file
 * Refer to https://wisdomplugin.com/support for help
 */
if( ! class_exists( 'Plugin_Usage_Tracker') ) {
	require_once dirname( __FILE__ ) . '/tracking/class-plugin-usage-tracker.php';
}
if( ! function_exists( 'wisdom_start_plugin_tracking' ) ) {
	function wisdom_start_plugin_tracking() {
		$wisdom = new Plugin_Usage_Tracker(
			__FILE__,
			'https://wisdomplugin.com',
			array(),
			false,
			false,
			3
		);
	}
	wisdom_start_plugin_tracking();
}

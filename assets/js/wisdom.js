( function( $ ) {
	
	var title_text = document.title;
	
	// Form to generate data
	// https://pippinsplugins.com/batch-processing-for-big-data/
	$('body').on( 'submit', '.wisdom-ajax-params', function(e) {
		e.preventDefault();

		$('.wisdom-run-query').prop( 'disabled', true );
		
		var params = $(this).serialize();

		$('.wisdom-batch-progress').append( '<div><span style="float: none;" class="spinner is-active"></span><div style="width: 300px; height: 4px; background: #ccc; display: inline-block;" class="wisdom-progress-bar"><div style="width: 0; height: 4px; background: #222;"></div></div><span style="vertical-align: middle; padding-left: 10px;" class="wisdom-offset"></span></div>' );

		// start the process
		self.process_offset( 0, params, self );
	});
	
	// This is the form to view a specific report
	$('body').on( 'submit', '.wisdom-report-params', function(e) {
		e.preventDefault();
		
		$('.wisdom-run-query').prop( 'disabled', true );

		var params = $(this).serialize();

		// start the process
		self.do_report( params, self );
	});
	
	do_report = function( params, self ) {
		// Do AJAX to check for existing transient
		// If transient doesn't exist, create it by doing process_offset(?)
		// Otherwise create the report
		$.ajax({
			type: 'POST',
			url: ajaxurl,
			data: {
				params: params,
				action: 'wisdom_check_transient',
			},
			dataType: "json",
			success: function( response ) {

				if( 'ok' == response.status ) {
					// Do report
					window.location = response.url;
				} else {
					// If there's no transient, then create one
					$('.wisdom-batch-progress').append( '<div><span style="float: none;" class="spinner is-active"></span><div style="width: 300px; height: 4px; background: #ccc; display: inline-block;" class="wisdom-progress-bar"><div style="width: 0; height: 4px; background: #222;"></div></div><span style="vertical-align: middle; padding-left: 10px;" class="wisdom-offset"></span></div>' );
					self.process_offset( 0, params, self );
				}

			}
		}).fail(function (response) {
			if ( window.console && window.console.log ) {
				console.log( response );
				$( '.wisdom-batch-progress' ).html( 'Report failed. Please check your parameters and try again' );
			}
		});
	}
	
	process_offset = function( offset, params, self ) {

		$.ajax({
			type: 'POST',
			url: ajaxurl,
			data: {
				params: params,
				action: 'wisdom_do_batch_query',
				offset: offset,
			},
			dataType: "json",
			success: function( response ) {
				console.log(response);
				if( 'done' == response.offset ) {
					window.location = response.url;
				} else {
					var offset = response.offset;
					var count = response.count;
					if( offset > count ) {
						offset = count;
					}
					$( '.wisdom-offset' ).html( offset + ' of ' + count );
					$( '.wisdom-progress-bar div' ).animate({
						width: response.percentage + '%',
					}, 1000, function() {
						// Animation complete.
					});
					document.title = Math.round( response.percentage, 1 ) + '%: ' + title_text;
					self.process_offset( parseInt( response.offset ), params, self );
				}

			}
		}).fail(function (response) {
			if ( window.console && window.console.log ) {
				console.log( response );
				$( '.wisdom-batch-progress' ).html( 'Report failed. Please check your parameters and try again' );
			}
		});

	}
	
	// AJAX for exporting
	$('body').on( 'submit', '#wisdom-export-form', function(e) {
		e.preventDefault();
		$('.wisdom-export-csv').prop( 'disabled', true );
		var params = $(this).serialize();
		$('.wisdom-batch-progress').append( '<div><span style="float: none;" class="spinner is-active"></span><span style="vertical-align: middle; padding-left: 10px;" class="wisdom-offset"></span></div>' );
		self.process_export( 1, params, self );
	});
	
	process_export = function( step, params, self ) {
		$.ajax({
			type: 'POST',
			url: ajaxurl,
			data: {
				params: params,
				action: 'wisdom_do_batch_export',
				step: step,
			},
			dataType: "json",
			success: function( response ) {
				console.log(response);
				if( false == response.status ) {
					$( '.wisdom-batch-progress' ).remove();
					window.location = response.url;
				} else {
					var step = response.step;
					var batch = response.batch;
					$( '.wisdom-offset' ).html( parseInt( step * batch ) + ' sites processed.' );
					self.process_export( parseInt( response.step ), params, self );
				}

			}
		}).fail(function (response) {
			if ( window.console && window.console.log ) {
				$( '.wisdom-batch-progress' ).html( 'Export failed. Please check your parameters and try again.' );
			}
		});

	}
	
}( jQuery ) );
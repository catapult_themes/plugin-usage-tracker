<?php
/**
 * Handles license updates
 *
 * @package     Wisdom Plugin
 * @since       1.0.0
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/************************************
* the code below is just a standard
* options page. Substitute with
* your own.
*************************************/

function wisdom_plugin_license_menu() {
	add_submenu_page( 'edit.php?post_type=tracked-plugin', __( 'License', 'wisdom-plugin' ), __( 'License', 'wisdom-plugin' ), 'manage_options', WISDOM_PLUGIN_LICENSE_PAGE, 'wisdom_plugin_license_page' );
}
add_action( 'admin_menu', 'wisdom_plugin_license_menu');

function wisdom_plugin_license_page() {
	$license = get_option( 'wisdom_plugin_license_key' );
	$status  = get_option( 'wisdom_plugin_license_status' );
	$message  = get_option( 'wisdom_plugin_license_message', false );
	?>
	<div class="wrap">
		<h2><?php _e( 'Wisdom License', 'wisdom-plugin' ); ?></h2>
		<form method="post" action="options.php">

			<?php settings_fields( 'wisdom_plugin_license'); ?>

			<table class="form-table">
				<tbody>
					<tr valign="top">
						<th scope="row" valign="top">
							<?php _e( 'License Key', 'wisdom-plugin' ); ?>
						</th>
						<td>
							<input id="wisdom_plugin_license_key" name="wisdom_plugin_license_key" type="text" class="regular-text" value="<?php esc_attr_e( $license ); ?>" />
							<p class="description" for="wisdom_plugin_license_key"><?php _e( 'Enter your license key', 'wisdom-plugin' ); ?></p>
						</td>
					</tr>
					<?php if( false !== $license && '' != $license ) { ?>
						<tr valign="top">
							<th scope="row" valign="top">
								<?php _e( 'Action', 'wisdom-plugin' ); ?>
							</th>
							<td>
								<?php if( $status !== false && $status == 'valid' ) { ?>
									<?php wp_nonce_field( 'wisdom_sample_nonce', 'wisdom_sample_nonce' ); ?>
									<input type="submit" class="button-secondary" name="wisdom_plugin_license_deactivate" value="<?php _e( 'Deactivate License', 'wisdom-plugin' ); ?>"/>
								<?php } else {
									wp_nonce_field( 'wisdom_sample_nonce', 'wisdom_sample_nonce' ); ?>
									<input type="submit" class="button-secondary" name="wisdom_plugin_license_activate" value="<?php _e( 'Activate License', 'wisdom-plugin' ); ?>"/>
								<?php } ?>
							</td>
						</tr>
					<?php } ?>
					<?php if( false !== $license && '' != $license ) { ?>
						<tr valign="top">
							<th scope="row" valign="top">
								<?php _e( 'Status', 'wisdom-plugin' ); ?>
							</th>
							<td>
								<?php if( $status !== false && $status == 'valid' ) { ?>
									<span style="color: green;"><span class="dashicons dashicons-yes"></span> <?php _e( 'Active', 'wisdom-plugin' ); ?></span>
								<?php } else { ?>
									<span style="color: red;"><span class="dashicons dashicons-no-alt"></span> <?php echo ucfirst( $status ); ?></span>
									<?php if( $message ) {
										echo '<br>' . $message;
									}
								} ?>
							</td>
						</tr>
					<?php } ?>

				</tbody>
			</table>
			<?php submit_button(); ?>

		</form>
	<?php
}

function wisdom_plugin_register_option() {
	// creates our settings in the options table
	register_setting( 'wisdom_plugin_license', 'wisdom_plugin_license_key', 'wisdom_plugin_sanitize_license' );
}
add_action( 'admin_init', 'wisdom_plugin_register_option');

function wisdom_plugin_sanitize_license( $new ) {
	$old = get_option( 'wisdom_plugin_license_key' );
	if( $old && $old != $new ) {
		delete_option( 'wisdom_plugin_license_status' ); // new license has been entered, so must reactivate
	}
	return $new;
}

/**
 * Check the license status, fires daily
 * @since 1.4.5
 */
function wisdom_daily_check_license() {
	$license = trim( get_option( 'wisdom_plugin_license_key' ) );
	wisdom_do_license_activation( $license );
}
add_action( 'wp_scheduled_delete', 'wisdom_daily_check_license' );

/**
 * Activate the license
 */
function wisdom_activate_license( $license='' ) {
	if( empty( $_POST['wisdom_plugin_license_activate'] ) || ! check_admin_referer( 'wisdom_sample_nonce', 'wisdom_sample_nonce' ) ) {
		// run a quick security check
		return; // get out if we didn't click the Activate button
	}
	$license = trim( $_POST['wisdom_plugin_license_key'] );
	wisdom_do_license_activation( $license );
}
add_action( 'admin_init', 'wisdom_activate_license' );

/**
 * Activate the license
 */
function wisdom_do_license_activation( $license ) {

	// retrieve the license from the database
	// $license = trim( get_option( 'wisdom_plugin_license_key' ) );

	// data to send in our API request
	$api_params = array(
		'edd_action' => 'activate_license',
		'license'    => $license,
		'item_name'  => urlencode( WISDOM_PLUGIN_ITEM_NAME ), // the name of our product in EDD
		'url'        => home_url()
	);

	// Call the custom API.
	$response = wp_remote_post( WISDOM_PLUGIN_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

	// make sure the response came back okay
	if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {

		if ( is_wp_error( $response ) ) {
			$message = $response->get_error_message();
		} else {
			$message = __( 'An error occurred, please try again.', 'wisdom-plugin' );
		}

	} else {

		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		if ( false === $license_data->success ) {

			switch( $license_data->error ) {

				case 'expired' :

					$message = sprintf(
						__( 'Your license key expired on %s.', 'wisdom-plugin' ),
						date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires, current_time( 'timestamp' ) ) )
					);
					break;

				case 'revoked' :

					$message = __( 'Your license key has been disabled.', 'wisdom-plugin' );
					break;

				case 'missing' :

					$message = __( 'Invalid license.', 'wisdom-plugin' );
					break;

				case 'invalid' :
				case 'site_inactive' :

					$message = __( 'Your license is not active for this URL.', 'wisdom-plugin' );
					break;

				case 'item_name_mismatch' :

					$message = sprintf(
						__( 'This appears to be an invalid license key for %s.', 'wisdom-plugin' ),
						WISDOM_PLUGIN_ITEM_NAME
					);
					break;

				case 'no_activations_left':

					$message = __( 'Your license key has reached its activation limit.', 'wisdom-plugin' );
					break;

				default :

					$message = __( 'An error occurred, please try again.', 'wisdom-plugin' );
					break;
			}

		}

	}

	// Check if anything passed on a message constituting a failure
	if ( ! empty( $message ) ) {
		// $base_url = admin_url( 'edit.php?post_type=tracked-plugin&page=' . WISDOM_PLUGIN_LICENSE_PAGE );
		// $redirect = add_query_arg( array( 'sl_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );
		//
		// wp_redirect( $redirect );
		// exit();
		update_option( 'wisdom_plugin_license_status', 'invalid' );
		update_option( 'wisdom_plugin_license_message', $message );
		return;
	} else {
		delete_option( 'wisdom_plugin_license_message' );
	}

	// Set the license limit
	// First parameter must be true to force an update (e.g. after upgrading)
	wisdom_plugin_set_plugin_limit( true, $license_data );

	// $license_data->license will be either "valid" or "invalid"
	update_option( 'wisdom_plugin_license_status', $license_data->license );
	// wp_redirect( admin_url( 'edit.php?post_type=tracked-plugin&page=' . WISDOM_PLUGIN_LICENSE_PAGE ) );
	// exit();

}
// add_action( 'admin_init', 'wisdom_plugin_activate_license' );


/***********************************************
* Illustrates how to deactivate a license key.
* This will decrease the site count
***********************************************/

function wisdom_plugin_deactivate_license() {

	// listen for our deactivate button to be clicked
	if( isset( $_POST['wisdom_plugin_license_deactivate'] ) ) {

		// run a quick security check
	 	if( ! check_admin_referer( 'wisdom_sample_nonce', 'wisdom_sample_nonce' ) )
			return; // get out if we didn't click the Activate button

		// retrieve the license from the database
		$license = trim( get_option( 'wisdom_plugin_license_key' ) );

		// data to send in our API request
		$api_params = array(
			'edd_action' => 'deactivate_license',
			'license'    => $license,
			'item_name'  => urlencode( WISDOM_PLUGIN_ITEM_NAME ), // the name of our product in EDD
			'url'        => home_url()
		);

		// Call the custom API.
		$response = wp_remote_post( WISDOM_PLUGIN_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

		// make sure the response came back okay
		if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {

			if ( is_wp_error( $response ) ) {
				$message = $response->get_error_message();
			} else {
				$message = __( 'An error occurred, please try again.', 'wisdom-plugin' );
			}

			$base_url = admin_url( 'edit.php?post_type=tracked-plugin&page=' . WISDOM_PLUGIN_LICENSE_PAGE );
			$redirect = add_query_arg( array( 'sl_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );

			wp_redirect( $redirect );
			exit();
		}

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		// $license_data->license will be either "deactivated" or "failed"
		// if( $license_data->license == 'deactivated' || $license_data->license == 'failed' ) {
		// 	delete_option( 'wisdom_plugin_license_status' );
		// }
		update_option( 'wisdom_plugin_license_status', $license_data->license );

		wp_redirect( admin_url( 'edit.php?post_type=tracked-plugin&page=' . WISDOM_PLUGIN_LICENSE_PAGE ) );
		exit();

	}
}
add_action( 'admin_init', 'wisdom_plugin_deactivate_license');


/************************************
* this illustrates how to check if
* a license key is still valid
* the updater does this for you,
* so this is only needed if you
* want to do something custom
*************************************/

function wisdom_plugin_check_license() {

	global $wp_version;

	$license = trim( get_option( 'wisdom_plugin_license_key' ) );

	$api_params = array(
		'edd_action' => 'check_license',
		'license' => $license,
		'item_name' => urlencode( WISDOM_PLUGIN_ITEM_NAME ),
		'url'       => home_url()
	);

	// Call the custom API.
	$response = wp_remote_post( WISDOM_PLUGIN_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

	if ( is_wp_error( $response ) )
		return false;

	$license_data = json_decode( wp_remote_retrieve_body( $response ) );

	if( $license_data->license == 'valid' ) {
		echo 'valid'; exit;
		// this license is still valid
	} else {
		echo 'invalid'; exit;
		// this license is no longer valid
	}
}

/**
 * This is a means of catching errors from the activation method above and displaying it to the customer
 */
function wisdom_plugin_admin_notices() {
	if ( isset( $_GET['sl_activation'] ) && ! empty( $_GET['message'] ) ) {

		switch( $_GET['sl_activation'] ) {

			case 'false':
				$message = urldecode( $_GET['message'] );
				?>
				<div class="error">
					<p><?php echo $message; ?></p>
				</div>
				<?php
				break;

			case 'true':
			default:
				// Developers can put a custom success message here for when activation is successful if they way.
				break;

		}
	}
}
add_action( 'admin_notices', 'wisdom_plugin_admin_notices' );

/**
 * Add licence status class to admin
 * @since 1.4.5
 */
function wisdom_admin_body_class( $classes ) {
	$status = get_option( 'wisdom_plugin_license_status' );
	$classes .= ' wisdom-licence-' . $status;
	return $classes;
}
add_filter( 'admin_body_class', 'wisdom_admin_body_class');

/**
 * Invalid licence notice
 * @since 1.4.5
 */
function wisdom_licence_notice() {
	global $typenow;
	if( $typenow && $typenow == 'tracked-plugin' ) {
		$status = get_option( 'wisdom_plugin_license_status' );
		if( $status != 'valid' ) {
			printf(
				'<div class="notice notice-error"><h4>%s</h4><p>%s</p></div>',
				__( 'Wisdom', 'wisdom-plugin' ),
				__( 'It looks like your licence for Wisdom is not currently valid. Please activate or renew your licence in order to view your data.', 'wisdom-plugin' )
			);
		}
	}
}
add_action( 'admin_notices', 'wisdom_licence_notice' );

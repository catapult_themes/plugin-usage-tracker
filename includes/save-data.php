<?php
/**
 * Receives data from remote sites and records it
 *
 * @package     Wisdom Plugin
 * @since       1.0.0
*/

// Exit if accessed directly

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * This takes our remote data and adds it to the dashboard
 * @since 1.0.0
 */
function wisdom_save_data() {

	if( isset( $_GET['usage_tracker'] ) && $_GET['usage_tracker'] == 'hello' ) {
		// If there is no value for the URL or the plugin, we can't continue
		if( ! isset( $_POST['url'] ) || ! isset( $_POST['plugin_slug'] ) ) {
			return;
		}

		$limit = get_option( 'wisdom_license_limit' );
		if( $limit == -1 ) {
			// License not valid so bounce
			return;
		} else if( $limit > 0 ) {
			$options = get_option( 'wisdom_plugins_option_name' );
			$can_track = false;
			// Check each saved plugin slug against the plugin reporting back
			for( $i = 1; $i <= $limit; $i++ ) {
				if( isset( $options['plugin_slug_' . $i] ) && trim( $_POST['plugin_slug'] ) == trim( $options['plugin_slug_' . $i] ) ) {
					// Found the plugin so it's okay to track
					$can_track = true;
				}
			}
			if( ! $can_track ) {
				$error = get_option( 'wisdom_error_log' );
				$new_error = array(
					'time'			=> time(),
					'plugin_slug'	=> sanitize_text_field( $_POST['plugin_slug'] ),
					'url'			=> sanitize_text_field( $_POST['url'] )
				);
				if( false === $error ) {
					$error = $new_error;
				} else {
					$error[] = $new_error;
				}
				update_option( 'wisdom_error_log', $error );
				// Couldn't find this plugin so bounce
				return;
			}
		}

		// Add any new plugins to our list
		$tracked_plugins = get_option( 'wisdom_tracked_plugins' );
		if( false === $tracked_plugins ) {
			$tracked_plugins = array( sanitize_text_field( $_POST['plugin_slug'] ) );
		} else if( ! in_array( $_POST['plugin_slug'], $tracked_plugins ) ) {
			$tracked_plugins[] = sanitize_text_field( $_POST['plugin_slug'] );
		}
		update_option( 'wisdom_tracked_plugins', $tracked_plugins );

		// We'll put our parameters in here
		$post_params = array();
		// Get our permitted keys
		$permitted_keys = wisdom_permitted_keys();
		// Iterate through permitted keys and build $post_params array
		foreach( $_POST as $key=>$value ) {
			// We save the data as a tracked_plugin post
			if( in_array( $key, $permitted_keys ) ) {
				$post_params[$key] = $value;
			}
		}

		/**
		 * Check if this site is already registered with us
		 * @todo use fields => ids to just return IDs
		 * @todo save query to a transient
		 */
		$item = get_page_by_title( $post_params['url'], OBJECT, 'tracked-plugin' );
		// Record the tracked-plugin ID if we find it
		$item_id = false;
		$query_args = array(
			'post_type'			=> 'tracked-plugin',
			'posts_per_page'	=> -1,
			'title'				=> esc_url( $post_params['url'] ),
		);
		// Query sites for same url in title
		$sites = new WP_Query( $query_args );
		global $post;
		if( $sites->have_posts() ) {
			// Iterate through sites to see if we have one with the same plugin
			while( $sites->have_posts() ) : $sites->the_post();
				$check_plugin = get_post_meta( $post->ID, 'wisdom_plugin_slug', true );
				if( $check_plugin == $post_params['plugin_slug'] ) {
					// Our plugin is already being recorded on this site
					$item_id = $post->ID;
				}
			endwhile;
		}

		// If no matching item
		if( false === $item_id ) {
			// If there is no item with this url and plugin, then create new entry
			$post_args = array(
				'post_title'	=> esc_url( $post_params['url'] ),
				'post_type'		=> 'tracked-plugin',
				'post_status'	=> 'publish'
			);
			$item_id = wp_insert_post( $post_args );
			if( isset ( $post_params['version'] ) ) {
				// We only add this first time round
				update_post_meta( $item_id, 'wisdom_initial_version', sanitize_text_field( $post_params['version'] ) );
				update_post_meta( $item_id, 'wisdom_first_recorded', time() );
			}
			// Hook here for any new sites being recorded
			do_action( 'wisdom_create_new_item', $item_id, $post_params );
		}

		// Here we're updating the meta, whether the item is newly created or previously existed
		if( ! is_wp_error( $item_id ) ) {
			if( isset ( $post_params['plugin_slug'] ) ) {
				update_post_meta( $item_id, 'wisdom_plugin_slug', sanitize_text_field( $post_params['plugin_slug'] ) );
			}
			if( isset ( $post_params['product_type'] ) ) {
				update_post_meta( $item_id, 'wisdom_product_type', sanitize_text_field( $post_params['product_type'] ) );
			}
			if( isset ( $post_params['site_name'] ) ) {
				update_post_meta( $item_id, 'wisdom_site_name', sanitize_text_field( $post_params['site_name'] ) );
			}
			if( isset ( $post_params['site_version'] ) ) {
				update_post_meta( $item_id, 'wisdom_site_version', sanitize_text_field( $post_params['site_version'] ) );
			}
			if( isset ( $post_params['site_language'] ) ) {
				update_post_meta( $item_id, 'wisdom_site_language', sanitize_text_field( $post_params['site_language'] ) );
			}
			if( isset ( $post_params['text_direction'] ) ) {
				update_post_meta( $item_id, 'wisdom_text_direction', sanitize_text_field( $post_params['text_direction'] ) );
			}
			if( isset ( $post_params['charset'] ) ) {
				update_post_meta( $item_id, 'wisdom_charset', sanitize_text_field( $post_params['charset'] ) );
			}
			if( isset ( $post_params['plugin'] ) ) {
				update_post_meta( $item_id, 'wisdom_plugin', sanitize_text_field( $post_params['plugin'] ) );
			}
			$status = 'Active';
			if( isset ( $post_params['status'] ) ) {
				$status = $post_params['status'];
				$current_status = get_post_meta( $item_id, 'wisdom_status', true );
				// Check if existing product is being deactivated
				if( $current_status == 'Active' && $status == 'Deactivated' ) {
					do_action( 'wisdom_deactivate_existing_item', $item_id, $post_params );
				}
			}
			update_post_meta( $item_id, 'wisdom_status', sanitize_text_field( $status ) );
			if( isset ( $post_params['version'] ) ) {
				// Update the current version
				// If this version is different from the current version, add the current version to the version journey before overwriting
				$current_version = get_post_meta( $item_id, 'wisdom_current_version', true );
				if( $post_params['version'] != $current_version ) {
					// Add to the journey
					// The journey is a list of all recorded versions of the plugin on this site
					$journey = get_post_meta( $item_id, 'wisdom_version_journey', true );
					if( empty( $journey ) ) {
						$journey = array( sanitize_text_field( $post_params['version'] ) );
					} else {
						$journey[] = sanitize_text_field( $post_params['version'] );
					}
					update_post_meta( $item_id, 'wisdom_version_journey', $journey );
				}
				update_post_meta( $item_id, 'wisdom_current_version', sanitize_text_field( $post_params['version'] ) );
			}

			if( isset ( $post_params['theme'] ) ) {
				update_post_meta( $item_id, 'wisdom_theme', sanitize_text_field( $post_params['theme'] ) );
			}
			if( isset ( $post_params['theme_version'] ) ) {
				update_post_meta( $item_id, 'wisdom_theme_version', sanitize_text_field( $post_params['theme_version'] ) );
			}
			if( isset ( $post_params['theme_parent'] ) ) {
				update_post_meta( $item_id, 'wisdom_theme_parent', sanitize_text_field( $post_params['theme_parent'] ) );
			}
			if( isset ( $post_params['first_recorded'] ) ) {
				update_post_meta( $item_id, 'wisdom_first_recorded', sanitize_text_field( $post_params['first_recorded'] ) );
			}
			if( isset ( $post_params['deactivated_date'] ) ) {
				update_post_meta( $item_id, 'wisdom_deactivated_date', sanitize_text_field( $post_params['deactivated_date'] ) );
			}
			if( isset ( $post_params['deactivation_reason'] ) ) {
				update_post_meta( $item_id, 'wisdom_deactivation_reason', sanitize_text_field( $post_params['deactivation_reason'] ) );
			}
			if( isset ( $post_params['deactivation_details'] ) ) {
				update_post_meta( $item_id, 'wisdom_deactivation_details', sanitize_text_field( $post_params['deactivation_details'] ) );
			}
			if( isset ( $post_params['message'] ) ) {
				update_post_meta( $item_id, 'wisdom_message', sanitize_text_field( $post_params['message'] ) );
			}
			if( isset ( $post_params['wisdom_version'] ) ) {
				update_post_meta( $item_id, 'wisdom_version', sanitize_text_field( $post_params['wisdom_version'] ) );
			}
			if( isset ( $post_params['file_location'] ) ) {
				update_post_meta( $item_id, 'wisdom_file_location', sanitize_text_field( $post_params['file_location'] ) );
			} else {
				delete_post_meta( $item_id, 'wisdom_file_location' );
			}
			if( isset ( $post_params['php_version'] ) ) {
				update_post_meta( $item_id, 'wisdom_php_version', sanitize_text_field( $post_params['php_version'] ) );
			}
			if( isset ( $post_params['server'] ) ) {
				update_post_meta( $item_id, 'wisdom_server', sanitize_text_field( $post_params['server'] ) );
			}
			if( isset ( $post_params['multisite'] ) ) {
				update_post_meta( $item_id, 'wisdom_multisite', sanitize_text_field( $post_params['multisite'] ) );
			}
			if( isset ( $post_params['email'] ) ) {
				update_post_meta( $item_id, 'wisdom_email', sanitize_email( $post_params['email'] ) );
				if( isset ( $post_params['plugin_slug'] ) ) {
					$unsubscribe = wisdom_get_option( 'wisdom_options_settings', 'unsubscribe_deactivated', 0 );
					if( $status != 'Active' && ! empty( $unsubscribe ) ) {
						// Unsubscribe if option set
						$Wisdom_Mailchimp = new Wisdom_Mailchimp( $item_id, false );
					} else if( $status == 'Active' ) {
						$mc_status = get_post_meta( $item_id, 'wisdom_mailchimp_status', true );
						if( $mc_status != 'subscribe' ) {
							// Subscribe to Mailchimp
							$Wisdom_Mailchimp = new Wisdom_Mailchimp( $item_id, true );
						}
					}
				}
			} else {
				update_post_meta( $item_id, 'wisdom_email', '-' );
			}
			if( isset ( $post_params['marketing_method'] ) ) {
				update_post_meta( $item_id, 'wisdom_marketing_method', sanitize_text_field( $post_params['marketing_method'] ) );
			}
			if( isset ( $post_params['active_plugins'] ) ) {
				if( ! empty( $post_params['active_plugins'] ) ) {
					$active_plugins = join( ',', $post_params['active_plugins'] );
					update_post_meta( $item_id, 'wisdom_active_plugins', sanitize_text_field( $active_plugins ) );
				}
			}
			if( isset ( $post_params['inactive_plugins'] ) ) {
				if( ! empty( $post_params['active_plugins'] ) ) {
					$inactive_plugins = join( ',', $post_params['inactive_plugins'] );
					update_post_meta( $item_id, 'wisdom_inactive_plugins', sanitize_text_field( $inactive_plugins ) );
				}
			}
			if( isset ( $post_params['plugin_options'] ) ) {
				if( ! empty( $post_params['plugin_options'] ) && is_array( $post_params['plugin_options'] ) ) {
					$sanitized_options = array();
					foreach( $post_params['plugin_options'] as $key=>$value ) {
						$sanitized_options[esc_attr( $key )] = sanitize_text_field( $value );
					}
					update_post_meta( $item_id, 'wisdom_plugin_options', $sanitized_options );
				}
			}
			if( isset ( $post_params['plugin_options_fields'] ) ) {
				if( ! empty( $post_params['plugin_options_fields'] ) && is_array( $post_params['plugin_options_fields'] ) ) {
					$sanitized_options = array();
					foreach( $post_params['plugin_options_fields'] as $key=>$value ) {
						$sanitized_options[esc_attr( $key )] = sanitize_text_field( $value );
					}
					update_post_meta( $item_id, 'wisdom_plugin_options_fields', $sanitized_options );
				}
			}
			//	Register the timestamp of most recent update
			update_post_meta( $item_id, 'wisdom_last_transmission', time() );

			do_action( 'wisdom_after_save_data', $item_id, $post_params );
		}
	}
}
add_action( 'init', 'wisdom_save_data' );

/*
 * Only certain keys are permitted
 * We don't want to save everything we find in $_POST necessarily
 * @since 1.0.0
 */
function wisdom_permitted_keys() {
	$keys = array(
		'url',
		'product_type',
		'plugin_slug',
		'site_name',
		'site_version',
		'site_language',
		'text_direction',
		'charset',
		'plugin',
		'status',
		'version',
		'theme',
		'theme_version',
		'theme_parent',
		'first_recorded',
		'deactivated_date',
		'deactivation_reason',
		'deactivation_details',
		'message',
		'file_location',
		'wisdom_version',
		'php_version',
		'server',
		'multisite',
		'email',
		'marketing_method',
		'active_plugins',
		'inactive_plugins',
		'plugin_options',
		'plugin_options_fields'
	);
	return $keys;
}

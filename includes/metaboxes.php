<?php
/**
 * Metaboxes for custom fields in tracked_plugin post types
 *
 * @package     Wisdom Plugin
 * @since       1.0.0
*/

/**
 * Our list of metafields
 * @since 1.0.0
 * @returns Array
 */
if( ! function_exists( 'wisdom_metaboxes' ) ) {
	function wisdom_metaboxes(){
		$metaboxes = array (
			array (
				'ID'			=> 'wisdom_product_metabox',
				'title'			=> __( 'Product Data', 'wisdom-plugin' ),
				'callback'		=> 'wisdom_meta_box_callback',
				'screens'		=> array( 'tracked-plugin' ),
				'context'		=> 'normal',
				'priority'		=> 'default',
				'fields'		=> array (
					array (
						'ID'		=> 'wisdom_plugin_slug',
						'name'		=> 'wisdom_plugin_slug',
						'title'		=> __( 'Product', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third wisdom-metabox-first',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_product_type',
						'name'		=> 'wisdom_product_type',
						'title'		=> __( 'Product Type', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third'
					),
					array (
						'ID'		=> 'wisdom_status',
						'name'		=> 'wisdom_status',
						'title'		=> __( 'Product Status', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_first_recorded',
						'name'		=> 'wisdom_first_recorded',
						'title'		=> __( 'First Recorded', 'wisdom-plugin' ),
						'type'		=> 'date',
						'class'		=> 'wisdom-metabox-third wisdom-metabox-first',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_deactivated_date',
						'name'		=> 'wisdom_deactivated_date',
						'title'		=> __( 'Time Deactivated', 'wisdom-plugin' ),
						'type'		=> 'date',
						'class'		=> 'wisdom-metabox-third',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_last_transmission',
						'name'		=> 'wisdom_last_transmission',
						'title'		=> __( 'Last Transmission', 'wisdom-plugin' ),
						'type'		=> 'date',
						'class'		=> 'wisdom-metabox-third'
					),
				),
			),
			array (
				'ID'			=> 'wisdom_theme_metabox',
				'title'			=> __( 'Theme Data', 'wisdom-plugin' ),
				'callback'		=> 'wisdom_meta_box_callback',
				'screens'		=> array( 'tracked-plugin' ),
				'context'		=> 'normal',
				'priority'		=> 'default',
				'fields'		=> array (
					array (
						'ID'		=> 'wisdom_theme',
						'name'		=> 'wisdom_theme',
						'title'		=> __( 'Theme', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third wisdom-metabox-first',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_theme_version',
						'name'		=> 'wisdom_theme_version',
						'title'		=> __( 'Theme Version', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third'
					),
					array (
						'ID'		=> 'wisdom_theme_parent',
						'name'		=> 'wisdom_theme_parent',
						'title'		=> __( 'Parent Theme', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third'
					),
				),
			),
			array (
				'ID'			=> 'wisdom_plugin_metabox',
				'title'			=> __( 'Plugin Data', 'wisdom-plugin' ),
				'callback'		=> 'wisdom_meta_box_callback',
				'screens'		=> array( 'tracked-plugin' ),
				'context'		=> 'normal',
				'priority'		=> 'default',
				'fields'		=> array (
					array (
						'ID'		=> 'wisdom_plugin',
						'name'		=> 'wisdom_plugin',
						'title'		=> __( 'Plugin', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third wisdom-metabox-first',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_initial_version',
						'name'		=> 'wisdom_initial_version',
						'title'		=> __( 'Initial Plugin Version', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_current_version',
						'name'		=> 'wisdom_current_version',
						'title'		=> __( 'Current Plugin Version', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_version_journey',
						'name'		=> 'wisdom_version_journey',
						'title'		=> __( 'Recorded Plugin Versions', 'wisdom-plugin' ),
						'type'		=> 'indexed_array',
						'class'		=> 'wisdom-metabox-first'
					),
					array (
						'ID'		=> 'wisdom_deactivation_reason',
						'name'		=> 'wisdom_deactivation_reason',
						'title'		=> __( 'Deactivation Reasons', 'wisdom-plugin' ),
						'type'		=> 'textarea',
						'class'		=> 'wisdom-metabox-first'
					),
					array (
						'ID'		=> 'wisdom_deactivation_details',
						'name'		=> 'wisdom_deactivation_details',
						'title'		=> __( 'Deactivation Details', 'wisdom-plugin' ),
						'type'		=> 'textarea',
						'class'		=> 'wisdom-metabox-first'
					),
					array (
						'ID'		=> 'wisdom_plugin_options',
						'name'		=> 'wisdom_plugin_options',
						'title'		=> __( 'Plugin Options', 'wisdom-plugin' ),
						'type'		=> 'indexed_array',
						'class'		=> 'wisdom-metabox-first'
					),
					array (
						'ID'		=> 'wisdom_plugin_options_fields',
						'name'		=> 'wisdom_plugin_options_fields',
						'title'		=> __( 'Plugin Options Fields', 'wisdom-plugin' ),
						'type'		=> 'assoc_array',
						'class'		=> 'wisdom-metabox-first'
					),
				),
			),
			array (
				'ID'			=> 'wisdom_site_metabox',
				'title'			=> __( 'Site Data', 'wisdom-plugin' ),
				'callback'		=> 'wisdom_meta_box_callback',
				'screens'		=> array( 'tracked-plugin' ),
				'context'		=> 'normal',
				'priority'		=> 'default',
				'fields'		=> array (
					array (
						'ID'		=> 'wisdom_site_name',
						'name'		=> 'wisdom_site_name',
						'title'		=> __( 'Site Name', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third wisdom-metabox-first'
					),
					array (
						'ID'		=> 'wisdom_site_version',
						'name'		=> 'wisdom_site_version',
						'title'		=> __( 'WordPress Version', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_site_language',
						'name'		=> 'wisdom_site_language',
						'title'		=> __( 'Site Language', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_text_direction',
						'name'		=> 'wisdom_text_direction',
						'title'		=> __( 'Text Direction', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third wisdom-metabox-first',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_charset',
						'name'		=> 'wisdom_charset',
						'title'		=> __( 'Charset', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_multisite',
						'name'		=> 'wisdom_multisite',
						'title'		=> __( 'Is Multisite', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_active_plugins',
						'name'		=> 'wisdom_active_plugins',
						'title'		=> __( 'Active Plugins', 'wisdom-plugin' ),
						'type'		=> 'plugins',
						'class'		=> 'wisdom-metabox-first'
					),
					array (
						'ID'		=> 'wisdom_inactive_plugins',
						'name'		=> 'wisdom_inactive_plugins',
						'title'		=> __( 'Inactive Plugins', 'wisdom-plugin' ),
						'type'		=> 'plugins',
						'class'		=> 'wisdom-metabox-first'
					),
					array (
						'ID'		=> 'wisdom_email',
						'name'		=> 'wisdom_email',
						'title'		=> __( 'Admin Email', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third wisdom-metabox-first'
					),
				),
			),
			array (
				'ID'			=> 'wisdom_wisdom_metabox',
				'title'			=> __( 'Wisdom Data', 'wisdom-plugin' ),
				'callback'		=> 'wisdom_meta_box_callback',
				'screens'		=> array( 'tracked-plugin' ),
				'context'		=> 'normal',
				'priority'		=> 'default',
				'fields'		=> array (
					array (
						'ID'		=> 'wisdom_version',
						'name'		=> 'wisdom_version',
						'title'		=> __( 'Wisdom Version', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third wisdom-metabox-first',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_marketing_method',
						'name'		=> 'wisdom_marketing_method',
						'title'		=> __( 'Notification Option', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_file_location',
						'name'		=> 'wisdom_file_location',
						'title'		=> __( 'Wisdom File Location', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_message',
						'name'		=> 'wisdom_message',
						'title'		=> __( 'Error Messages', 'wisdom-plugin' ),
						'type'		=> 'textarea',
						'class'		=> 'wisdom-metabox-first'
					),
				),
			),
			array (
				'ID'			=> 'wisdom_server_metabox',
				'title'			=> __( 'Server Data', 'wisdom-plugin' ),
				'callback'		=> 'wisdom_meta_box_callback',
				'screens'		=> array( 'tracked-plugin' ),
				'context'		=> 'normal',
				'priority'		=> 'default',
				'fields'		=> array (
					array (
						'ID'		=> 'wisdom_php_version',
						'name'		=> 'wisdom_php_version',
						'title'		=> __( 'PHP Version', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third wisdom-metabox-first',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_server',
						'name'		=> 'wisdom_server',
						'title'		=> __( 'Server', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third',
						'report'	=> true
					),
				),
			),
			array (
				'ID'			=> 'wisdom_mailchimp_metabox',
				'title'			=> __( 'Mailchimp Data', 'wisdom-plugin' ),
				'callback'		=> 'wisdom_meta_box_callback',
				'screens'		=> array( 'tracked-plugin' ),
				'context'		=> 'normal',
				'priority'		=> 'default',
				'fields'		=> array (
					array (
						'ID'		=> 'wisdom_mailchimp_status',
						'name'		=> 'wisdom_mailchimp_status',
						'title'		=> __( 'Mailchimp Status', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third wisdom-metabox-first',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_mailchimp_list',
						'name'		=> 'wisdom_mailchimp_list',
						'title'		=> __( 'Mailchimp List', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third',
						'report'	=> true
					),
					array (
						'ID'		=> 'wisdom_mailchimp_response',
						'name'		=> 'wisdom_mailchimp_response',
						'title'		=> __( 'Mailchimp Response', 'wisdom-plugin' ),
						'type'		=> 'text',
						'class'		=> 'wisdom-metabox-third',
						'report'	=> true
					),
				),
			),
		
		);

		return $metaboxes;
	}
}

/*
 * Register the metabox
 * @since 1.0.0
 */
function wisdom_add_meta_box() {
	
	$metaboxes = wisdom_metaboxes();
	
	foreach ( $metaboxes as $metabox ) {
	
		add_meta_box (
			$metabox['ID'],
			$metabox['title'],
			$metabox['callback'],
			$metabox['screens'],
			$metabox['context'],
			$metabox['priority'],
			$metabox['fields']
		);
		
	}
	
}
add_action ( 'add_meta_boxes', 'wisdom_add_meta_box' );

/*
 * Metabox callback for slide order
 * @since 1.0.0
*/
function wisdom_meta_box_callback( $post, $fields ) {

	wp_nonce_field ( 'save_metabox_data', 'wisdom_metabox_nonce' );
	
	if ( $fields['args'] ) {
		
		foreach ( $fields['args'] as $field ) {
				
			switch ( $field['type'] ) {

				case 'text':
					wisdom_metabox_text_output( $post, $field );
					break;
				case 'textarea':
					wisdom_metabox_textarea_output( $post, $field );
					break;
				case 'array':
					wisdom_metabox_array_output( $post, $field );
					break;
				case 'indexed_array':
					wisdom_metabox_indexed_array_output( $post, $field );
					break;
				case 'assoc_array':
					wisdom_metabox_assoc_array_output( $post, $field );
					break;
				case 'plugins':
					wisdom_metabox_plugins_output( $post, $field );
					break;
				case 'date':
					wisdom_metabox_date_output( $post, $field );
					break;
			}
				
		}
		
	}

}

/*
 * Metabox callback for text type
 * @since 1.0.0
 */
function wisdom_metabox_text_output( $post, $field ) {
	$value = get_post_meta ( $post->ID, $field['ID'], true );
	$classes = '';
	if( isset( $field['class'] ) ) {
		$classes = $field['class'];
	}
	?>
	<div class="wisdom-metabox-wrap <?php echo esc_attr( $classes ); ?>">
		<label for="<?php echo $field['name']; ?>"><?php echo $field['title']; ?></label>
		<input type="text" id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>" value="<?php echo esc_attr( $value ); ?>" >
	</div>
	<?php
}

/*
 * Metabox callback for textarea type
 * @since 1.0.0
 */
function wisdom_metabox_textarea_output( $post, $field ) {
	$value = get_post_meta ( $post->ID, $field['ID'], true );
	$classes = '';
	if( isset( $field['class'] ) ) {
		$classes = $field['class'];
	}
	?>
	<div class="wisdom-metabox-wrap <?php echo esc_attr( $classes ); ?>" style="margin-bottom: 1em;">
		<label for="<?php echo $field['name']; ?>"><?php echo $field['title']; ?></label>
		<textarea id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>"><?php echo esc_html( $value ); ?></textarea>
	</div>
	<?php
}

/*
 * Metabox callback for indexed array type
 * @since 1.0.0
 */
function wisdom_metabox_indexed_array_output( $post, $field ) {
	$values = get_post_meta ( $post->ID, $field['ID'], true );
	$output = '';
	if( ! empty( $values ) && is_array( $values ) ) {
		$output = join( "\n", $values );
	}
	$classes = '';
	if( isset( $field['class'] ) ) {
		$classes = $field['class'];
	}
	?>
	<div class="wisdom-metabox-wrap <?php echo esc_attr( $classes ); ?>" style="margin-bottom: 1em;">
		<label for="<?php echo $field['name']; ?>"><?php echo $field['title']; ?></label>
		<input type="hidden" id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>" value="<?php echo esc_attr( $values ); ?>">
		<textarea readonly rows=10><?php echo esc_attr( $output ); ?></textarea>
	</div>
	<?php
}


/*
 * Metabox callback for associated array type
 * @since 1.0.0
 */
function wisdom_metabox_assoc_array_output( $post, $field ) {
	$values = get_post_meta ( $post->ID, $field['ID'], true );
	$output = '';
	if( ! empty( $values ) && is_array( $values ) ) {
		foreach( $values as $key=>$value ) {
			$output .= esc_attr( $key ) . ' => ' . esc_attr( $value ) . "\n";
		}
	}
	$classes = '';
	if( isset( $field['class'] ) ) {
		$classes = $field['class'];
	}
	?>
	<div class="wisdom-metabox-wrap <?php echo esc_attr( $classes ); ?>" style="margin-bottom: 1em;">
		<label for="<?php echo $field['name']; ?>"><?php echo $field['title']; ?></label>
		<input type="hidden" id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>" value="<?php echo esc_attr( $values ); ?>">
		<textarea readonly rows=10><?php echo $output; ?></textarea>
	</div>
	<?php
}

/*
 * Metabox callback for array type
 * @since 1.0.0
 */
function wisdom_metabox_array_output( $post, $field ) {
	$value = get_post_meta ( $post->ID, $field['ID'], true );
	$classes = '';
	if( isset( $field['class'] ) ) {
		$classes = $field['class'];
	}
	?>
	<div class="wisdom-metabox-wrap <?php echo esc_attr( $classes ); ?>" style="margin-bottom: 1em;">
		<label for="<?php echo $field['name']; ?>"><?php echo $field['title']; ?></label>
		<input type="hidden" id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>" value="<?php echo esc_attr( $value ); ?>">
		<textarea readonly rows=10><?php print_r( $value ); ?></textarea>
	</div>
	<?php
}

/*
 * Metabox callback for plugins type
 * @since 1.0.0
 */
function wisdom_metabox_plugins_output( $post, $field ) {
	$values = get_post_meta ( $post->ID, $field['ID'], true );
	// Replace commas with line break
	$output = str_replace( ',', "\n", $values );
	$classes = '';
	if( isset( $field['class'] ) ) {
		$classes = $field['class'];
	}
	?>
	<div class="wisdom-metabox-wrap <?php echo esc_attr( $classes ); ?>" style="margin-bottom: 1em;">
		<label for="<?php echo $field['name']; ?>"><?php echo $field['title']; ?></label>
		<input type="hidden" id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>" value="<?php echo esc_attr( $values ); ?>">
		<textarea readonly rows=10><?php echo esc_attr( $output ); ?></textarea>
	</div>
	<?php
}

/*
 * Metabox callback for date type
 * @since 1.0.0
 */
function wisdom_metabox_date_output( $post, $field ) {
	$value = get_post_meta ( $post->ID, $field['ID'], true );
	$date = '';
	if( ! empty( $value ) ) {
		$date = date( 'r', $value );
	}
	$classes = '';
	if( isset( $field['class'] ) ) {
		$classes = $field['class'];
	}
	?>
	<div class="wisdom-metabox-wrap <?php echo esc_attr( $classes ); ?>">
		<label for="<?php echo $field['name']; ?>"><?php echo $field['title']; ?></label>
		<input type="hidden" id="<?php echo $field['name']; ?>" name="<?php echo $field['name']; ?>" value="<?php echo esc_attr( $value ); ?>">
		<input readonly type="text" value="<?php echo esc_attr( $date ); ?>">
	</div>
	<?php
}

/*
 * Save the metabox data
 * @since 1.0.0
 */
function wisdom_save_metabox_data( $post_id ) {
	
	// Check the nonce is set
	if ( ! isset ( $_POST['wisdom_metabox_nonce'] ) ) {
		return;
	}
	
	// Verify the nonce
	if ( ! wp_verify_nonce ( $_POST['wisdom_metabox_nonce'], 'save_metabox_data' ) ) {
		return;
	}
	
	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	
	// Check the user's permissions.
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}
	
	// Save all our metaboxes
	$metaboxes = wisdom_metaboxes();
	foreach ( $metaboxes as $metabox ) {
		if ( $metabox['fields'] ) {
			foreach ( $metabox['fields'] as $field ) {
				
				if ( $field['type'] != 'divider' ) {
					
					if ( isset ( $_POST[$field['name']] ) ) {
						
						$data = sanitize_text_field ( $_POST[$field['name']] );
						
						update_post_meta ( $post_id, $field['ID'], $data );
					} else {
						delete_post_meta ( $post_id, $field['ID'] );
					}
				}
			}
		}
	}
	
}
add_action( 'save_post', 'wisdom_save_metabox_data' );
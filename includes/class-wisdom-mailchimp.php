<?php
/**
 * Functions for MailChimp
 *
 * @package     Wisdom Plugin
 * @since       1.2.0
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use \DrewM\MailChimp\MailChimp;

/**
 * Class Wisdom_Admin
 * @since 1.1.0
 */
if( ! class_exists( 'Wisdom_Mailchimp' ) ) {

	class Wisdom_Mailchimp {

		private $item_id;
		private $email_address;
		private $plugin_slug;

		/**
		 * Constructor
		 * @param $_item_id	The post ID of the newly saved site
		 * @param $action Boolean - true to subscribe, false to unsubscribe
		 * @since 1.2.0
		 */
		public function __construct( $_item_id=null, $action=true ) {
			$this->item_id = $_item_id;
			$this->email_address = get_post_meta( $_item_id, 'wisdom_email', true );
			$this->plugin_slug = get_post_meta( $_item_id, 'wisdom_plugin_slug', true );
			if( $action == true ) {
				$this->subscribe_to_list();
			} else {
				$this->unsubscribe_from_list();
			}
		}

		/**
		 * Init
		 * @since 1.2.0
		 */
		public function init() {

		}

		/**
		 * Subscribe to list
		 * @param $Mailchimp	Mailchimp instance
		 * @param $list_id		List ID to subscribe to
		 * @since 1.2.0
		 */
		public function subscribe_to_list() {
			// Get Mailchimp API key
			$mailchimp_api_key = $this->get_api_key();

			// Get List ID
			$map_lists = $this->get_map_lists();

			if( ! empty( $map_lists ) ) {
				$number_rules = count( $map_lists );
				// Iterate through each rule
				for( $i = 0; $i < $number_rules; $i++ ) {
					// Find a rule that has a list set
					if( isset( $map_lists[$i]['list'] ) && $map_lists[$i]['list']  != 'no_list' ) {
						// We've found a list ID
						$list_id = $map_lists[$i]['list'];
						// Check the plugin slug
						if( ! isset( $map_lists[$i]['slug'] ) || $map_lists[$i]['slug'] == 'all' || $map_lists[$i]['slug'] == $this->plugin_slug ) {
							// Okay, now let's check for any rules we need to be aware of
							if( isset( $map_lists[$i]['rule'] ) && $map_lists[$i]['rule'] != 'no_rule' ) {
								// Check the rule and subscribe if it validates
								$is_valid = $this->check_rule( $map_lists[$i] );
								if( $is_valid ) {
									$this->do_subscription( $mailchimp_api_key, $list_id );
								}
							} else {
								// No rule set so can subscribe
								$this->do_subscription( $mailchimp_api_key, $list_id );
							}
						}
					}
				}
			}
		}

		/**
		 * Get API key
		 * @since 1.3.0
		 */
		public function get_api_key() {
			// Get Mailchimp API key
			$options = get_option( 'wisdom_options_settings' );
			if( isset( $options['mailchimp_api_key'] ) ) {
				return $options['mailchimp_api_key'];
			} else {
				// No key set
				return null;
			}
		}

		/**
		 * Get lists for new subscribers
		 * @since 1.3.0
		 */
		public function get_map_lists() {
			$options = get_option( 'wisdom_options_settings' );
			if( isset( $options['map_lists'] ) ) {
				return $options['map_lists'];
			} else {
				// No lists set
				return null;
			}
		}

		/**
		 * Get lists for deactivated users
		 * @since 1.3.0
		 */
		public function get_deactivation_lists() {
			$options = get_option( 'wisdom_options_settings' );
			if( isset( $options['deactivate_lists'] ) ) {
				return $options['deactivate_lists'];
			} else {
				// No lists set
				return null;
			}
		}

		/**
		 * Unsubscribe from list
		 * @since 1.3.0
		 */
		public function unsubscribe_from_list() {
			// Get Mailchimp API key
			$mailchimp_api_key = $this->get_api_key();

			// Get List ID
			$map_lists = $this->get_map_lists();

			if( ! empty( $map_lists ) ) {
				$number_rules = count( $map_lists );
				// Iterate through each rule
				for( $i = 0; $i < $number_rules; $i++ ) {
					// Find a rule that has a list set
					if( isset( $map_lists[$i]['list'] ) && $map_lists[$i]['list']  != 'no_list' ) {
						// We've found a list ID
						$list_id = $map_lists[$i]['list'];
						$this->do_unsubscription( $mailchimp_api_key, $list_id );

						// Now check if this email address should be subscribed to a deactivation list
						$deactivated_lists = $this->get_deactivation_lists();
						if( ! empty( $deactivated_lists ) ) {
							$number_rules = count( $deactivated_lists );
							// Iterate through each rule
							for( $i = 0; $i < $number_rules; $i++ ) {
								// Find a rule that has a list set
								if( isset( $deactivated_lists[$i]['list'] ) && $deactivated_lists[$i]['list']  != 'no_list' ) {
									// We've found a list ID
									$list_id = $deactivated_lists[$i]['list'];
									$this->do_subscription( $mailchimp_api_key, $list_id );
								}
							}
						}
					}
				}
			}
		}

		/**
		 * Subscribe the email address to the list
		 * @param $item	Current rule
		 * @return Boolean
		 * @since 1.2.0
		 */
		public function check_rule( $item ) {
			// Possible rules: Other plugins include, Themes include
			if( isset( $item['rule'] ) && $item['rule'] == 'plugins_include' && isset( $item['condition'] ) ) {
				// Check which other plugins are installed on the site
				$other_plugins = get_post_meta( $this->item_id, 'wisdom_active_plugins', true );
				if( $other_plugins ) {
					$other_plugins = explode( ',', $other_plugins );
					if( in_array( $item['condition'], $other_plugins ) ) {
						// The site has the specified plugin installed
						return true;
					}
				}
			} else if( isset( $item['rule'] ) && $item['rule'] == 'themes_include' && isset( $item['condition'] ) ) {
				// Check the active theme on the site
				$active_theme = get_post_meta( $this->item_id, 'wisdom_theme', true );
				if( $item['condition'] == $active_theme ) {
					return true;
				}
			}
			return false;
		}

		/**
		 * Subscribe the email address to the list
		 * @param $key	Key to validate
		 * @return Mixed
		 * @since 1.2.0
		 */
		public function do_subscription( $key, $list_id ) {
			if( isset( $key ) ) {
				$MailChimp = new MailChimp( $key );

				// Subscribe email to list according to plugin tracked
				$lists = $MailChimp->get('lists');

				if( ! empty( $lists['lists'] ) ) {
					$status = apply_filters( 'wisdom_mailchimp_optin_status', 'subscribed' );
					$result = $MailChimp->post(
						"lists/$list_id/members",
						[
							'email_address' => $this->email_address,
							'status'        => $status,
						]
					);
					$this->new_mailchimp_action( 'subscribe', $key, $list_id, $result, $this->email_address );
					update_post_meta( $this->item_id, 'wisdom_mailchimp_list', sanitize_text_field( $list_id ) );
				} else {
					// Can't get lists
					// $response = $MailChimp->getLastResponse();
					// update_post_meta( $this->item_id, 'wisdom_mailchimp_response', sanitize_text_field( $response['headers']['http_code'] ) );
					// echo $response['headers']['http_code'];
				}
				$response = $MailChimp->getLastResponse();
				update_post_meta( $this->item_id, 'wisdom_mailchimp_response', sanitize_text_field( $response['headers']['http_code'] ) );
			}
		}

		/**
		 * Unsubscribe the email address from the list
		 * @param $key	Key to validate
		 * @return Mixed
		 * @since 1.3.0
		 */
		public function do_unsubscription( $key, $list_id ) {
			if( isset( $key ) ) {
				$MailChimp = new MailChimp( $key );

				// Unsubscribe email to list according to plugin tracked
				$lists = $MailChimp->get('lists');

				if( ! empty( $lists['lists'] ) ) {
					$hash = $MailChimp->subscriberHash( $this->email_address );
					$result = $MailChimp->delete( "lists/$list_id/members/" . $hash );
					$this->new_mailchimp_action( 'unsubscribe', $key, $list_id, $result, $this->email_address );
					update_post_meta( $this->item_id, 'wisdom_mailchimp_list', sanitize_text_field( $list_id ) );
				} else {
					// Can't get lists
					// $response = $MailChimp->getLastResponse();
					// new_mailchimp_action( 'unsubscribe', $key, $list_id, $response, $this->email_address );
				}
				$response = $MailChimp->getLastResponse();
				update_post_meta( $this->item_id, 'wisdom_mailchimp_response', sanitize_text_field( $response['headers']['http_code'] ) );
			}
		}

		/**
		 * Validate key
		 * @param $key	Key to validate
		 * @return Mixed
		 * @since 1.2.0
		 */
		public function validate_key( $key ) {
			// Get Mailchimp API key
			$MailChimp = new MailChimp( $key );
			$lists = $MailChimp->get('lists');
			$response = $MailChimp->getLastResponse();
			return $response['headers']['http_code'];
		}

		/**
		 * Log attempted subscriptions
		 * @param $action			Either subscribe or unsubscribe
		 * @param $key				Mailchimp key
		 * @param $list_id 			List ID
		 * @param $response 		API call response
		 * @param $email_address	User email address
		 * @since 1.3.1
		 */
		public function new_mailchimp_action( $action, $key, $list_id, $response, $email_address ) {
			update_post_meta( $this->item_id, 'wisdom_mailchimp_status', $action );
			$option = get_option( 'wisdom_mc_tracker' );
			$new_entry = array(
				'action'	=> sanitize_text_field( $action ),
				'time'		=> time(),
				'key'		=> sanitize_text_field( $key ),
				'list'		=> sanitize_text_field( $list_id ),
				'email'		=> sanitize_email( $email_address ),
				'response'	=> print_r( $response, true )
			);
			if( is_array( $option ) ) {
				array_unshift( $option, $new_entry );
			} else {
				$option = array( $new_entry );
			}
			$option = array_slice( $option, 0, 10 );
			$option['wisdom_registered_setting'] = 1;
			update_option( 'wisdom_mc_tracker', $option );
		}

	}

}

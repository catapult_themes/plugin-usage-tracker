<?php
/**
 * Register tracked-plugin post type
 *
 * @package     Wisdom Plugin
 * @since       1.0.0
*/

// Exit if accessed directly

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Register the tracked_plugin post type
 * @since 1.0.0
 */
function wisdom_register_post_type() {
	$labels = array(
		'name'               => _x( 'Tracked Products', 'post type general name', 'wisdom-plugin' ),
		'singular_name'      => _x( 'Tracked Product', 'post type singular name', 'wisdom-plugin' ),
		'menu_name'          => _x( 'Wisdom', 'admin menu', 'wisdom-plugin' ),
		'name_admin_bar'     => _x( 'Tracked Product', 'add new on admin bar', 'wisdom-plugin' ),
		'add_new'            => _x( 'Add New', 'topic', 'wisdom-plugin' ),
		'add_new_item'       => __( 'Add New Tracked Product', 'wisdom-plugin' ),
		'new_item'           => __( 'New Tracked Product', 'wisdom-plugin' ),
		'edit_item'          => __( 'Edit Tracked Product', 'wisdom-plugin' ),
		'view_item'          => __( 'View Tracked Products', 'wisdom-plugin' ),
		'all_items'          => __( 'All Tracked Products', 'wisdom-plugin' ),
		'search_items'       => __( 'Search Tracked Products', 'wisdom-plugin' ),
		'parent_item_colon'  => __( 'Parent Tracked Product:', 'wisdom-plugin' ),
		'not_found'          => __( 'No products found.', 'wisdom-plugin' ),
		'not_found_in_trash' => __( 'No products found in Trash.', 'wisdom-plugin' )
	);
	$args = array(
		'labels'            => $labels,
		'description'       => __( 'Description.', 'wisdom-plugin' ),
		'public'            => false,
		'publicly_queryable'=> false,
		'show_ui'           => true,
		'show_in_menu'      => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'tracked-plugin' ),
		'capability_type'   => 'post',
		'menu_icon'			=> 'dashicons-chart-area',
		'has_archive'       => true,
		'hierarchical'      => false,
		'menu_position'     => null,
		'supports'          => array( 'title' )
	);
	register_post_type( 'tracked-plugin', $args );
}
add_action( 'init', 'wisdom_register_post_type' );

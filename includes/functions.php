<?php
/**
 * Functions for Wisdom
 *
 * @package     Wisdom Plugin
 * @since       1.0.0
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Validate the license and get the number of permitted plugins
 * @param $force Boolean	True if we want to force the HTTP Request (e.g. after an upgrade)
 * @param $license_data 	License data
 * @since 1.0.0
 */
function wisdom_plugin_set_plugin_limit( $force=false, $license_data='' ) {

	global $wp_version;
	
	$limit = trim( get_option( 'wisdom_license_limit' ) );
	// If limit is set
	if( ! $force && $limit !== false ) {
		return $limit;
	}

	// If we haven't passed in any license data, get it now
	// Avoid doubling up on HTTP requests
	if( empty( $license_data ) ) {
		
		$license = trim( get_option( 'wisdom_plugin_license_key' ) );

		$api_params = array(
			'edd_action' 	=> 'check_license',
			'license' 		=> $license,
			'item_name' 	=> urlencode( WISDOM_PLUGIN_ITEM_NAME ),
			'url'       	=> home_url()
		);

		// Call the custom API.
		//$response = wp_remote_post( WISDOM_PLUGIN_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
		$response = wp_remote_post( WISDOM_PLUGIN_STORE_URL, array( 'timeout' => 15, 'body' => $api_params ) );

		if ( is_wp_error( $response ) )
			return false;

		$license_data = json_decode( wp_remote_retrieve_body( $response ) );
		
	}

	$limit = 0;
	
	if( $license_data->license != 'valid' ) {
		// This license is not valid
		$limit = -1;
	} else if( isset( $license_data->license_limit ) ) {
		// Using the license_limit to define how many plugins can be tracked
		$limit = $license_data->license_limit;
	}
	
	update_option( 'wisdom_license_limit', intval( $limit ) );
	
	return $limit;
	
}

/**
 * Returns the little code snippet
 * @since 1.0.0
 */
function wisdom_code_snippet() {
$snippet = "/**
 * This function allows you to track usage of your plugin
 * Place in your main plugin file
 * Refer to https://wisdomplugin.com/support for help
 */
if( ! class_exists( 'Plugin_Usage_Tracker') ) {
	require_once dirname( __FILE__ ) . '/tracking/class-plugin-usage-tracker.php';
}
if( ! function_exists( 'your_product_prefix_start_plugin_tracking' ) ) {
	function your_product_prefix_start_plugin_tracking() {
		VARIABLE_NAME = new Plugin_Usage_Tracker(
			__FILE__,
			'BASE_URL',
			array(SETTINGS),
			OPTIN,
			FORM,
			MARKETING
		);
	}
	your_product_prefix_start_plugin_tracking();
}";
	
	return $snippet;
}

function wisdom_get_option( $settings, $option, $default='' ) {
	$settings = get_option( $settings );
	if( isset( $settings[$option] ) ) {
		return $settings[$option];
	}
	if( $default ) {
		return $default;
	}
	return false;
}